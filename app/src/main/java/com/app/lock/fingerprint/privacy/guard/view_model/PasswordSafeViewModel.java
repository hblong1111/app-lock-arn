package com.app.lock.fingerprint.privacy.guard.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.lock.fingerprint.privacy.guard.model.PasswordSafe;

import java.util.List;

public class PasswordSafeViewModel extends ViewModel {
    private MutableLiveData<List<PasswordSafe>> passwordList = new MutableLiveData<>();
    private MutableLiveData<PasswordSafe> passwordEdit = new MutableLiveData<>();

    public MutableLiveData<PasswordSafe> getPasswordEdit() {
        return passwordEdit;
    }

    public void setPasswordEdit(PasswordSafe passwordEdit) {
        this.passwordEdit .postValue(passwordEdit);
    }

    public MutableLiveData<List<PasswordSafe>> getPasswordList() {
        return passwordList;
    }

    public void setPasswordList(List<PasswordSafe> passwordList) {
        this.passwordList.postValue(passwordList);
    }
}
