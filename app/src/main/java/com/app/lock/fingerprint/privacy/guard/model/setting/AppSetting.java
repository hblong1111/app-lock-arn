package com.app.lock.fingerprint.privacy.guard.model.setting;

public abstract class AppSetting {
    protected String getKey () {
        return this.getClass().getName();
    }

    abstract void setValue (Object value);

    abstract Object getValue ();

}
