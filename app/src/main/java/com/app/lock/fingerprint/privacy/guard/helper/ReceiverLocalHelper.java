package com.app.lock.fingerprint.privacy.guard.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

public class ReceiverLocalHelper {
    public static void sendBroadcast(Context context, ReceiverType type, Bundle data) {
        context.sendBroadcast(new Intent(getAction(type)).putExtra("data", data));
    }

    public static void sendBroadcast(Context context, ReceiverType type) {
        context.sendBroadcast(new Intent(getAction(type)));
    }

    public static void registerReceiver(Context context, BroadcastReceiver receiver, ReceiverType type) {
        context.registerReceiver(receiver, new IntentFilter(getAction(type)));
    }

    public static String getAction(ReceiverType type) {
        return type.name();
    }


    public enum ReceiverType {
        ACTION_CLOSE_USAGE_PERMISSION_DIALOG,
        ACTION_CLOSE_OVERLAY_PERMISSION_DIALOG,
        ACTION_GRAND_ALL_PERMISSION,
        ACTION_CHANGE_LIST_APP_LOCK,
        ACTION_CHANGE_THEME,
        ACTION_CHANGE_SHOW_ICON,
        ACTION_TAKE_INTRUDER,
        ACTION_GRAND_CAMERA_PERMISSION,
        ACTION_CHANGE_INTRUDER_SETTING,
        ACTION_CHANGE_LOCK_TYPE,
    }
}
