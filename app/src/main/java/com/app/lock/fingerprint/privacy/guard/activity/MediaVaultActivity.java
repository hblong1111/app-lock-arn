package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.app.lock.R;
import com.app.lock.databinding.ActivityMediaVaultBinding;
import com.app.lock.fingerprint.privacy.guard.fragment.media_vault.MediaVaultFragment;
import com.app.lock.fingerprint.privacy.guard.helper.MediaVaultHelper;
import com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper;
import com.app.lock.fingerprint.privacy.guard.utils.CheckEvent;
import com.app.lock.fingerprint.privacy.guard.view_model.MediaVaultViewModel;

import java.util.List;

public class MediaVaultActivity extends AppCompatActivity {
    private ActivityMediaVaultBinding binding;

    private MediaVaultViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMediaVaultBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(MediaVaultViewModel.class);


        if (PermissionHelper.checkWriteExternalStorage(this)) {
            fetchData();
        } else {
            PermissionHelper.requestWriteExternalStorage(this);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.containerFragment, new MediaVaultFragment()).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PermissionHelper.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (PermissionHelper.checkWriteExternalStorage(this)) {
                fetchData();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionHelper.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (PermissionHelper.checkWriteExternalStorage(this)) {
                fetchData();
            }
        }
    }

    private void fetchData() {
        new Thread(() -> {
            List<String> listAudios = MediaVaultHelper.getAllFileVault(MediaVaultHelper.Type.TYPE_AUDIOS);
            List<String> listImage = MediaVaultHelper.getAllFileVault(MediaVaultHelper.Type.TYPE_IMAGES);
            List<String> listVideos = MediaVaultHelper.getAllFileVault(MediaVaultHelper.Type.TYPE_VIDEOS);
            List<String> listDocument = MediaVaultHelper.getAllFileVault(MediaVaultHelper.Type.TYPE_DOCUMENTS);

            viewModel.setListAudios(listAudios);
            viewModel.setListImages(listImage);
            viewModel.setListVideos(listVideos);
            viewModel.setListDocument(listDocument);
        }).start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        int audioSize = viewModel.getListAudios().getValue() != null ? viewModel.getListAudios().getValue().size() : 0;
        int imageSize = viewModel.getListImages().getValue() != null ? viewModel.getListAudios().getValue().size() : 0;
        int documentSize = viewModel.getListDocument().getValue() != null ? viewModel.getListAudios().getValue().size() : 0;
        int videoSize = viewModel.getListVideos().getValue() != null ? viewModel.getListAudios().getValue().size() : 0;

        int numberFileLock = audioSize + imageSize + documentSize + videoSize;
        int numberClick = viewModel.getNumberClickFile().getValue();

        CheckEvent.checkMediaVault(this, numberFileLock, numberClick);
    }
}