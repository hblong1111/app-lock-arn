package com.app.lock.fingerprint.privacy.guard.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.app.lock.fingerprint.privacy.guard.utils.SharedUtils;
import com.blankj.utilcode.util.ActivityUtils;

public class SystemsCloseDialogReceiver extends BroadcastReceiver {

    String SYSTEM_DIALOG_REASON_KEY = "reason";
    String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
    String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";

    @Override
    public void onReceive (Context context, Intent intent) {

    }

}
