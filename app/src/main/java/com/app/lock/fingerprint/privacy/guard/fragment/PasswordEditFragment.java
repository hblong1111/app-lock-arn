package com.app.lock.fingerprint.privacy.guard.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.lock.R;
import com.app.lock.databinding.FragmentPasswordAddBinding;
import com.app.lock.databinding.FragmentPasswordEditBinding;
import com.app.lock.fingerprint.privacy.guard.db.AppDatabase;
import com.app.lock.fingerprint.privacy.guard.model.PasswordSafe;
import com.app.lock.fingerprint.privacy.guard.view_model.PasswordSafeViewModel;

public class PasswordEditFragment extends Fragment implements View.OnClickListener {
    private FragmentPasswordEditBinding binding;
    private PasswordSafeViewModel viewModel;
    private AppDatabase database;

    private PasswordSafe passwordSafeEdit;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPasswordEditBinding.inflate(inflater);
        database = AppDatabase.getInstance(requireActivity());
        viewModel = new ViewModelProvider(requireActivity()).get(PasswordSafeViewModel.class);

        viewModel.getPasswordEdit().observe(requireActivity(), passwordSafe -> {
            passwordSafeEdit = passwordSafe;

            binding.setTitle(passwordSafe.getTitle());
            binding.setAccount(passwordSafe.getAccount());
            binding.setPassword(passwordSafe.getPassword());

            binding.setPasswordSafe(passwordSafe);
        });


        binding.btnBack.setOnClickListener(this);
        binding.btnSave.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                requireActivity().onBackPressed();
                break;
            case R.id.btnSave:
                String note = binding.edtNote.getText().toString();
                String password = binding.edtPassword.getText().toString();
                String title = binding.edtTitle.getText().toString();
                String account = binding.edtAccount.getText().toString();

                binding.setTitle(title);
                binding.setAccount(account);
                binding.setPassword(password);

                if (!checkData(title) && !checkData(account) && !checkData(password)) {
                    passwordSafeEdit.setPassword(password);
                    passwordSafeEdit.setAccount(account);
                    passwordSafeEdit.setTitle(title);
                    passwordSafeEdit.setNote(note);


                    database.passwordDao().update(passwordSafeEdit);
                    viewModel.setPasswordEdit(passwordSafeEdit);
                    Toast.makeText(requireActivity(), "Success!", Toast.LENGTH_SHORT).show();
                    requireActivity().onBackPressed();
                } else {
                    Toast.makeText(requireActivity(), "Data error, please try again!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private boolean checkData(String txt) {
        return txt == null || txt.isEmpty();
    }
}
