package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.app.lock.R;
import com.app.lock.databinding.ActivityChangePasswordBinding;
import com.app.lock.databinding.ActivitySetPasswordBinding;
import com.app.lock.fingerprint.privacy.guard.fragment.change_password.ChangePasswordFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.change_password.UnLockChangePasswordFragment;
import com.app.lock.fingerprint.privacy.guard.view_model.ChangePasswordViewModel;

public class ChangePasswordActivity extends AppCompatActivity {
    private ActivityChangePasswordBinding binding;

    private ChangePasswordViewModel viewModel;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChangePasswordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(ChangePasswordViewModel.class);

        getSupportFragmentManager().beginTransaction().replace(binding.containerFragmentChangePassword.getId(), new UnLockChangePasswordFragment()).commit();


        viewModel.getIsUnLock().observe(this, isUnLock -> {
            if (isUnLock) {
                getSupportFragmentManager().beginTransaction().replace(binding.containerFragmentChangePassword.getId(), new ChangePasswordFragment()).commit();
            }
        });
    }
}