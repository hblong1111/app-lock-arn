package com.app.lock.fingerprint.privacy.guard.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.app.lock.R;
import com.app.lock.fingerprint.privacy.guard.model.PasswordSafe;

@Database(entities = {PasswordSafe.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, context.getString(R.string.app_name) + ".db")
                    .allowMainThreadQueries()
                    .build();

        }
        return INSTANCE;
    }

    public abstract PasswordDao passwordDao();

}