package com.app.lock.fingerprint.privacy.guard.ads;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.core.content.ContextCompat;

import com.ads.control.Admod;
import com.ads.control.funtion.AdCallback;
import com.app.lock.R;
import com.app.lock.fingerprint.privacy.guard.application.MyApplication;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

public class NativeAds {


    public NativeAds(Context context, String idAds, int resLayout, FrameLayout frameLayout, Callback callback) {
        if (MyApplication.isShowAds) {

            UnifiedNativeAdView adView = (UnifiedNativeAdView) LayoutInflater.from(context).inflate(resLayout, null).findViewById(R.id.ads_view);

            frameLayout.removeAllViews();
            frameLayout.addView((View) adView.getParent());
//            ((ShimmerFrameLayout) adView.findViewById(R.id.shimmerLayout)).startShimmer();

            Admod.getInstance().loadUnifiedNativeAd(context, idAds, new AdCallback() {


                @Override
                public void onAdFailedToLoad(LoadAdError i) {
                    super.onAdFailedToLoad(i);
                    callback.onAdFailedToLoad(i);
                }

                @Override
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    super.onUnifiedNativeAdLoaded(unifiedNativeAd);

//                    adView.findViewById(R.id.ad_media).setBackgroundColor(0);
//                    adView.findViewById(R.id.ad_app_icon).setBackgroundColor(0);
//                    adView.findViewById(R.id.ad_headline).setBackgroundColor(0);
//                    adView.findViewById(R.id.ad_body).setBackgroundColor(0);
//                    adView.findViewById(R.id.ad_call_to_action).setBackground(ContextCompat.getDrawable(context, R.drawable.bg_circle_ads_color));
//
                    ((ShimmerFrameLayout) adView.getParent()).stopShimmer();
                    ((ShimmerFrameLayout) adView.getParent()).hideShimmer();
                    Admod.getInstance().populateUnifiedNativeAdView(unifiedNativeAd, adView);
                }

            });
        }
    }

    public abstract static class Callback {
        public void onAdFailedToLoad(LoadAdError i) {
        }
    }
}
