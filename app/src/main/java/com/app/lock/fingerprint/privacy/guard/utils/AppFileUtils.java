package com.app.lock.fingerprint.privacy.guard.utils;

import android.content.Context;

import java.io.File;

public class AppFileUtils {
    private static final String INTRUDER_ROOT_FOLDER_NAME = "Image Intruder";
    private static final String FOLDER_BACKGROUND_NAME = "FOLDER_BACKGROUND_NAME";
    private static final String FOLDER_PREVIEW_ROOT = "FOLDER_PREVIEW_ROOT";

    public static File getFileRootIntruder(Context context) {
        File file = new File(context.getFilesDir(), INTRUDER_ROOT_FOLDER_NAME);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static File getThemeBackgroundRoot(Context context) {
        File inFolderApp = context.getFilesDir();
        File rootThemeBackground = new File(inFolderApp, FOLDER_BACKGROUND_NAME);
        return rootThemeBackground;
    }

    public static File getThemePreviewRoot(Context context) {
        File inFolderApp = context.getFilesDir();
        File themePreviewRoot = new File(inFolderApp, FOLDER_PREVIEW_ROOT);
        return themePreviewRoot;
    }
}
