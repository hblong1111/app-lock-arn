package com.app.lock.fingerprint.privacy.guard.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

public class MediaVaultViewModel extends ViewModel {
    private MutableLiveData<List<String>> listImages = new MutableLiveData<>();
    private MutableLiveData<List<String>> listDocument = new MutableLiveData<>();
    private MutableLiveData<List<String>> listVideos = new MutableLiveData<>();
    private MutableLiveData<List<String>> listAudios = new MutableLiveData<>();

    private MutableLiveData<Integer> numberClickFile = new MutableLiveData<>();

    public MediaVaultViewModel() {
        numberClickFile.setValue(0);
    }

    public MutableLiveData<Integer> getNumberClickFile() {
        return numberClickFile;
    }

    public void setNumberClickFile(int numberClickFile) {
        this.numberClickFile.postValue(numberClickFile);
    }

    private MutableLiveData<Integer> numberChoose = new MutableLiveData<>();

    public MutableLiveData<Integer> getNumberChoose() {
        return numberChoose;
    }

    public void setNumberChoose(int numberChoose) {
        this.numberChoose.postValue(numberChoose);
    }

    public MutableLiveData<List<String>> getListImages() {
        return listImages;
    }

    public void setListImages(List<String> listImages) {
        this.listImages.postValue(listImages);
    }

    public MutableLiveData<List<String>> getListDocument() {
        return listDocument;
    }

    public void setListDocument(List<String> listDocument) {
        this.listDocument.postValue(listDocument);
    }

    public MutableLiveData<List<String>> getListVideos() {
        return listVideos;
    }

    public void setListVideos(List<String> listVideos) {
        this.listVideos.postValue(listVideos);
    }

    public MutableLiveData<List<String>> getListAudios() {
        return listAudios;
    }

    public void setListAudios(List<String> listAudios) {
        this.listAudios.postValue(listAudios);
    }
}
