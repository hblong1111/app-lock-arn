package com.app.lock.fingerprint.privacy.guard.network;

import com.app.lock.fingerprint.privacy.guard.model.ThemeInstallResult;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ThemeService {
    @GET("mediaserver/images")
    Call<ThemeInstallResult> getAllTheme();
}
