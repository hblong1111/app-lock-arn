package com.app.lock.fingerprint.privacy.guard.fragment.media_vault.document;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.lock.R;
import com.app.lock.databinding.FragmentDocumentsAddBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.media_vault.document.DocumentAddAdapter;
import com.app.lock.fingerprint.privacy.guard.helper.MediaVaultHelper;
import com.app.lock.fingerprint.privacy.guard.view_model.MediaVaultViewModel;

import java.util.ArrayList;
import java.util.List;

public class DocumentsAddFragment extends Fragment implements View.OnClickListener {
    private FragmentDocumentsAddBinding binding;
    private MediaVaultViewModel viewModel;
    private DocumentAddAdapter adapter;
    private List<String> listDocument;
    private List<String> listDocumentChoose;
    private Context context;
    private MediaVaultHelper.Type type = MediaVaultHelper.Type.TYPE_DOCUMENTS;
    private DocumentAddAdapter.Callback callback = new DocumentAddAdapter.Callback() {
        @Override
        public void onItemClick (int pos) {
            String path = listDocument.get(pos);
            if (listDocumentChoose.contains(path)) {
                listDocumentChoose.remove(path);
            } else {
                listDocumentChoose.add(path);
            }
            adapter.notifyItemChanged(pos, adapter.PAYLOAD_CHOOSE_CHANGE);

            viewModel.setNumberChoose(listDocumentChoose.size());
        }
    };


    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = requireActivity();
        binding = FragmentDocumentsAddBinding.inflate(inflater);

        viewModel = new ViewModelProvider(requireActivity()).get(MediaVaultViewModel.class);

        viewModel.setNumberChoose(0);


        setupRCV();

        fetchData();

        viewModel.getNumberChoose().observe(requireActivity(), integer -> {
            binding.bntAdd.setText(String.format("Add %d Files", integer));
        });

        binding.btnBack.setOnClickListener(this);
        binding.bntAdd.setOnClickListener(this);
        return binding.getRoot();
    }

    private void fetchData () {
        new Thread(() -> {
            ArrayList<String> listImageDevice = (ArrayList<String>) MediaVaultHelper.getDocumentPath(context);

            requireActivity().runOnUiThread(() -> {
                if (listImageDevice.isEmpty()) {
                    binding.rcv.setVisibility(View.INVISIBLE);
                    binding.groupNotFound.setVisibility(View.VISIBLE);
                } else {
                    binding.rcv.setVisibility(View.VISIBLE);
                    binding.groupNotFound.setVisibility(View.INVISIBLE);
                }
            });
            for (String path :
                    listImageDevice) {
                listDocument.add(path);
                requireActivity().runOnUiThread(() -> {
                    adapter.notifyItemInserted(adapter.getItemCount());
                });
            }
        }).start();
    }

    private void setupRCV () {
        listDocument = new ArrayList<>();
        listDocumentChoose = new ArrayList<>();
        adapter = new DocumentAddAdapter(listDocument, listDocumentChoose, callback);
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new LinearLayoutManager(context));
    }

    @Override
    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.bntAdd:
                if (! listDocumentChoose.isEmpty()) {
                    List<String> listLock = viewModel.getListDocument().getValue();
                    for (String path :
                            listDocumentChoose) {
                        listLock.add(MediaVaultHelper.lockFile(context, path, type));
                    }
                    viewModel.setListDocument(listLock);
                }
                break;
        }

        requireActivity().onBackPressed();
    }
}
