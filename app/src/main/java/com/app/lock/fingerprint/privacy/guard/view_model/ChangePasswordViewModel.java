package com.app.lock.fingerprint.privacy.guard.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ChangePasswordViewModel extends ViewModel {
    private MutableLiveData<Boolean> isUnLock = new MutableLiveData<>();


    public MutableLiveData<Boolean> getIsUnLock () {
        return isUnLock;
    }

    public void setIsUnLock (boolean isUnLock) {
        this.isUnLock.postValue(isUnLock);
    }
}
