package com.app.lock.fingerprint.privacy.guard.view_custom;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.app.lock.R;
import com.app.lock.databinding.DialogRateBinding;
import com.app.lock.fingerprint.privacy.guard.model.setting.RateSetting;
import com.app.lock.fingerprint.privacy.guard.utils.CheckEvent;

public class RateDialog extends Dialog implements View.OnClickListener {
    private DialogRateBinding binding;

    public RateDialog (@NonNull Context context) {
        super(context);
        init();
    }

    public RateDialog (@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    private void init () {
        binding = DialogRateBinding.inflate(getLayoutInflater(), null, false);
        binding.btnCancel.setOnClickListener(this);
        binding.btnSubmit.setOnClickListener(this);

        binding.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged (RatingBar ratingBar, float rating, boolean fromUser) {
                switch ((int) rating) {
                    case 4:
                    case 5:
                        startRateOnMarket();
                        dismiss();

                        break;
                    case 1:
                    case 2:
                    case 3:
                        binding.btnCancel.setVisibility(View.GONE);
                        binding.btnSubmit.setVisibility(View.VISIBLE);
                        binding.edtFeedback.setVisibility(View.VISIBLE);

                        binding.tvTitle.setText("Help us to improve our app");
                        binding.tvSubTitle.setText("Write us a review!");
                        break;
                    default:
                        binding.btnCancel.setVisibility(View.VISIBLE);
                        binding.btnSubmit.setVisibility(View.GONE);
                        binding.edtFeedback.setVisibility(View.GONE);

                        binding.tvTitle.setText("Your opinion matter to us!");
                        binding.tvSubTitle.setText("Tap a star to rate our app.");
                        break;

                }
            }
        });

        int width = getContext().getResources().getDisplayMetrics().widthPixels;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(binding.getRoot());
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = (int) (0.9f * width);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        setCancelable(false);
        getWindow().setAttributes(lp);
    }


    @Override
    public void onClick (View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                break;
            case R.id.btnSubmit:
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    Toast.makeText(getContext(), "Thank for rating!", Toast.LENGTH_SHORT).show();
                }, 100);
                break;
        }
        dismiss();
    }

    @Override
    public void dismiss () {
        super.dismiss();
        float star = binding.ratingBar.getRating();
        String comment = binding.edtFeedback.getText().toString();
        if (star > 0) {
            CheckEvent.checkRate(getContext(), (int) star, comment);
            RateSetting.getInstance(getContext()).setValue(true);
        }
    }

    private void startRateOnMarket () {
        String pkgName = getContext().getPackageName();
        Uri uri = Uri.parse("market://details?id=" + pkgName);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            getContext().startActivity(goToMarket);
        } catch (Exception e) {
            getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + pkgName)));
        }
    }

}
