package com.app.lock.fingerprint.privacy.guard.fragment.themes_pager;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.lock.R;
import com.app.lock.databinding.FragmentThemeInstallDetailBinding;
import com.app.lock.fingerprint.privacy.guard.ads.InterstitialAds;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.model.setting.ThemeSetting;
import com.app.lock.fingerprint.privacy.guard.utils.CheckEvent;
import com.app.lock.fingerprint.privacy.guard.view_model.HomeViewModel;
import com.bumptech.glide.Glide;

import java.util.Objects;

import static java.util.Objects.*;

public class ThemeInstallDetailFragment extends Fragment implements View.OnClickListener {
    private FragmentThemeInstallDetailBinding binding;
    private HomeViewModel viewModel;
    private boolean selected;

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentThemeInstallDetailBinding.inflate(inflater);

        viewModel = new ViewModelProvider(getActivity()).get(HomeViewModel.class);

        viewModel.getThemeInstallPreview().observe(getActivity(), theme -> {
            if (getActivity() != null) {
                Glide.with(requireActivity()).load(theme.getPreviewFile()).into(binding.img);
            }
        });
        viewModel.getThemeChoose().observe(requireActivity(), themeChoose -> {
            Theme theme = viewModel.getThemeInstallPreview().getValue();
            selected = themeChoose.equals(theme);
            binding.setSelected(selected);
            binding.executePendingBindings();
        });
        binding.getRoot().setOnClickListener(this);
        binding.btnSave.setOnClickListener(this);
        binding.bntClose.setOnClickListener(this);

        return binding.getRoot();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bntClose:
                requireActivity().onBackPressed();
                break;
            case R.id.btnSave:
                if (!selected) {
                    showDialogConfirm();
                } else {
                    Toast.makeText(requireActivity(), "Theme is selected!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void showDialogConfirm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            dialog.dismiss();
        });
        builder.setPositiveButton("Ok", (dialog, which) -> {
            dialog.dismiss();
            loadAds();
        });
        builder.setMessage("Are you sure you want to choose this theme?");
        builder.setTitle("Alert");
        builder.create().show();
    }

    private void loadAds() {
        InterstitialAds.getInstance(requireActivity(), new InterstitialAds.CustomCallback() {
            @Override
            public void onPostShow() {
                super.onPostShow();

                CheckEvent.checkTheme(requireContext());

                Theme themeChoose = viewModel.getThemeInstallPreview().getValue();
                ThemeSetting.getInstance(getActivity()).setValue(themeChoose);
                viewModel.setThemeChoose(themeChoose);
                requireActivity().onBackPressed();
            }
        }).showAds();
    }
}
