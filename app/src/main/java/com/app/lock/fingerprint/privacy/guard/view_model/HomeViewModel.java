package com.app.lock.fingerprint.privacy.guard.view_model;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.model.ThemeInstallResult;

import java.io.File;
import java.util.List;

public class HomeViewModel extends ViewModel {
    private MutableLiveData<List<List<String>>> listApp = new MutableLiveData<>();
    private MutableLiveData<List<String>> listAppLock = new MutableLiveData<>();
    private MutableLiveData<List<Theme>> listThemeInstall = new MutableLiveData<>();
    private MutableLiveData<Theme> themeChoose = new MutableLiveData<>();
    private MutableLiveData<Theme> themeInstallPreview = new MutableLiveData<>();
    private MutableLiveData<ThemeInstallResult.Image> themeFeaturedPreview = new MutableLiveData<>();
    private MutableLiveData<List<ThemeInstallResult.Image>> themeFeaturedList = new MutableLiveData<>();
    private MutableLiveData<ThemeInstallResult> themeResult = new MutableLiveData<>();

    public MutableLiveData<List<ThemeInstallResult.Image>> getThemeFeaturedList() {
        return themeFeaturedList;
    }

    public void setThemeFeaturedList(List<ThemeInstallResult.Image> themeFeaturedList) {
        this.themeFeaturedList.postValue(themeFeaturedList);
    }

    public MutableLiveData<ThemeInstallResult.Image> getThemeFeaturedPreview() {
        return themeFeaturedPreview;
    }

    public void setThemeFeaturedPreview(ThemeInstallResult.Image themeFeaturedPreview) {
        this.themeFeaturedPreview.postValue(themeFeaturedPreview);
    }

    //theme install preview

    public MutableLiveData<Theme> getThemeInstallPreview() {
        return themeInstallPreview;
    }

    public void setThemeInstallPreview(Theme themeInstallPreview) {
        this.themeInstallPreview.postValue(themeInstallPreview);
    }

    //set and get theme choose
    public MutableLiveData<Theme> getThemeChoose() {
        return themeChoose;
    }

    public void setThemeChoose(Theme themeChoose) {
        this.themeChoose.postValue(themeChoose);
    }


    //set and get list theme installed
    public MutableLiveData<List<Theme>> getListThemeInstall() {
        return listThemeInstall;
    }

    public void setListThemeInstall(List<Theme> listThemeInstall) {
        this.listThemeInstall.postValue(listThemeInstall);
    }


    //set and get list app on device
    public MutableLiveData<List<List<String>>> getListApp() {
        return listApp;
    }

    public void setListApp(List<List<String>> listApp) {
        this.listApp.postValue(listApp);
    }


    //set and get list app locked
    public MutableLiveData<List<String>> getListAppLock() {
        return listAppLock;
    }

    public void setListAppLock(List<String> listAppLock) {
        this.listAppLock.postValue(listAppLock);
    }

    public MutableLiveData<ThemeInstallResult> getThemeResult() {
        return themeResult;
    }

    public void setThemeResult(ThemeInstallResult themeResult) {
        this.themeResult.postValue(themeResult);
    }
}
