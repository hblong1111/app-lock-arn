package com.app.lock.fingerprint.privacy.guard.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.content.ContextCompat;

import com.app.lock.fingerprint.privacy.guard.helper.ServiceHelper;
import com.app.lock.fingerprint.privacy.guard.service.LockAppService;

public class AutoStart extends BroadcastReceiver {
    @Override
    public void onReceive (Context context, Intent intent) {
        ServiceHelper.startService(context, LockAppService.class);
    }
}
