package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.app.lock.R;
import com.app.lock.databinding.ActivityChangeLockTypeBinding;
import com.app.lock.fingerprint.privacy.guard.fragment.change_lock_type.ChooseLockTypeFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.change_lock_type.SetPasswordFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.change_lock_type.UnLockChangeTypeFragment;
import com.app.lock.fingerprint.privacy.guard.view_model.ChangeLockTypeViewModel;

public class ChangeLockTypeActivity extends AppCompatActivity {
    private ActivityChangeLockTypeBinding binding;

    private ChangeLockTypeViewModel viewModel;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChangeLockTypeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(ChangeLockTypeViewModel.class);

        viewModel.getIsUnLock().observe(this, isUnLock -> {
            if (isUnLock) {
                getSupportFragmentManager().beginTransaction().replace(R.id.containerFrChangeType, new ChooseLockTypeFragment()).commit();
            }
        });


        viewModel.getTypeChoose().observe(this, lockType -> {
            if (lockType != null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.containerFrChangeType, new SetPasswordFragment()).addToBackStack(null).commit();
            }
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.containerFrChangeType, new UnLockChangeTypeFragment()).commit();
    }

    @Override
    public void onBackPressed () {
        int fragmentCount = getSupportFragmentManager().getBackStackEntryCount();
        if (fragmentCount > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}