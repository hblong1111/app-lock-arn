package com.app.lock.fingerprint.privacy.guard.fragment.media_vault.photos;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.app.lock.BuildConfig;
import com.app.lock.R;
import com.app.lock.databinding.FragmentPhotoBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.media_vault.photo.ImageAdapter;
import com.app.lock.fingerprint.privacy.guard.helper.MediaVaultHelper;
import com.app.lock.fingerprint.privacy.guard.view_model.MediaVaultViewModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PhotosFragment extends Fragment implements View.OnClickListener {
    private FragmentPhotoBinding binding;
    private MediaVaultViewModel viewModel;
    private ImageAdapter adapter;
    private List<String> listImage;
    private List<String> listSelected;
    private boolean isEditList;
    private ImageAdapter.Callback callback;

    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPhotoBinding.inflate(inflater);
        viewModel = new ViewModelProvider(requireActivity()).get(MediaVaultViewModel.class);

        viewModel.setNumberChoose(0);

        setupRCV();

        binding.btnBack.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);
        binding.btnUnLock.setOnClickListener(this);
        binding.btnAdd.setOnClickListener(this);

        viewModel.getListImages().observe(requireActivity(), paths -> {
            if (paths.isEmpty()) {
                binding.rcv.setVisibility(View.INVISIBLE);
                binding.groupNotFound.setVisibility(View.VISIBLE);
            } else {

                binding.rcv.setVisibility(View.VISIBLE);
                binding.groupNotFound.setVisibility(View.INVISIBLE);
            }
            if (! paths.equals(listImage)) {
                for (String path :
                        paths) {
                    if (! listImage.contains(path)) {
                        listImage.add(0, path);
                        adapter.notifyItemInserted(0);
                    }
                }
            }
        });

        viewModel.getNumberChoose().observe(requireActivity(), integer -> {
            binding.btnUnLock.setText(String.format("Un Lock %d File", integer));
        });
        return binding.getRoot();
    }

    private void setupRCV () {
        listImage = new ArrayList<>();
        listSelected = new ArrayList<>();
        callback = new ImageAdapter.Callback() {
            @Override
            public void onClick (int pos) {
                String path = listImage.get(pos);
                if (! isEditList) {
                    File file = new File(path);
                    Uri uri = FileProvider.getUriForFile(requireActivity(), BuildConfig.APPLICATION_ID, file);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    String type = "image/*";
                    intent.setDataAndType(uri, type);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    requireActivity().startActivity(intent);
                    viewModel.getNumberClickFile().setValue(viewModel.getNumberClickFile().getValue() + 1);
                } else {
                    if (listSelected.contains(path)) {
                        listSelected.remove(path);
                    } else {
                        listSelected.add(path);
                    }
                    viewModel.setNumberChoose(listSelected.size());
                    adapter.notifyItemChanged(pos, adapter.PAYLOAD_CHANGE_SELECTED);
                }
            }

            @Override
            public void onLongClick (int position) {
                if (! isEditList) {
                    setEditList(true);
                } else {
                    String path = listImage.get(position);
                    if (listSelected.contains(path)) {
                        listSelected.remove(path);
                    } else {
                        listSelected.add(path);
                    }
                    viewModel.setNumberChoose(listSelected.size());
                    adapter.notifyItemChanged(position, adapter.PAYLOAD_CHANGE_SELECTED);
                }
            }
        };
        adapter = new ImageAdapter(listImage, listSelected, callback);

        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
    }

    @Override
    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                requireActivity().onBackPressed();
                break;
            case R.id.btnAdd:
                requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerFragment, new PhotoAddFragment()).addToBackStack(null).commit();
                break;
            case R.id.btnUnLock:
                for (String path :
                        listSelected) {
                    MediaVaultHelper.unLock(requireActivity(), path);
                    int index = listImage.indexOf(path);
                    listImage.remove(index);
                    adapter.notifyItemRemoved(index);
                }
                viewModel.setListImages(listImage);
            case R.id.btnCancel:
                setEditList(false);
                break;
        }
    }

    public void setEditList (boolean editList) {
        if (! editList) {
            listSelected.clear();
        }
        if (isEditList == editList) {
            return;
        }
        isEditList = editList;
        adapter.setEdit(editList);
        binding.setIsEdit(isEditList);
        binding.executePendingBindings();
    }
}
