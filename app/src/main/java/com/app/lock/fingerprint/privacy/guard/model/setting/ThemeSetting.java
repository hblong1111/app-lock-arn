package com.app.lock.fingerprint.privacy.guard.model.setting;

import android.content.Context;

import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.utils.AppFileUtils;
import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.app.lock.fingerprint.privacy.guard.utils.SharedUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ThemeSetting extends AppSetting {
    private static ThemeSetting INSTANCE;
    private Context context;

    public static ThemeSetting getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new ThemeSetting(context);
        }
        return INSTANCE;
    }

    private ThemeSetting(Context context) {
        this.context = context;
    }

    @Override
    protected String getKey() {
        return super.getKey();
    }

    @Override
    public void setValue(Object value) {
        try {
            Theme theme = (Theme) value;
            List<String> list = new ArrayList<>();
            list.add(theme.getPreviewFile().getAbsolutePath());
            list.add(theme.getBackgroundFile().getAbsolutePath());
            SharedUtils.getInstance(context).putListString(getKey(), (ArrayList<String>) list);

            ReceiverLocalHelper.sendBroadcast(context, ReceiverLocalHelper.ReceiverType.ACTION_CHANGE_THEME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //get Theme choose
    @Override
    public Theme getValue() {
        List<String> list = SharedUtils.getInstance(context).getListString(getKey());

        File bgFile;
        File previewFile;
        if (list.isEmpty()) {
            previewFile = new File(AppFileUtils.getThemePreviewRoot(context), "0.png");
            bgFile = new File(AppFileUtils.getThemeBackgroundRoot(context), "0.png");
        } else {
            previewFile = new File(list.get(0));
            bgFile = new File(list.get(1));
        }
        return new Theme(bgFile, previewFile);
    }
}
