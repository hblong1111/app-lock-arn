package com.app.lock.fingerprint.privacy.guard.fragment.themes_pager;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.lock.R;
import com.app.lock.databinding.FragmentThemesInstalledBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.home_theme.ThemeInstallAdapter;
import com.app.lock.fingerprint.privacy.guard.ads.InterstitialAds;
import com.app.lock.fingerprint.privacy.guard.ads.NativeAds;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.view_model.HomeViewModel;
import com.google.android.gms.ads.LoadAdError;

import java.util.ArrayList;
import java.util.List;

public class InstalledFragment extends Fragment {
    private FragmentThemesInstalledBinding binding;
    private Context context;
    private ThemeInstallAdapter adapter;
    private List<Theme> listTheme;
    private Theme themeChoose;

    private HomeViewModel viewModel;
    private ThemeInstallDetailFragment fragmentDetail;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentThemesInstalledBinding.inflate(inflater);

        fragmentDetail = new ThemeInstallDetailFragment();

        context = getActivity();

        viewModel = new ViewModelProvider(getActivity()).get(HomeViewModel.class);

        setupRCV();
        viewModel.getListThemeInstall().observe(getActivity(), themes -> {
            if (!themes.equals(listTheme)) {
                boolean isEmpty = listTheme.isEmpty();
                for (Theme theme :
                        themes) {
                    if (!listTheme.contains(theme)) {
                        int index = isEmpty ? adapter.getItemCount() : 0;
                        listTheme.add(index, theme);
                        adapter.notifyItemInserted(index);
                        if (index == 0) {
                            adapter.notifyItemChanged(3, adapter.PAYLOAD_UPDATE_THEME_CHOOSE);
                        }
                    }
                }
            }
        });
        viewModel.getThemeChoose().observe(getActivity(), theme -> {
            if (theme.equals(themeChoose)) {
                return;
            }
            themeChoose.setPreviewFile(theme.getPreviewFile());
            themeChoose.setBackgroundFile(theme.getBackgroundFile());
            adapter.notifyItemRangeChanged(0, adapter.getItemCount(), ThemeInstallAdapter.PAYLOAD_UPDATE_THEME_CHOOSE);
        });


        return binding.getRoot();
    }

    private void setupRCV() {
        themeChoose = new Theme();
        listTheme = new ArrayList<>();
        adapter = new ThemeInstallAdapter(listTheme, themeChoose, createCallbackAdapter());
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new GridLayoutManager(context, 3));
    }

    private ThemeInstallAdapter.Callback createCallbackAdapter() {
        return position -> {
            viewModel.setThemeInstallPreview(listTheme.get(position));
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerFragmentHome, fragmentDetail).addToBackStack(null).commit();
        };
    }

}
