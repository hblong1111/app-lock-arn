package com.app.lock.fingerprint.privacy.guard.fragment.intruder;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.lock.BuildConfig;
import com.app.lock.R;
import com.app.lock.databinding.FragmentIntruderBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.intruder.IntruderAdapter;
import com.app.lock.fingerprint.privacy.guard.ads.InterstitialAds;
import com.app.lock.fingerprint.privacy.guard.ads.NativeAds;
import com.app.lock.fingerprint.privacy.guard.model.setting.IntruderSetting;
import com.app.lock.fingerprint.privacy.guard.utils.AppFileUtils;
import com.app.lock.fingerprint.privacy.guard.view_model.IntruderViewModel;
import com.google.android.gms.ads.LoadAdError;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class IntruderFragment extends Fragment implements View.OnClickListener {
    private FragmentIntruderBinding binding;
    private IntruderViewModel viewModel;
    private IntruderAdapter adapter;
    private List<File> list;
    private IntruderAdapter.Callback callback = new IntruderAdapter.Callback() {
        @Override
        public void onItemClick (int pos) {
            File file = list.get(pos);
            Uri uri = FileProvider.getUriForFile(requireActivity(), BuildConfig.APPLICATION_ID, file);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            String type = "image/*";
            intent.setDataAndType(uri, type);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            requireActivity().startActivity(intent);
        }

        @Override
        public void onDeleteItem (File file) {
            int pos = list.indexOf(file);
            if (list.get(pos).delete()) {
                list.remove(pos);
                adapter.notifyItemRemoved(pos);
                adapter.notifyItemRangeChanged(0, adapter.getItemCount(), adapter.PAYLOAD_UPDATE_PADDING);
            }

            if (list.isEmpty()) {
                binding.groupNotFound.setVisibility(View.VISIBLE);
            }
        }
    };

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView (@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = FragmentIntruderBinding.inflate(inflater);

        viewModel = new ViewModelProvider(requireActivity()).get(IntruderViewModel.class);


        setupView();

        observeViewModel();



        new NativeAds(requireActivity(), getString(R.string.id_ads_native_function), R.layout.ads_native_home_medium, binding.frameLayoutAds, new NativeAds.Callback() {
            @Override
            public void onAdFailedToLoad (LoadAdError i) {
                super.onAdFailedToLoad(i);
                binding.motionLayoutAds.transitionToEnd();
            }
        });

        return binding.getRoot();
    }

    private void observeViewModel () {
        binding.switchActive.setOnCheckedChangeListener((compoundButton, b) -> {
            IntruderSetting.getInstance(requireActivity()).setValue(false);
            viewModel.setIsActive(false);
        });

    }

    private void setupView () {
        list = new ArrayList<>();
        adapter = new IntruderAdapter(list, callback);
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new GridLayoutManager(requireActivity(), 2));

        getData();
        binding.btnBack.setOnClickListener(this);
    }

    private void getData () {
        File[] files = AppFileUtils.getFileRootIntruder(requireActivity()).listFiles();
        for (File file :
                files) {
            list.add(0, file);
            adapter.notifyItemInserted(0);
        }

        if (list.isEmpty()) {
            binding.groupNotFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                requireActivity().onBackPressed();
                break;
        }
    }
}
