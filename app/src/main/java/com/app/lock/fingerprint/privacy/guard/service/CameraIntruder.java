package com.app.lock.fingerprint.privacy.guard.service;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ViewPort;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import com.app.lock.R;
import com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.utils.AppFileUtils;
import com.google.common.util.concurrent.ListenableFuture;

import java.io.File;

public class CameraIntruder {
    private Context context;
    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    private ImageCapture imageCapture;
    private LifecycleOwner lifecycle;
    private boolean isTakeReady;
    private ProcessCameraProvider cameraProvider;

    public CameraIntruder (Context context, LifecycleOwner lifecycle) {
        this.context = context;
        this.lifecycle = lifecycle;
        createCameraIntruder();
    }

    @SuppressLint({"RestrictedApi", "UnsafeOptInUsageError"})
    private void createCameraIntruder () {
        cameraProviderFuture = ProcessCameraProvider.getInstance(context);

        cameraProviderFuture.addListener(() -> {
              cameraProvider = null;
            try {
                cameraProvider = cameraProviderFuture.get();

                CameraSelector cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA;

                imageCapture =
                        new ImageCapture.Builder()
                                .setCameraSelector(CameraSelector.DEFAULT_FRONT_CAMERA)
                                .build();
                cameraProvider.unbindAll();


                // Bind use cases to camera
                cameraProvider.bindToLifecycle(lifecycle, cameraSelector, (ViewPort) null, imageCapture);
                isTakeReady = true;
            } catch (Exception e) {
                isTakeReady = false;
                e.printStackTrace();
            }
        }, ContextCompat.getMainExecutor(context));
    }


    public void takePhoto () {
        if (isTakeReady) {
            File file = new File(AppFileUtils.getFileRootIntruder(context), context.getString(R.string.app_name) + "-" + System.currentTimeMillis() + ".jpg");
            file.mkdirs();
            ImageCapture.OutputFileOptions outputFileOptions =
                    new ImageCapture.OutputFileOptions.Builder(file).build();
            imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(context),
                    new ImageCapture.OnImageSavedCallback() {
                        @Override
                        public void onImageSaved (ImageCapture.OutputFileResults outputFileResults) {
                            // insert your code here.
                            Log.d("hblong", "ForegroundService.onImageSaved: " + "save photo success ");
                        }

                        @Override
                        public void onError (ImageCaptureException error) {
                            // insert your code here.
                            Log.e("hblong", "ForegroundService.onError: ", error);
                        }
                    }
            );
        } else {
            Log.d("hblong", "ForegroundService.takePhoto: " + "error create");
        }
    }

    public void destroy() {
        if (cameraProvider != null) {
            cameraProvider.unbindAll();
        }
    }
}
