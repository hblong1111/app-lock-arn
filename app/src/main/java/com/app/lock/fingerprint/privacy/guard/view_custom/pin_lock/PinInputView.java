package com.app.lock.fingerprint.privacy.guard.view_custom.pin_lock;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.app.lock.R;
import com.app.lock.databinding.ViewPinInputBinding;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;

import org.jetbrains.annotations.NotNull;

public class PinInputView extends ConstraintLayout implements View.OnClickListener {
    private ViewPinInputBinding binding;
    private Context context;
    private String password;
    private int passLength;

    private Callback callback;
    private CallbackIndicator callbackIndicator;

    public PinInputView (@NonNull @NotNull Context context) {
        super(context);
        initView();
    }

    public PinInputView (@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public PinInputView (@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public PinInputView (@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView () {
        context = getContext();
        binding = ViewPinInputBinding.inflate(LayoutInflater.from(context), this, true);


        password = "";

        setColor(R.color.white);


        binding.btn0.setOnClickListener(this);
        binding.btn1.setOnClickListener(this);
        binding.btn2.setOnClickListener(this);
        binding.btn3.setOnClickListener(this);
        binding.btn4.setOnClickListener(this);
        binding.btn5.setOnClickListener(this);
        binding.btn6.setOnClickListener(this);
        binding.btn7.setOnClickListener(this);
        binding.btn8.setOnClickListener(this);
        binding.btn9.setOnClickListener(this);
        binding.btnDelete.setOnClickListener(this);
        binding.btnReset.setOnClickListener(this);
    }

    public void resetPin () {
        if (password.length() != 0) {
            password = "";
            if (callbackIndicator != null) {
                callbackIndicator.onChangeTxt(password);
            }
            if (callback != null) {
                callback.onChange(password);
            }
        }
    }

    @Override
    public void onClick (View v) {
        switch (v.getId()) {
            case R.id.btn0:
            case R.id.btn1:
            case R.id.btn2:
            case R.id.btn3:
            case R.id.btn4:
            case R.id.btn5:
            case R.id.btn6:
            case R.id.btn7:
            case R.id.btn8:
            case R.id.btn9:
                setChangePass(((TextView) v).getText().toString());
                break;
            case R.id.btnDelete:
                removePassword();
                break;
            case R.id.btnReset:
                resetPin();
                break;
        }

    }

    private void removePassword () {
        if (password.length() != 0) {
            password = password.substring(0, password.length() - 1);
            if (callbackIndicator != null) {
                callbackIndicator.onChangeTxt(password);
            }
            if (callback != null) {
                callback.onChange(password);
            }
        }
    }

    private void setChangePass (String number) {
        if (password.length() < getLengthPin()) {
            password += number;


            if (callbackIndicator != null) {
                callbackIndicator.onChangeTxt(password);
            }

            if (password.length() == getLengthPin() && callback != null) {
                callback.onComplete(password);
            } else {
                if (callback != null) {
                    callback.onChange(password);
                }
            }

        }
    }

    private int getLengthPin () {
        return passLength == 0 ? LockTypeSetting.getInstance(context).getValue() == LockTypeSetting.LockType.TYPE_PIN_4 ? 4 : 6 : passLength;
    }

    public void setCallback (Callback callback) {
        this.callback = callback;
    }

    public void setCallbackIndicator (CallbackIndicator callbackIndicator) {
        this.callbackIndicator = callbackIndicator;
    }

    public void setPassLength (int passLength) {
        this.passLength = passLength;
    }

    public interface Callback {
        void onChange (String password);

        void onComplete (String password);

    }

    public interface CallbackIndicator {
        void onChangeTxt (String password);

    }

    public void setColor (int color) {
        int colorInput = ContextCompat.getColor(context, color);
        binding.btn0.setTextColor(colorInput);
        binding.btn1.setTextColor(colorInput);
        binding.btn2.setTextColor(colorInput);
        binding.btn3.setTextColor(colorInput);
        binding.btn4.setTextColor(colorInput);
        binding.btn5.setTextColor(colorInput);
        binding.btn6.setTextColor(colorInput);
        binding.btn7.setTextColor(colorInput);
        binding.btn8.setTextColor(colorInput);
        binding.btn9.setTextColor(colorInput);
        binding.btnReset.setColorFilter(colorInput);
        binding.btnDelete.setColorFilter(colorInput);
    }

}
