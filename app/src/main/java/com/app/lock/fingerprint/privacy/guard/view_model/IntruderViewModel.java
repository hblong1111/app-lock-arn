package com.app.lock.fingerprint.privacy.guard.view_model;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.os.Bundle;

import com.app.lock.R;

import java.io.File;
import java.util.List;

public class IntruderViewModel extends ViewModel {
    private MutableLiveData<Boolean> isActive = new MutableLiveData<>();
    private MutableLiveData<List<File>> listIntruderFile = new MutableLiveData<>();

    public MutableLiveData<List<File>> getListIntruderFile () {
        return listIntruderFile;
    }

    public void setListIntruderFile (List<File> listIntruderFile) {
        this.listIntruderFile.postValue(listIntruderFile);
    }

    public MutableLiveData<Boolean> getIsActive () {
        return isActive;
    }

    public void setIsActive (Boolean isActive) {
        this.isActive.postValue(isActive);
    }
}