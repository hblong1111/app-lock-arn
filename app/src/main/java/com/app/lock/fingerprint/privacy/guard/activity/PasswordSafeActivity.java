package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;

import com.app.lock.R;
import com.app.lock.databinding.ActivityPasswordSafeBinding;
import com.app.lock.fingerprint.privacy.guard.db.AppDatabase;
import com.app.lock.fingerprint.privacy.guard.fragment.PasswordSafeFragment;
import com.app.lock.fingerprint.privacy.guard.model.PasswordSafe;
import com.app.lock.fingerprint.privacy.guard.view_model.PasswordSafeViewModel;

import java.util.ArrayList;
import java.util.List;

public class PasswordSafeActivity extends AppCompatActivity {
    private PasswordSafeViewModel viewModel;
    private ActivityPasswordSafeBinding binding;

    private AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPasswordSafeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        database = AppDatabase.getInstance(this);

        viewModel = new ViewModelProvider(this).get(PasswordSafeViewModel.class);

        if (viewModel.getPasswordList().getValue() == null) {
            List<PasswordSafe> passwordList = database.passwordDao().getAll();
            viewModel.setPasswordList(passwordList);
        }


        int fragmentCount = getSupportFragmentManager().getBackStackEntryCount();
        if (fragmentCount == 0) {
            getSupportFragmentManager().beginTransaction().replace(binding.containerFragment.getId(), new PasswordSafeFragment()).commit();
        } else {
            Fragment fr = getSupportFragmentManager().getFragments().get(fragmentCount - 1);
            getSupportFragmentManager().beginTransaction().replace(binding.containerFragment.getId(), fr).commit();

        }

    }
}