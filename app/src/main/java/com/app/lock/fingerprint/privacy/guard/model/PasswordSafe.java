package com.app.lock.fingerprint.privacy.guard.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Objects;

@Entity
public class PasswordSafe implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String title;
    private String account;
    private String password;
    private String note;

    public PasswordSafe(String title, String account, String password, String note) {
        this.title = title;
        this.account = account;
        this.password = password;
        this.note = note;
    }

    public PasswordSafe() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PasswordSafe that = (PasswordSafe) o;
        return id == that.id && Objects.equals(title, that.title) && Objects.equals(account, that.account) && Objects.equals(password, that.password) && Objects.equals(note, that.note);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, account, password, note);
    }
}
