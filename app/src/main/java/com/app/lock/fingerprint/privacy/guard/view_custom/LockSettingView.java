package com.app.lock.fingerprint.privacy.guard.view_custom;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.app.lock.databinding.ViewLockBinding;
import com.app.lock.databinding.ViewLockSettingBinding;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.PasswordSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ShowIconSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ThemeSetting;
import com.app.lock.fingerprint.privacy.guard.view_custom.pattern.PatternLockView;
import com.app.lock.fingerprint.privacy.guard.view_custom.pin_lock.PinInputView;
import com.bumptech.glide.Glide;

import java.io.File;

public class LockSettingView extends ConstraintLayout {
    private Context context;
    private ViewLockSettingBinding binding;


    public LockSettingView(@NonNull Context context) {
        super(context);
        init();
    }

    public LockSettingView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LockSettingView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public LockSettingView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        context = getContext();

        binding = ViewLockSettingBinding.inflate(LayoutInflater.from(context), this, true);


        binding.pinInput.setOnClickListener(view -> Log.d("hblong", "init: " + "click"));
//        setup view
        setLockType(LockTypeSetting.getInstance(context).getValue());
        setShowIconApp(ShowIconSetting.getInstance(context).getValue());

    }

    public void setLockType(LockTypeSetting.LockType lockType) {
        if (lockType == LockTypeSetting.LockType.TYPE_PIN_4) {
            binding.indicatorPin4.setPinView(binding.pinInput);
        } else {
            binding.indicatorPin6.setPinView(binding.pinInput);
        }

        binding.setType(lockType);
        binding.executePendingBindings();

    }

    public void setShowIconApp(boolean showIconApp) {
        binding.imgIconApp.setVisibility(showIconApp ? VISIBLE : INVISIBLE);
    }


    public void setBackgroundFile(File backgroundFile) {
        Glide.with(this).load(backgroundFile).into(binding.imgBackground);
    }

    public void setBackgroundLink(String backgroundLink) {
        Glide.with(this).load(backgroundLink).into(binding.imgBackground);
    }
}
