package com.app.lock.fingerprint.privacy.guard.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.app.lock.R;
import com.kaopiz.kprogresshud.KProgressHUD;

public class DialogHelper {

    public static KProgressHUD showDialogLoading (Context context, String detailsLabel) {
        return KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(context.getString(R.string.please_wait))
                .setDetailsLabel(detailsLabel)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public static Dialog showDialog (Context context, View view, ConfigDialog configDialog) {
        int width = context.getResources().getDisplayMetrics().widthPixels;
        Dialog alertDialog = new Dialog(context);

        if (configDialog != null) {

            configDialog.configDialog(alertDialog);
        }

        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = (int) (0.9f * width);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        alertDialog.setCancelable(configDialog.setCancelable());
        try {
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        alertDialog.getWindow().setAttributes(lp);

        return alertDialog;
    }


    public abstract static class ConfigDialog {
        public void configDialog (Dialog dialog) {
        }

        public abstract boolean setCancelable ();
    }

}
