package com.app.lock.fingerprint.privacy.guard.remote_config;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lock.BuildConfig;
import com.app.lock.R;
import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;

public class MyRemoteConfig {

    private Activity activity;
    private int appVersionCode;
    public static final String PREF_RATE = "PREF_RATE";
    private Drawable drawableApp;

    public MyRemoteConfig(Activity activity) {
        this.activity = activity;
        appVersionCode = BuildConfig.VERSION_CODE;
        try {
            drawableApp = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 0).loadIcon(activity.getPackageManager());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        checkUpdate();
    }

    private void checkUpdate() {
        FirebaseRemoteConfig config = FirebaseRemoteConfig.getInstance();
        long minFetch = 12 * 60 * 60;
        if (BuildConfig.DEBUG) {
            minFetch = 1;
        }

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(minFetch).build();

        config.setConfigSettingsAsync(configSettings);

        config.fetchAndActivate().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) return;
            long time = config.getLong("delay_time_load_native_unlock");

            Common.TIME_DELAY_LOAD_NATIVE_ADS = time;

            String json = config.getString("config_update_version");
            ConfigUpdateVersion result = new Gson().fromJson(json, ConfigUpdateVersion.class);

            if (result.isRequired) {
                for (int version : result.versionCodeRequired) {
                    if (version == appVersionCode) {
                        //todo: show dialog update bat buoc
                        showDialogUpdateApp(result);
                        break;
                    }
                }
            } else if (!result.status) return;
            else if (result.versionCode > appVersionCode) {
                showBottomSheetUpdate(activity, result);
            }


        });
    }

    private void showDialogUpdateApp(ConfigUpdateVersion result) {
        Dialog alertDialog = new Dialog(activity);
        int width = activity.getResources().getDisplayMetrics().widthPixels;


        View view = activity.getLayoutInflater().inflate(R.layout.dialog_update, null, false);

        view.findViewById(R.id.ud_icon).setBackground(drawableApp);
        ((TextView) view.findViewById(R.id.ud_app_title)).setText(activity.getString(R.string.app_name));

        ((TextView) view.findViewById(R.id.ud_version_name)).setText("Version " + result.versionName);
        ((TextView) view.findViewById(R.id.ud_message)).setText(result.message);

        view.findViewById(R.id.btnUpdate).setOnClickListener(v -> {
            alertDialog.dismiss();

            startRateOnMarket();
        });

        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(view);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = (int) (0.9f * width);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }

    private void startRateOnMarket() {
        String pkgName = activity.getPackageName();
        Uri uri = Uri.parse("market://details?id=" + pkgName);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            activity.startActivity(goToMarket);
        } catch (Exception e) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + pkgName)));
        }
    }

    private void showBottomSheetUpdate(Activity activity, ConfigUpdateVersion config) {
        final BottomSheetDialog dialog = new BottomSheetDialog(activity);
        dialog.setContentView(R.layout.bottom_remote_update);

        ((TextView) dialog.findViewById(R.id.bru_title)).setText(config.title);
        ((TextView) dialog.findViewById(R.id.bru_version_name)).setText("Version " + config.versionName);
        ((TextView) dialog.findViewById(R.id.bru_message)).setText(config.message);
        ((ImageView) dialog.findViewById(R.id.bru_icon)).setBackground(drawableApp);
        ((TextView) dialog.findViewById(R.id.bru_app_title)).setText(activity.getString(R.string.app_name));

        ((Button) dialog.findViewById(R.id.bru_update)).setOnClickListener(v -> {
            if (config.newPackage != null) {
                linkToStore(activity, config.newPackage);
            } else {

                linkToStore(activity, "");
            }
            dialog.cancel();

        });
        dialog.show();
    }

    private void linkToStore(Activity activity, String newPackage) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(
                PREF_RATE, Context.MODE_PRIVATE
        );

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(PREF_RATE, true);
        editor.apply();

        String appPackageName = "";
        if (newPackage.equals("")) {
            appPackageName = activity.getPackageName();
        } else {
            appPackageName = newPackage;
        }

        try {
            activity.startActivity(
                    new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + appPackageName)
                    )
            );
        } catch (ActivityNotFoundException e) {
            activity.startActivity(
                    new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)
                    )
            );
        }
    }
}
