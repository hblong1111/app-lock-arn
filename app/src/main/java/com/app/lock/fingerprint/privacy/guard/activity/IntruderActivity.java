package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.app.lock.R;
import com.app.lock.databinding.ActivityIntruderBinding;
import com.app.lock.fingerprint.privacy.guard.fragment.intruder.IntruderFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.intruder.IntruderIntroFragment;
import com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ServiceHelper;
import com.app.lock.fingerprint.privacy.guard.model.setting.IntruderSetting;
import com.app.lock.fingerprint.privacy.guard.service.LockAppService;
import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.app.lock.fingerprint.privacy.guard.view_model.IntruderViewModel;

import org.jetbrains.annotations.NotNull;

public class IntruderActivity extends AppCompatActivity {
    private ActivityIntruderBinding binding;
    private IntruderViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityIntruderBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(IntruderViewModel.class);

        viewModel.setIsActive(IntruderSetting.getInstance(this).getValue());

        viewModel.getIsActive().observe(this, isActive -> {
            if (isActive) {
                ServiceHelper.startService(this, LockAppService.class, Common.ACTION_CREATE_CAMERA);
            } else {
                ReceiverLocalHelper.sendBroadcast(this, ReceiverLocalHelper.ReceiverType.ACTION_CHANGE_INTRUDER_SETTING);
            }
            getSupportFragmentManager().beginTransaction().replace(binding.containerFragmentIntruder.getId(), !isActive ? new IntruderIntroFragment() : new IntruderFragment()).commit();
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionHelper.REQUEST_CODE_CAMERA:
                if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    IntruderSetting.getInstance(this).setValue(true);
                    viewModel.setIsActive(true);
                }
                break;
        }
    }
}