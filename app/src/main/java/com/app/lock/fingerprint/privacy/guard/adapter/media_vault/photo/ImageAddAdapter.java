package com.app.lock.fingerprint.privacy.guard.adapter.media_vault.photo;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemImageAddBinding;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;

public class ImageAddAdapter extends RecyclerView.Adapter<ImageAddAdapter.Viewholder> {
    public static final String PAYLOAD_CHOOSE_CHANGE = "PAYLOAD_CHOOSE_CHANGE";
    private List<String> list;
    private List<String> listChoose;
    private Callback callback;

    public ImageAddAdapter(List<String> list, List<String> listChoose, Callback callback) {
        this.list = list;
        this.listChoose = listChoose;
        this.callback = callback;
    }

    @NonNull
    @NotNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new Viewholder(ItemImageAddBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull Viewholder holder, int position) {
        String path = list.get(position);
        File imageFile = new File(path);
        Boolean isChoose = listChoose.contains(path);
        holder.bind(imageFile, isChoose);

        setPadding(holder, position);
        holder.itemView.setOnClickListener(view -> callback.onItemClick(position));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull Viewholder holder, int position, @NonNull @NotNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            String payload = (String) payloads.get(0);
            if (payload.equals(PAYLOAD_CHOOSE_CHANGE)) {
                String path = list.get(position);
                Boolean isChoose = listChoose.contains(path);
                holder.binding.setIsChoose(isChoose);
                holder.itemView.setOnClickListener(view -> callback.onItemClick(position));
            }
        }
    }

    private void setPadding(Viewholder holder, int position) {
        int l = 0;
        int t = 0;
        int r = 0;
        int b = 0;

        if (position < 4) {
            t = 50;
        } else {
            t = 5;
        }

        if (position > getItemCount() - 4) {
            b = 50;
        } else {
            b = 5;
        }
        int index = position % 4;

        switch (index) {
            case 0:
                l = 10;
                break;
            case 1:
                l = 10;
                r = 5;
                break;
            case 2:
                l = 5;
                r = 10;
                break;
            case 3:
                r = 10;
                break;
        }

        holder.itemView.setPadding(l, t, r, b);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private final ItemImageAddBinding binding;

        public Viewholder(@NonNull @NotNull ItemImageAddBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(File imageFile, Boolean isChoose) {
            binding.setImageFile(imageFile);
            binding.setIsChoose(isChoose);
            binding.executePendingBindings();
        }
    }

    public interface Callback {
        void onItemClick(int pos);
    }
}
