package com.app.lock.fingerprint.privacy.guard.utils;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

public class CheckEvent {
    public static void checkRate(Context context, int star, String comment) {
        Bundle bundle = new Bundle();
        bundle.putString("event_type", "rated");
        bundle.putString("star", star + " star");
        bundle.putString("comment", comment);
        FirebaseAnalytics.getInstance(context).logEvent("prox_rating_layout", bundle);
    }

    public static void checkHome(Context context, String itemName) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "Home");
        bundle.putString("Item_name", "Home");
        FirebaseAnalytics.getInstance(context).logEvent("Click_Item", bundle);
    }

    public static void checkAppLock(Context context, int numberApp) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "App lock");
        bundle.putString("item_name", "Tick App");
        bundle.putString("Number", String.valueOf(numberApp));
        FirebaseAnalytics.getInstance(context).logEvent("Click_Item", bundle);
    }

    public static void checkMediaVault(Context context, int numberFile, int numberClickile) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "Media Vault");
        bundle.putString("item_name", "File lock");
        bundle.putString("Number", String.valueOf(numberFile));
        bundle.putString("Number", String.valueOf(numberClickile));
        FirebaseAnalytics.getInstance(context).logEvent("Click_Item", bundle);
    }

    public static void checkTheme(Context context) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "theme");
        bundle.putString("item_name", "Click_Apply");
        FirebaseAnalytics.getInstance(context).logEvent("Click_Item", bundle);
    }

    public static void checkUnlock(Context context) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "Unlock");
        bundle.putString("item_name", "Unlock");
        FirebaseAnalytics.getInstance(context).logEvent("Click_Item", bundle);
    }

    public static void checkFakeIcon(Context context) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "Fake icon");
        bundle.putString("item_name", "Click_Submit");
        FirebaseAnalytics.getInstance(context).logEvent("Layout_Fake_icon", bundle);
    }

    public static void checkPasswordSafe(Context context) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "Password Safe");
        bundle.putString("item_name", "Click_Done");
        FirebaseAnalytics.getInstance(context).logEvent("Layout_Password_Safe", bundle);
    }

    public static void checkIntruder(Context context) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "Intruder Sefie");
        bundle.putString("item_name", "Click_Turn_on");
        FirebaseAnalytics.getInstance(context).logEvent("Layout_Intruder_Sefie", bundle);
    }

    public static void checkChangePassword(Context context) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "Change Password");
        bundle.putString("item_name", "Done");
        FirebaseAnalytics.getInstance(context).logEvent("Layout_Change_Password", bundle);
    }

    public static void checkChangeLockType(Context context) {
        Bundle bundle = new Bundle();
        bundle.putString("Layout", "Change Lock Type");
        bundle.putString("item_name", "Done");
        FirebaseAnalytics.getInstance(context).logEvent("Layout_Change_Lock_Type", bundle);
    }
}
