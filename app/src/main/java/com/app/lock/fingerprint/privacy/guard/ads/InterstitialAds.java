package com.app.lock.fingerprint.privacy.guard.ads;


import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.ads.control.Admod;
import com.ads.control.funtion.AdCallback;
import com.app.lock.R;
import com.app.lock.fingerprint.privacy.guard.application.MyApplication;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;

public class InterstitialAds {

    private static InterstitialAds INSTANCE;
    private static InterstitialAd mInterstitialAd;
    private static CustomCallback customCallback;
    private static Context context;

    public static InterstitialAds getInstance(Activity activity1, CustomCallback customCallback1) {
        if (INSTANCE == null) {
            INSTANCE = new InterstitialAds(activity1, customCallback1);
        } else {
            context = activity1;
            customCallback = customCallback1;
        }
        return INSTANCE;
    }

    private InterstitialAds(Activity activity, CustomCallback customCallback) {
        this.context = activity;
        this.customCallback = customCallback;
        loadAds();
    }

    private static void loadAds() {
        if (MyApplication.isShowAds) {
            mInterstitialAd = Admod.getInstance().getInterstitalAds(context, context.getString(R.string.id_interstitial_click_function), new AdCallback() {
                @Override
                public void onAdFailedToLoad(LoadAdError i) {
                    super.onAdFailedToLoad(i);
                    mInterstitialAd = null;
                    customCallback.onAdFailedToLoad(i);
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    customCallback.onAdLoaded();
                }
            });
        } else {
            mInterstitialAd = null;
        }

    }

    public void showAds() {
        if (mInterstitialAd != null && MyApplication.isShowAds) {
            Admod.getInstance().forceShowInterstitial(context, mInterstitialAd, new AdCallback() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    customCallback.onPostShow();
                }

                @Override
                public void onAdFailedToLoad(LoadAdError i) {
                    super.onAdFailedToLoad(i);
                    customCallback.onPostShow();
                }
            });
        } else {
            mInterstitialAd = null;
            customCallback.onPostShow();
        }

    }

    public abstract static class CustomCallback {
        public void onAdLoaded() {
        }

        public void onAdFailedToLoad(LoadAdError i) {
        }

        public void onPostShow() {
            if (mInterstitialAd != null) {
                loadAds();
            }
        }
    }

}
