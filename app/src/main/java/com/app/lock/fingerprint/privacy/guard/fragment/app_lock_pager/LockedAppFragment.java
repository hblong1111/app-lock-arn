package com.app.lock.fingerprint.privacy.guard.fragment.app_lock_pager;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.lock.databinding.FragmentLockedAppBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.home_app.LockAppAdapter;
import com.app.lock.fingerprint.privacy.guard.model.setting.ListAppLockSetting;
import com.app.lock.fingerprint.privacy.guard.view_model.HomeViewModel;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class LockedAppFragment extends Fragment {
    private FragmentLockedAppBinding binding;
    private LockAppAdapter adapter;
    private List<String> listAppLock;


    private LockAppAdapter.Callback callback;
    private Context context;

    private HomeViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentLockedAppBinding.inflate(inflater, container, false);

        context = getActivity();

        viewModel = new ViewModelProvider(getActivity()).get(HomeViewModel.class);

        setupRCV();

        return binding.getRoot();
    }

    private void setupRCV() {
        callback = (pos) -> {
            try {
                String label = (String) context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(listAppLock.get(pos), 0));
                Toasty.error(requireActivity(), "UnLocked: " + label, Toast.LENGTH_SHORT, true).show();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (pos < adapter.getItemCount()) {
                listAppLock.remove(pos);
                adapter.notifyItemRemoved(pos);
                adapter.notifyItemRangeChanged(0,adapter.getItemCount(),adapter.PAYLOAD_UPDATE_POS_EVENT);
            }
            ListAppLockSetting.getInstance(context).setValue(listAppLock);
            viewModel.setListAppLock(listAppLock);
        };
        listAppLock = new ArrayList<>();
        adapter = new LockAppAdapter(listAppLock, callback);

        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new LinearLayoutManager(context));


        viewModel.getListAppLock().observe(getActivity(), strings -> {
            binding.rcv.setVisibility(strings.size() != 0 ? View.VISIBLE : View.INVISIBLE);
            binding.groupEmpty.setVisibility(strings.size() == 0 ? View.VISIBLE : View.INVISIBLE);
            if (strings.equals(listAppLock)) {
                return;
            }
            for (String appPackage :
                    strings) {
                if (!listAppLock.contains(appPackage)) {
                    listAppLock.add(0, appPackage);
                    adapter.notifyItemInserted(0);
                    adapter.notifyItemChanged(1);
                    binding.rcv.getLayoutManager().scrollToPosition(0);
                }
            }

            for (int i = 0; i < listAppLock.size(); i++) {
                String app = listAppLock.get(i);
                if (!strings.contains(app)) {
                    listAppLock.remove(i);
                    adapter.notifyItemRemoved(i);
                    break;
                }
            }
        });

    }
}
