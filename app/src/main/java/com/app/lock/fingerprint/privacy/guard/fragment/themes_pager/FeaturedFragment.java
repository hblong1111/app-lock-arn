package com.app.lock.fingerprint.privacy.guard.fragment.themes_pager;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.lock.R;
import com.app.lock.databinding.FragmentThemesFeaturedBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.home_theme.ThemeFeatureAdapter;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.model.ThemeInstallResult;
import com.app.lock.fingerprint.privacy.guard.view_model.HomeViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FeaturedFragment extends Fragment {
    private FragmentActivity activity;
    private FragmentThemesFeaturedBinding binding;
    private HomeViewModel viewModel;
    private ThemeFeatureAdapter adapter;
    private Theme themeChoose;
    private ThemeFeaturedDetailFragment fragmentDetail;
    private ThemeInstallResult result;


    private ThemeFeatureAdapter.Callback callback = pos -> {
        viewModel.setThemeFeaturedPreview(result.getImages().get(pos));
        activity.getSupportFragmentManager().beginTransaction().replace(R.id.containerFragmentHome, fragmentDetail).addToBackStack(null).commit();
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = requireActivity();
        binding = FragmentThemesFeaturedBinding.inflate(inflater);
        viewModel = new ViewModelProvider(activity).get(HomeViewModel.class);

        fragmentDetail = new ThemeFeaturedDetailFragment();

        setupRCV();

        viewModel.getThemeResult().observe(activity, themeFeatured -> {
            binding.groupError.setVisibility(themeFeatured == null || themeFeatured.getImages() == null || themeFeatured.getImages().isEmpty() ? View.VISIBLE : View.INVISIBLE);
            binding.rcv.setVisibility(themeFeatured == null || themeFeatured.getImages() == null || themeFeatured.getImages().isEmpty() ? View.INVISIBLE : View.VISIBLE);
            if (themeFeatured != null && themeFeatured.getImages() != null && !themeFeatured.getImages().isEmpty() && !themeFeatured.equals(result)) {
                Collections.sort(themeFeatured.getImages(), (o1, o2) -> (int) (o2.getCreatedAt() - o1.getCreatedAt()));
                if (result.getImages() == null) {
                    result.setImages(new ArrayList<>());
                }
                for (ThemeInstallResult.Image theme :
                        themeFeatured.getImages()) {
                    if (!result.getImages().contains(theme)) {
                        result.getImages().add(theme);
                        adapter.notifyItemInserted(adapter.getItemCount());
                    }
                }
            }
        });

        viewModel.getThemeChoose().observe(activity, theme -> {
            if (!theme.equals(themeChoose)) {
                themeChoose.setBackgroundFile(theme.getBackgroundFile());
                themeChoose.setPreviewFile(theme.getPreviewFile());

                adapter.notifyItemRangeChanged(0, adapter.getItemCount(), adapter.PAYLOAD_UPDATE_THEME_CHOOSE);
            }
        });
        return binding.getRoot();
    }

    private void setupRCV() {
        themeChoose = new Theme();
        result = new ThemeInstallResult();
        adapter = new ThemeFeatureAdapter(result, themeChoose, callback);
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new GridLayoutManager(activity, 3));
    }
}
