package com.app.lock.fingerprint.privacy.guard.adapter.home_app;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemAppLockBinding;
import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class LockAppAdapter extends RecyclerView.Adapter<LockAppAdapter.ViewHolder> {
    public static final String PAYLOAD_UPDATE_POS_EVENT = "PAYLOAD_UPDATE_POS_EVENT";
    private List<String> list;
    private Callback callback;

    public LockAppAdapter(List<String> list, Callback callback) {
        this.list = list;
        this.callback = callback;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemAppLockBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        setPaddingItem(holder, position);
        String packageName = list.get(position);
        holder.binding.setPackageName(packageName);

        holder.itemView.setOnClickListener(v -> callback.onItemClick(position));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            try {
                String payload = (String) payloads.get(0);
                if (payload.equals(PAYLOAD_UPDATE_POS_EVENT)) {
                    holder.itemView.setOnClickListener(v -> callback.onItemClick(position));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void setPaddingItem(@NotNull ViewHolder holder, int position) {
        if (position == 0) {
            holder.itemView.setPadding(0, 50, 0, 0);
        } else if (position == getItemCount() - 1) {
            holder.itemView.setPadding(0, 15, 0, 50);
        } else {
            holder.itemView.setPadding(0, 15, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemAppLockBinding binding;

        public ViewHolder(@NonNull @NotNull ItemAppLockBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }

    @BindingAdapter("loadLabelWithPackageName")
    public static void loadLabelWithPackageName(TextView textView, String packageName) {
        Context context = textView.getContext();
        PackageManager packageManager = context.getPackageManager();
        try {
            ApplicationInfo info = packageManager.getApplicationInfo(packageName, 0);
            String label = (String) info.loadLabel(packageManager);
            textView.setText(label);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter("loadIconAppWithPackageName")
    public static void loadIconAppWithPackageName(ImageView imageView, String packageName) {
        Context context = imageView.getContext();
        PackageManager packageManager = context.getPackageManager();
        try {
            ApplicationInfo info = packageManager.getApplicationInfo(packageName, 0);
            Drawable icon = info.loadIcon(packageManager);
            Glide.with(context).load(icon).into(imageView);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public interface Callback {
        void onItemClick(int pos);
    }

}
