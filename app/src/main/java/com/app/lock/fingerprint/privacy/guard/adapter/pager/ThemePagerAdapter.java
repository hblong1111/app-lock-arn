package com.app.lock.fingerprint.privacy.guard.adapter.pager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.lock.fingerprint.privacy.guard.fragment.themes_pager.FeaturedFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.themes_pager.InstalledFragment;

public class ThemePagerAdapter extends FragmentStatePagerAdapter {
    public ThemePagerAdapter (@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem (int position) {

        if (position == 0) {
            return new FeaturedFragment();
        }
        return new InstalledFragment();
    }

    @Override
    public int getCount () {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle (int position) {
        switch (position) {
            case 0:
                return "Featured";
            case 1:
                return "Installed";
        }
        return super.getPageTitle(position);
    }
}
