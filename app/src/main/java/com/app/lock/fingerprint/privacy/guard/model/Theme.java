package com.app.lock.fingerprint.privacy.guard.model;

import android.util.Log;

import java.io.File;
import java.util.Objects;

public class Theme {
    private File backgroundFile;
    private File previewFile;

    public Theme () {
    }

    public Theme (File backgroundFile, File previewFile) {
        this.backgroundFile = backgroundFile;
        this.previewFile = previewFile;
    }

    public File getBackgroundFile () {
        return backgroundFile;
    }

    public void setBackgroundFile (File backgroundFile) {
        this.backgroundFile = backgroundFile;
    }

    public File getPreviewFile () {
        return previewFile;
    }

    public void setPreviewFile (File previewFile) {
        this.previewFile = previewFile;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Theme theme = (Theme) o;
        return backgroundFile.equals(theme.backgroundFile) && previewFile.equals(theme.previewFile);
    }

    @Override
    public int hashCode () {
        return Objects.hash(backgroundFile, previewFile);
    }
}
