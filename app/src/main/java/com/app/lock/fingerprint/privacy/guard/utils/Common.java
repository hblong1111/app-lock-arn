package com.app.lock.fingerprint.privacy.guard.utils;

public class Common {
    public static final String ACTION_CREATE_CAMERA = "ACTION_CREATE_CAMERA";

    public static final String ACTION_TAKE_INTRUDER = "ACTION_TAKE_INTRUDER";
    public static long TIME_DELAY_LOAD_NATIVE_ADS = 2500;
    public static String THEME_BASE;
    public static String PATH_IMAGES;
    public static String PATH_DOCUMENTS;
    public static String PATH_VIDEOS;
    public static String PATH_MUSIC;
    public static String PATH_FILES_UN_LOCK;
}
