package com.app.lock.fingerprint.privacy.guard.adapter.media_vault.photo;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemImageBinding;
import com.blankj.utilcode.util.ScreenUtils;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    public static final String PAYLOAD_CHANGE_SELECTED = "PAYLOAD_CHANGE_SELECTED";
    private List<String> list;
    private List<String> listSelected;
    private Callback callback;
    private boolean isEdit;

    public ImageAdapter(List<String> list, List<String> listSelected, Callback callback) {
        this.list = list;
        this.listSelected = listSelected;
        this.callback = callback;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemImageBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {

        holder.bind(new File(list.get(position)), isEdit, listSelected.contains(list.get(position)));
        setPadding(holder, position);
        holder.itemView.setOnClickListener(view -> {
            callback.onClick(position);
        });

        holder.itemView.setOnLongClickListener(view -> {
            callback.onLongClick(position);
            return false;
        });
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            try {
                String payload = (String) payloads.get(0);
                if (payload.equals(PAYLOAD_CHANGE_SELECTED)) {
                    holder.binding.setIsEdit(isEdit);
                    holder.binding.setSelected(listSelected.contains(list.get(position)));
                    holder.binding.executePendingBindings();

                    setPadding(holder, position);
                    holder.itemView.setOnClickListener(view -> {
                        callback.onClick(position);
                    });

                    holder.itemView.setOnLongClickListener(view -> {
                        callback.onLongClick(position);
                        return false;
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setPadding(ViewHolder holder, int position) {
        int l;
        int t;
        int r;
        int b;
        if (position < 2) {
            t = 50;
        } else {
            t = 15;
        }

        if (position >= getItemCount() - 2) {
            b = ScreenUtils.getScreenHeight() / 4;
        } else {
            b = 15;
        }

        if (position % 2 == 0) {
            l = 20;
            r = 10;
        } else {
            r = 20;
            l = 10;
        }


        holder.itemView.setPadding(l, t, r, b);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
        notifyItemRangeChanged(0, getItemCount(), PAYLOAD_CHANGE_SELECTED);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemImageBinding binding;

        public ViewHolder(@NonNull @NotNull ItemImageBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(File imageFile, boolean isEdit, boolean isChoose) {
            binding.setImageFile(imageFile);
            binding.setIsEdit(isEdit);
            if (isEdit) {
                binding.setSelected(isChoose);
            }
            binding.executePendingBindings();
        }
    }

    public interface Callback {
        void onClick(int pos);

        void onLongClick(int position);
    }
}
