package com.app.lock.fingerprint.privacy.guard.model.setting;

import android.content.Context;

import com.app.lock.fingerprint.privacy.guard.utils.SharedUtils;

public class RateSetting extends AppSetting {
    private static RateSetting INSTANCE;
    private Context context;

    public static RateSetting getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new RateSetting(context);
        }
        return INSTANCE;
    }

    private RateSetting (Context context) {
        this.context = context;
    }

    @Override
    protected String getKey() {
        return super.getKey();
    }

    @Override
    public void setValue(Object password) {
        try {
            SharedUtils.getInstance(context).putBoolean(getKey(), (Boolean) password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean getValue() {
        return SharedUtils.getInstance(context).getBoolean(getKey(), false);
    }
}
