package com.app.lock.fingerprint.privacy.guard.adapter.pager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.lock.fingerprint.privacy.guard.fragment.app_lock_pager.AppLockPagerFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.themes_pager.ThemePagerFragment;

public class HomePagerAdapter extends FragmentStatePagerAdapter {
    public HomePagerAdapter (@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem (int position) {
        if (position == 1) {
            return new ThemePagerFragment();
        }
        return new AppLockPagerFragment();
    }

    @Override
    public int getCount () {
        return 2;
    }
}
