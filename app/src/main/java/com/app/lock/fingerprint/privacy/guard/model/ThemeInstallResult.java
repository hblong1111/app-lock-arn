package com.app.lock.fingerprint.privacy.guard.model;

import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class ThemeInstallResult {


    @SerializedName("base")
    @Expose
    private String base;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThemeInstallResult that = (ThemeInstallResult) o;
        return Objects.equals(base, that.base) && Objects.equals(images, that.images);
    }

    @Override
    public int hashCode() {
        return Objects.hash(base, images);
    }

    public class Image {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("createdAt")
        @Expose
        private Long createdAt;
        @SerializedName("preview")
        @Expose
        private String preview;
        @SerializedName("background")
        @Expose
        private String background;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Long getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(Long createdAt) {
            this.createdAt = createdAt;
        }

        public String getPreview() {
            return preview;
        }

        public void setPreview(String preview) {
            this.preview = preview;
        }

        public String getBackground() {
            return background;
        }

        public void setBackground(String background) {
            this.background = background;
        }

        public String getName() {
            return getCreatedAt() + ".png";
        }

        public String getPreviewLink() {
            return Common.THEME_BASE + getPreview();
        }

        public String getBackgroundLink() {

            return Common.THEME_BASE + getBackground();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Image image = (Image) o;
            return Objects.equals(id, image.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, createdAt, preview, background);
        }
    }

}
