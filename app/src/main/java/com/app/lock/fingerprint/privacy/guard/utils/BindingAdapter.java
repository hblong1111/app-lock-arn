package com.app.lock.fingerprint.privacy.guard.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.app.lock.R;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;
import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;

public class BindingAdapter {
    @androidx.databinding.BindingAdapter("loadImageWithDrawable")
    public static void loadImageWithDrawable(ImageView imageView, Drawable drawable) {
        Glide.with(imageView).load(drawable).into(imageView);
    }

    @androidx.databinding.BindingAdapter("loadImageWithLink")
    public static void loadImageWithLink(ImageView imageView, String linkImage) {
        Glide.with(imageView).load(linkImage).into(imageView);
    }

    @androidx.databinding.BindingAdapter("loadImageWithFile")
    public static void loadImageWithFile(ImageView imageView, File file) {
        Glide.with(imageView).load(file).into(imageView);
    }

    @androidx.databinding.BindingAdapter("loadIconChangePasswordSetting")
    public static void loadIconChangePasswordSetting(ImageView imageView, LockTypeSetting.LockType lockType) {
        Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), lockType == LockTypeSetting.LockType.TYPE_PATTERN ? R.drawable.ic_change_pattern_setting : R.drawable.ic_change_pin_code_setting);
        Glide.with(imageView).load(drawable).into(imageView);
    }

    @androidx.databinding.BindingAdapter("loadDateWithFile")
    public static void loadDateWithFile(TextView textView, File file) {
        long date = file.lastModified();
        String dateString = new SimpleDateFormat("dd/MM/yyyy").format(date);
        textView.setText(dateString);
    }

    @androidx.databinding.BindingAdapter("loadTimeWithFile")
    public static void loadTimeWithFile(TextView textView, File file) {
        long date = file.lastModified();
        String dateString = new SimpleDateFormat("hh:mm a").format(date);
        textView.setText(dateString);
    }

    @androidx.databinding.BindingAdapter("loadTimeAndDateWithFile")
    public static void loadTimeAndDateWithFile(TextView textView, File file) {
        long date = file.lastModified();
        String dateString = new SimpleDateFormat("dd/MM/yyyy").format(date);
        String timeString = new SimpleDateFormat("hh:mm a").format(date);
        textView.setText(timeString + ", " + dateString);
    }

    @androidx.databinding.BindingAdapter("loadImageWithPackageName")
    public static void loadImageWithPackageName(ImageView imageView, String packageName) {
        Context context = imageView.getContext();
        Drawable icon = null;
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo infoApp = packageManager.getApplicationInfo(packageName, 0);
            icon = infoApp.loadIcon(packageManager);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (icon != null) {
            Glide.with(context).load(icon).into(imageView);
        } else {
            imageView.setBackgroundColor(ContextCompat.getColor(context, R.color.black));
        }
    }

    @androidx.databinding.BindingAdapter("loadLabelWithPackageName")
    public static void loadLabelWithPackageName(TextView textView, String packageName) {
        Context context = textView.getContext();
        String label = null;
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo infoApp = packageManager.getApplicationInfo(packageName, 0);
            label = (String) infoApp.loadLabel(packageManager);
            textView.setText(label);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
