package com.app.lock.fingerprint.privacy.guard.fragment.setting;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.app.lock.R;
import com.app.lock.databinding.FragmentSettingBinding;
import com.app.lock.fingerprint.privacy.guard.activity.ChangeLockTypeActivity;
import com.app.lock.fingerprint.privacy.guard.activity.ChangePasswordActivity;
import com.app.lock.fingerprint.privacy.guard.activity.IntruderActivity;
import com.app.lock.fingerprint.privacy.guard.activity.MediaVaultActivity;
import com.app.lock.fingerprint.privacy.guard.activity.PasswordSafeActivity;
import com.app.lock.fingerprint.privacy.guard.activity.PermissionListActivity;
import com.app.lock.fingerprint.privacy.guard.activity.RemoveAdsActivity;
import com.app.lock.fingerprint.privacy.guard.ads.InterstitialAds;
import com.app.lock.fingerprint.privacy.guard.application.MyApplication;
import com.app.lock.fingerprint.privacy.guard.fragment.FakeIconFragment;
import com.app.lock.fingerprint.privacy.guard.model.PasswordSafe;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ShowIconSetting;
import com.google.android.gms.ads.LoadAdError;

public class SettingFragment extends Fragment implements View.OnClickListener {
    private FragmentSettingBinding binding;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = requireActivity();
        binding = FragmentSettingBinding.inflate(inflater);

        binding.switchShowIcon.setChecked(ShowIconSetting.getInstance(context).getValue());

        binding.switchShowIcon.setOnCheckedChangeListener((compoundButton, b) -> {
            ShowIconSetting.getInstance(context).setValue(b);
        });

        binding.btnPasswordSafe.setOnClickListener(this);
        binding.btnFakeIcon.setOnClickListener(this);
        binding.btnChangePassword.setOnClickListener(this);
        binding.btnChangeLockType.setOnClickListener(this);
        binding.btnVisilityIcon.setOnClickListener(this);
        binding.btnRemoveAds.setOnClickListener(this);
        binding.btnPermissionList.setOnClickListener(this);
        binding.btnMediaVault.setOnClickListener(this);
        binding.btnIntruder.setOnClickListener(this);
        binding.btnBack.setOnClickListener(this);

        binding.getRoot().setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.setTypeLock(LockTypeSetting.getInstance(context).getValue());
        binding.executePendingBindings();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRemoveAds:

                if (MyApplication.isShowAds) {
                    startActivity(new Intent(requireActivity(), RemoveAdsActivity.class));
                } else {
                    showDialogAlertSubscribe();
                }
                break;
            case R.id.btnBack:
                requireActivity().onBackPressed();
                break;
            case R.id.btnPermissionList:
                startActivity(new Intent(requireActivity(), PermissionListActivity.class));
                break;
            case R.id.btnChangeLockType:
            case R.id.btnFakeIcon:
            case R.id.btnPasswordSafe:
            case R.id.btnIntruder:
                InterstitialAds.getInstance(requireActivity(), new InterstitialAds.CustomCallback() {
                    @Override
                    public void onPostShow() {
                        handlerClickShowAds(view.getId());
                    }
                }).showAds();
                break;
            case R.id.btnChangePassword:
                startActivity(new Intent(requireActivity(), ChangePasswordActivity.class));
                break;
            case R.id.btnVisilityIcon:
                binding.switchShowIcon.setChecked(!binding.switchShowIcon.isChecked());
                break;
            case R.id.btnMediaVault:
                startActivity(new Intent(requireActivity(), MediaVaultActivity.class));
                break;
        }
    }


    private void showDialogAlertSubscribe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setMessage("You are using the upgrade, if you want to check the information, please visit the store's application to check. Thank you very much!");

        builder.setPositiveButton("To the Store", (dialog, which) -> {
            dialog.dismiss();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/account/subscriptions")));
            } catch (android.content.ActivityNotFoundException anfe) {
                Toast.makeText(requireActivity(), "An Error!", Toast.LENGTH_SHORT).show();
            }

        });
        builder.setNegativeButton("Ok", (dialog, which) -> {
            dialog.dismiss();
        });

        builder.create().show();
    }

    private void handlerClickShowAds(int id) {
        switch (id) {

            case R.id.btnIntruder:
                startActivity(new Intent(requireActivity(), IntruderActivity.class));
                break;
            case R.id.btnChangeLockType:
                startActivity(new Intent(requireActivity(), ChangeLockTypeActivity.class));
                break;
            case R.id.btnFakeIcon:
                requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerFragmentHome, new FakeIconFragment()).addToBackStack(null).commit();
                break;
            case R.id.btnPasswordSafe:
                requireActivity().startActivity(new Intent(requireActivity(), PasswordSafeActivity.class));
                break;
        }
    }
}
