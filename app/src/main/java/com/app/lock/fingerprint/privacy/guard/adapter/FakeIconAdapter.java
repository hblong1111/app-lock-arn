package com.app.lock.fingerprint.privacy.guard.adapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemFakeIconBinding;
import com.app.lock.fingerprint.privacy.guard.model.setting.FakeIconSetting;
import com.blankj.utilcode.util.ScreenUtils;

import java.util.List;

public class FakeIconAdapter extends RecyclerView.Adapter<FakeIconAdapter.ViewHolder> {
    private List<IconApp> list;
    private List<String> pkgList;
    private Callback callback;

    public FakeIconAdapter(List<IconApp> list, List<String> pkgList, Callback callback) {
        this.list = list;
        this.pkgList = pkgList;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemFakeIconBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        setPadding(holder, position);
        boolean select = list.get(position).getPkgName().equals(FakeIconSetting.getInstance(holder.itemView.getContext()).getValue());
        holder.bind(list.get(position), select);
        holder.itemView.setOnClickListener(v -> {
            if (!select) {
                callback.onItemClick(list.get(position), pkgList);
            } else {
                Toast.makeText(v.getContext(), "Icon is being applied!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setPadding(ViewHolder holder, int position) {

        int l;
        int t;
        int r;
        int b;

        if (position < 2) {
            t = 50;
        } else {
            t = 10;
        }

        int number = getItemCount() - (getItemCount() / 2) * 2;
        if (position >= getItemCount() - number) {
            b = ScreenUtils.getScreenHeight() / 4;
        } else {
            b = 10;
        }

        if (position % 2 == 0) {
            l = 20;
            r = 10;
        } else {
            l = 10;
            r = 20;
        }

        holder.itemView.setPadding(l, t, r, b);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemFakeIconBinding binding;

        public ViewHolder(@NonNull ItemFakeIconBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(IconApp item, Boolean select) {
            binding.setItem(item);
            binding.setSelect(select);
            binding.executePendingBindings();
        }
    }


    public static class IconApp {
        private String name;
        private String pkgName;
        private Drawable drawable;

        public IconApp(String name, String pkgName, Drawable drawable) {
            this.name = name;
            this.pkgName = pkgName;
            this.drawable = drawable;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPkgName() {
            return pkgName;
        }

        public void setPkgName(String pkgName) {
            this.pkgName = pkgName;
        }

        public Drawable getDrawable() {
            return drawable;
        }

        public void setDrawable(Drawable drawable) {
            this.drawable = drawable;
        }
    }

    public interface Callback {
        void onItemClick(IconApp iconApp, List<String> pkgList);
    }
}
