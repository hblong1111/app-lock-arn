package com.app.lock.fingerprint.privacy.guard.view_custom.pin_lock;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.app.lock.databinding.ViewIndicatorPin6Binding;

import org.jetbrains.annotations.NotNull;

public class IndicatorPin6View extends ConstraintLayout {
    private ViewIndicatorPin6Binding binding;
    private Context context;


    public IndicatorPin6View (@NonNull @NotNull Context context) {
        super(context);
        initView();
    }

    public IndicatorPin6View (@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public IndicatorPin6View (@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public IndicatorPin6View (@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView () {
        context = getContext();
        binding = ViewIndicatorPin6Binding.inflate(LayoutInflater.from(context), this, true);

        setPinChange("");

    }

    public void setPinView (PinInputView pinView) {
        pinView.setCallbackIndicator(password -> setPinChange(password));
    }

    private void setPinChange (String password) {

        binding.tvPass6.setVisibility(INVISIBLE);
        binding.tvPass5.setVisibility(INVISIBLE);
        binding.tvPass4.setVisibility(INVISIBLE);
        binding.tvPass3.setVisibility(INVISIBLE);
        binding.tvPass2.setVisibility(INVISIBLE);
        binding.tvPass1.setVisibility(INVISIBLE);

        binding.viewCur6.setVisibility(INVISIBLE);
        binding.viewCur5.setVisibility(INVISIBLE);
        binding.viewCur4.setVisibility(INVISIBLE);
        binding.viewCur3.setVisibility(INVISIBLE);
        binding.viewCur2.setVisibility(INVISIBLE);
        binding.viewCur1.setVisibility(INVISIBLE);

        switch (password.length()) {
            case 6:
                binding.tvPass6.setVisibility(VISIBLE);
            case 5:
                binding.tvPass5.setVisibility(VISIBLE);
            case 4:
                binding.tvPass4.setVisibility(VISIBLE);
            case 3:
                binding.tvPass3.setVisibility(VISIBLE);
            case 2:
                binding.tvPass2.setVisibility(VISIBLE);
            case 1:
                binding.tvPass1.setVisibility(VISIBLE);
                break;
        }

        switch (password.length()) {
            case 5:
                binding.viewCur6.setVisibility(VISIBLE);
                break;

            case 4:
                binding.viewCur5.setVisibility(VISIBLE);
                break;

            case 3:
                binding.viewCur4.setVisibility(VISIBLE);
                break;

            case 2:
                binding.viewCur3.setVisibility(VISIBLE);
                break;

            case 1:
                binding.viewCur2.setVisibility(VISIBLE);
                break;

            case 0:
                binding.viewCur1.setVisibility(VISIBLE);
                break;
        }
    }

    private void hideAll () {
    }
}
