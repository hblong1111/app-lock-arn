package com.app.lock.fingerprint.privacy.guard.model.setting;

import android.content.Context;

import com.app.lock.fingerprint.privacy.guard.utils.SharedUtils;

public class PasswordSetting extends AppSetting {
    private static PasswordSetting INSTANCE;
    private Context context;

    public static PasswordSetting getInstance (Context context) {
        if (INSTANCE == null) {
            INSTANCE = new PasswordSetting(context);
        }
        return INSTANCE;
    }

    private PasswordSetting (Context context) {
        this.context = context;
    }

    @Override
    protected String getKey () {
        return super.getKey();
    }

    @Override
    public void setValue (Object password) {
        try {
            SharedUtils.getInstance(context).putString(getKey(), (String) password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getValue () {
        return SharedUtils.getInstance(context).getString(getKey());
    }
}
