package com.app.lock.fingerprint.privacy.guard.helper;

import android.Manifest;
import android.app.Activity;
import android.app.AppOpsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.databinding.BindingAdapter;


import com.app.lock.R;
import com.app.lock.databinding.ActivityDialogBinding;
import com.bumptech.glide.Glide;

import java.util.List;

public class PermissionHelper {
    public static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 13;
    public static final int CODE_REQUEST_PERMISSION_USAGE = 22;
    public static final int CODE_REQUEST_PERMISSION_OVERLAY = 33;
    public static final int REQUEST_CODE_AUTO_START = 44;
    public static final int REQUEST_CODE_CAMERA = 55;

    public static void requestCamera(Activity context) {
        ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_CAMERA);

    }

    public static boolean checkCamera(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkWriteExternalStorage(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else {
            return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PermissionChecker.PERMISSION_GRANTED;
        }
    }

    public static void requestWriteExternalStorage(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getString(R.string.permission_necessary));
            builder.setMessage(activity.getString(R.string.write_permission_descriptsion));
            builder.setPositiveButton("OK", (dialog, which) -> {
                dialog.dismiss();
                try {
                    activity.startActivityForResult(new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION, Uri.parse("package:" + activity.getPackageName())), REQUEST_CODE_WRITE_EXTERNAL_STORAGE);
                } catch (Exception e) {
                    activity.startActivityForResult(new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION), REQUEST_CODE_WRITE_EXTERNAL_STORAGE);
                }
            });
            builder.setNegativeButton("Cancel", (dialogInterface, i) -> {
                dialogInterface.dismiss();
                activity.onBackPressed();
            });
            builder.setCancelable(false);
            builder.create().show();
        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE_WRITE_EXTERNAL_STORAGE);
        }
    }


    public static boolean checkUsagePermission(Context context) {
        boolean granted = false;
        AppOpsManager appOps = null;
        try {
            appOps = (AppOpsManager) context
                    .getSystemService(Context.APP_OPS_SERVICE);
        } catch (Exception e) {
            return false;
        }
        int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                android.os.Process.myUid(), context.getPackageName());

        if (mode == AppOpsManager.MODE_DEFAULT) {
            granted = (context.checkCallingOrSelfPermission(Manifest.permission.PACKAGE_USAGE_STATS) == PackageManager.PERMISSION_GRANTED);
        } else {
            granted = (mode == AppOpsManager.MODE_ALLOWED);
        }
        return granted;
    }

    public static boolean canDrawOverlays(Context context) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return context != null && Settings.canDrawOverlays(context);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {//USING APP OPS MANAGER
            AppOpsManager manager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            if (manager != null) {
                try {
                    int result = manager.checkOp(AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW, Binder.getCallingUid(), context.getPackageName());
                    return result == AppOpsManager.MODE_ALLOWED;
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }
        }


        try {//IF This Fails, we definitely can't do it
            WindowManager mgr = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            if (mgr == null) return false; //getSystemService might return null
            View viewToAdd = new View(context);
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(0, 0, Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ?
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSPARENT);
            viewToAdd.setLayoutParams(params);
            mgr.addView(viewToAdd, params);
            mgr.removeView(viewToAdd);
            return true;
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }

        return false;
    }

    public static void requestUsagePermission(Activity activity) {
        if (checkUsagePermission(activity)) {
            return;
        }
        try {
            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS, Uri.parse("package:" + activity.getPackageName()));
            activity.startActivityForResult(intent, CODE_REQUEST_PERMISSION_USAGE);
        } catch (Exception e) {
            try {
                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS, null);
                activity.startActivityForResult(intent, CODE_REQUEST_PERMISSION_USAGE);
            } catch (Exception exception) {
                return;
            }
        }

        new Thread(() -> {
            while (true) {
                if (PermissionHelper.checkUsagePermission(activity)) {
                    try {
                        activity.finishActivity(CODE_REQUEST_PERMISSION_USAGE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }).start();
        startActivityTutorial(activity, 100);

    }

    public static void startActivityTutorial(Activity activity, long delay) {
        new Thread(() -> {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            activity.startActivity(new Intent(activity, DialogActivity.class));
        }).start();
    }


    public static void requestOverlayPermission(Activity activity) {
        if (canDrawOverlays(activity)) {
            return;
        }
        try {
            Intent intent1 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + activity.getPackageName()));
            activity.startActivityForResult(intent1, CODE_REQUEST_PERMISSION_OVERLAY);
        } catch (Exception e) {
            Intent intent1 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            activity.startActivityForResult(intent1, CODE_REQUEST_PERMISSION_OVERLAY);
        }

        new Thread(() -> {
            while (true) {
                if (PermissionHelper.canDrawOverlays(activity)) {
                    try {
                        activity.finishActivity(CODE_REQUEST_PERMISSION_OVERLAY);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                }
            }
        }).start();
        startActivityTutorial(activity, 1000);
    }


    public static void requestAutoStart(Activity activity) {
        try {
            Intent intent = new Intent();
            String manufacturer = Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }

            List<ResolveInfo> list = activity.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                try {
                    activity.startActivityForResult(intent, REQUEST_CODE_AUTO_START);
                } catch (Exception e) {
                    e.printStackTrace();
                    activity.startActivity(intent);
                }

                startActivityTutorial(activity, 1000);
            }
        } catch (Exception e) {
            Log.e("exc", String.valueOf(e));
        }
    }


    //check device does exist Auto Start Permission
    public static boolean checkDeviceAutoStart(Activity activity) {
        try {
            Intent intent = new Intent();
            String manufacturer = Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }

            List<ResolveInfo> list = activity.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                return true;
            }
        } catch (Exception e) {
            Log.e("exc", String.valueOf(e));
            return false;
        }
        return false;
    }

    public static class DialogActivity extends AppCompatActivity implements View.OnClickListener {
        private ActivityDialogBinding binding;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            binding = ActivityDialogBinding.inflate(getLayoutInflater());
            setContentView(binding.getRoot());

            Window window = getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.width = wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(wlp);
            binding.btnClose.setOnClickListener(this);
            binding.btnGotIt.setOnClickListener(this);
            binding.setAppName(getString(R.string.app_name));
            Glide.with(this).load(R.drawable.gif_tutorial_permission).into(binding.img);

            try {
                Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_launcher);
                Glide.with(this).load(drawable).into(binding.imgIconApp);
            } catch (Exception e) {
                e.printStackTrace();
                Glide.with(this).load(R.mipmap.ic_launcher).into(binding.imgIconApp);

            }
        }

        @BindingAdapter("setTitle")
        public static void setTitle(TextView textView, String txt) {
            textView.setText("Find " + txt + " and turn it on");
        }

        @Override
        public void onClick(View v) {
            finish();
        }
    }
}
