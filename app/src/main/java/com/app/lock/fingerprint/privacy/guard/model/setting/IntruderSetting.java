package com.app.lock.fingerprint.privacy.guard.model.setting;

import android.content.Context;

import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.utils.SharedUtils;

public class IntruderSetting extends AppSetting {
    private static IntruderSetting INSTANCE;
    private Context context;

    public static IntruderSetting getInstance (Context context) {
        if (INSTANCE == null) {
            INSTANCE = new IntruderSetting(context);
        }
        return INSTANCE;
    }

    private IntruderSetting(Context context) {
        this.context = context;
    }

    @Override
    protected String getKey () {
        return super.getKey();
    }

    @Override
    public void setValue (Object password) {
        try {
            SharedUtils.getInstance(context).putBoolean(getKey(), (Boolean) password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean getValue () {
        return SharedUtils.getInstance(context).getBoolean(getKey(), false);
    }
}
