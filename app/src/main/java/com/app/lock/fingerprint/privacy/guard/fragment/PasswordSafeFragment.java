package com.app.lock.fingerprint.privacy.guard.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.lock.R;
import com.app.lock.databinding.FragmentPasswordSafeBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.PasswordSafeAdapter;
import com.app.lock.fingerprint.privacy.guard.db.AppDatabase;
import com.app.lock.fingerprint.privacy.guard.model.PasswordSafe;
import com.app.lock.fingerprint.privacy.guard.view_model.PasswordSafeViewModel;

import java.util.ArrayList;
import java.util.List;

public class PasswordSafeFragment extends Fragment implements View.OnClickListener {
    private FragmentPasswordSafeBinding binding;
    private PasswordSafeViewModel viewModel;
    private PasswordSafeAdapter adapter;
    private List<PasswordSafe> passwordList;

    private AppDatabase database;

    private PasswordSafe passwordEdit;
    private PasswordSafeAdapter.Callback callback = new PasswordSafeAdapter.Callback() {
        @Override
        public void onClickItemAdapter(int pos) {
            PasswordSafe passwordSafe = passwordList.get(pos);
            passwordEdit = passwordSafe;

            viewModel.setPasswordEdit(passwordSafe);
            requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerFragment, new PasswordEditFragment()).addToBackStack(null).commit();
        }

        @Override
        public void onRemoveItem(int pos) {
            database.passwordDao().delete(passwordList.get(pos));

            List<PasswordSafe> listNew = viewModel.getPasswordList().getValue();
            listNew.remove(passwordList.get(pos));

            viewModel.getPasswordList().postValue(listNew);


            binding.rcv.setVisibility(passwordList.isEmpty() ? View.INVISIBLE : View.VISIBLE);
            binding.groupEmpty.setVisibility(!passwordList.isEmpty() ? View.INVISIBLE : View.VISIBLE);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPasswordSafeBinding.inflate(inflater);
        viewModel = new ViewModelProvider(requireActivity()).get(PasswordSafeViewModel.class);
        database = AppDatabase.getInstance(requireActivity());
        //setup data Recycler View
        setupRCV();

        //handle change list password safe
        viewModel.getPasswordList().observe(requireActivity(), passwordSafes -> {
            Log.d("hblong", "PasswordSafeFragment | onCreateView: " + passwordSafes.size());

            binding.rcv.setVisibility(passwordSafes.isEmpty() ? View.INVISIBLE : View.VISIBLE);
            binding.groupEmpty.setVisibility(!passwordSafes.isEmpty() ? View.INVISIBLE : View.VISIBLE);
            if (!passwordSafes.equals(passwordList)) {
                passwordList.clear();
                passwordList.addAll(passwordSafes);
                requireActivity().runOnUiThread(() -> adapter.notifyDataSetChanged());
            }
        });


        viewModel.getPasswordEdit().observe(requireActivity(), passwordSafe -> {
            if (passwordEdit != null && !passwordEdit.equals(passwordSafe)) {
                int index = passwordList.indexOf(passwordEdit);
                adapter.notifyItemChanged(index);
            }
        });

        //config event view in fragment
        binding.btnBack.setOnClickListener(this);
        binding.btnAdd.setOnClickListener(this);


        return binding.getRoot();
    }


    private void setupRCV() {
        passwordList = new ArrayList<>();
        adapter = new PasswordSafeAdapter(passwordList, callback);
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new LinearLayoutManager(requireActivity()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                requireActivity().onBackPressed();
                break;
            case R.id.btnAdd:
                requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerFragment, new PasswordAddFragment()).addToBackStack(null).commit();
                break;
        }
    }
}