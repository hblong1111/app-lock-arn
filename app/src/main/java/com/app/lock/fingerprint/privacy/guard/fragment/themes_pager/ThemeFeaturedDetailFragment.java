package com.app.lock.fingerprint.privacy.guard.fragment.themes_pager;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.app.lock.R;
import com.app.lock.databinding.FragmentThemeFeaturedDetailBinding;
import com.app.lock.fingerprint.privacy.guard.ads.InterstitialAds;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.model.ThemeInstallResult;
import com.app.lock.fingerprint.privacy.guard.model.setting.ThemeSetting;
import com.app.lock.fingerprint.privacy.guard.utils.AppFileUtils;
import com.app.lock.fingerprint.privacy.guard.view_model.HomeViewModel;
import com.blankj.utilcode.util.BarUtils;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ThemeFeaturedDetailFragment extends Fragment implements View.OnClickListener {
    private FragmentThemeFeaturedDetailBinding binding;
    private HomeViewModel viewModel;
    private FragmentActivity activity;
    private int type;
    private ThemeInstallResult.Image themeFeatured;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = requireActivity();
        binding = FragmentThemeFeaturedDetailBinding.inflate(inflater);

        viewModel = new ViewModelProvider(getActivity()).get(HomeViewModel.class);

        viewModel.getThemeFeaturedPreview().observe(getActivity(), themeFeatured -> {
            this.themeFeatured = themeFeatured;
            if (getActivity() != null) {
                loadImageBackground(themeFeatured);
                observerThemeChoose(themeFeatured);
            }


        });

        binding.getRoot().setOnClickListener(this);
        binding.bntClose.setOnClickListener(this);
        binding.btnApply.setOnClickListener(this);


        return binding.getRoot();
    }

    private void observerThemeChoose(ThemeInstallResult.Image themeFeatured) {
        viewModel.getThemeChoose().observe(getActivity(), themeChoose -> {
            if (themeChoose.getBackgroundFile().getName().equals(themeFeatured.getName())) {
                setType(0);
            } else {
                boolean isDownload = checkThemeDownload(themeFeatured);
                setType(isDownload ? 1 : 2);
            }
        });
    }

    private boolean checkThemeDownload(ThemeInstallResult.Image themeFeatured) {
        boolean isDownload = false;
        List<Theme> listThemeInstall = viewModel.getListThemeInstall().getValue();
        for (Theme theme :
                listThemeInstall) {
            if (theme.getBackgroundFile().getName().equals(themeFeatured.getName())) {
                isDownload = true;
                break;
            }
        }
        return isDownload;
    }

    private void loadImageBackground(ThemeInstallResult.Image themeFeatured) {
        Glide.with(activity).load(themeFeatured.getPreviewLink()).into(binding.img);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bntClose:
                requireActivity().onBackPressed();
                break;
            case R.id.btnApply:
                loadAds();
                break;
        }
    }

    private void loadAds() {
        InterstitialAds.getInstance(requireActivity(), new InterstitialAds.CustomCallback() {
            @Override
            public void onPostShow() {
                super.onPostShow();

                if (themeFeatured == null) {
                    return;
                }
                switch (type) {
                    case 0:
                        Toast.makeText(activity, "Theme is applied!", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        applyTheme();
                        break;
                    default:
                        downloadAndApplyTheme();
                        break;
                }
            }
        }).showAds();
    }

    private void applyTheme() {
        File fileSaveBackground = new File(AppFileUtils.getThemeBackgroundRoot(activity), themeFeatured.getName());
        File fileSavePreview = new File(AppFileUtils.getThemePreviewRoot(activity), themeFeatured.getName());


        Theme themeChoose = new Theme();
        themeChoose.setBackgroundFile(fileSaveBackground);
        themeChoose.setPreviewFile(fileSavePreview);
        ThemeSetting.getInstance(getActivity()).setValue(themeChoose);
        viewModel.setThemeChoose(themeChoose);
        Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show();
        activity.onBackPressed();
    }

    private void downloadAndApplyTheme() {
        AtomicReference<KProgressHUD> dialogLoading = new AtomicReference<>();
        new Thread(() -> {
            activity.runOnUiThread(() -> {
                if (isAdded()) {
                    dialogLoading.set(KProgressHUD.create(activity)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setLabel("Please wait")
                            .setDetailsLabel("Downloading Theme")
                            .setCancellable(false)
                            .setAnimationSpeed(2)
                            .setDimAmount(0.5f)
                            .show());
                }
            });

            File fileSavePreview = new File(AppFileUtils.getThemePreviewRoot(activity), themeFeatured.getName());
            boolean resultDownloadPreview = saveImageToDevice(themeFeatured.getPreviewLink(), fileSavePreview);
            File fileSaveBackground = new File(AppFileUtils.getThemeBackgroundRoot(activity), themeFeatured.getName());
            boolean resultDownloadBackground = saveImageToDevice(themeFeatured.getBackgroundLink(), fileSaveBackground);

            if (resultDownloadBackground && resultDownloadPreview) {
                activity.runOnUiThread(() -> {
                    List<Theme> listThemeInstall = viewModel.getListThemeInstall().getValue();
                    listThemeInstall.add(0, new Theme(fileSaveBackground, fileSavePreview));
                    viewModel.setListThemeInstall(listThemeInstall);
                    Theme themeChoose = new Theme();
                    themeChoose.setBackgroundFile(fileSaveBackground);
                    themeChoose.setPreviewFile(fileSavePreview);
                    ThemeSetting.getInstance(getActivity()).setValue(themeChoose);
                    viewModel.setThemeChoose(themeChoose);
                    Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show();
                });
            } else {
                activity.runOnUiThread(() -> {
                    Toast.makeText(activity, "An error occurred!", Toast.LENGTH_SHORT).show();
                });
            }
            if (isAdded()) {
                requireActivity().onBackPressed();
            }
            if (isAdded() && dialogLoading.get() != null && dialogLoading.get().isShowing()) {
                dialogLoading.get().dismiss();
            }
        }).start();

    }

    private boolean saveImageToDevice(String linkImage, File fileSave) {

        try {
            URL url = new URL(linkImage);

            InputStream input = url.openStream();
            try {
                OutputStream output = new FileOutputStream(fileSave);
                try {
                    byte[] buffer = new byte[1024];
                    int bytesRead = 0;
                    while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                        output.write(buffer, 0, bytesRead);
                    }
                } finally {
                    output.close();
                }
            } finally {
                input.close();
                Log.d("hblong", "ThemeFeaturedDetailFragment | saveImageToDevice: " + "success");
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("hblong", "ThemeFeaturedDetailFragment | saveImageToDevice: ", e);
        }
        fileSave.delete();
        return false;

    }

    public void setType(int type) {
        this.type = type;
        binding.setType(type);
        binding.executePendingBindings();
    }
}
