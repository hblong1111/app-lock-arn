package com.app.lock.fingerprint.privacy.guard.purchase.function;

public interface BillingListener {
    void onInitBillingListener(int code);
}