package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.app.lock.R;
import com.app.lock.databinding.ActivityHomeBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.pager.HomePagerAdapter;
import com.app.lock.fingerprint.privacy.guard.ads.InterstitialAds;
import com.app.lock.fingerprint.privacy.guard.ads.NativeAds;
import com.app.lock.fingerprint.privacy.guard.application.MyApplication;
import com.app.lock.fingerprint.privacy.guard.fragment.SearchAppFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.setting.SettingFragment;
import com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ServiceHelper;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.model.ThemeInstallResult;
import com.app.lock.fingerprint.privacy.guard.model.setting.FirstLoginSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ListAppLockSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.RateSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ThemeSetting;
import com.app.lock.fingerprint.privacy.guard.network.ThemeService;
import com.app.lock.fingerprint.privacy.guard.purchase.ProxPurchase;
import com.app.lock.fingerprint.privacy.guard.remote_config.MyRemoteConfig;
import com.app.lock.fingerprint.privacy.guard.utils.AppFileUtils;
import com.app.lock.fingerprint.privacy.guard.utils.CheckEvent;
import com.app.lock.fingerprint.privacy.guard.utils.MyUtils;
import com.app.lock.fingerprint.privacy.guard.service.LockAppService;
import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.app.lock.fingerprint.privacy.guard.view_custom.RateDialog;
import com.app.lock.fingerprint.privacy.guard.view_model.HomeViewModel;
import com.blankj.utilcode.util.BarUtils;
import com.google.android.gms.ads.LoadAdError;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityHomeBinding binding;
    private Context context = this;
    private HomeViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        new Thread(() ->
                fetchDataViewModel()).start();

        binding.viewPager.setAdapter(new HomePagerAdapter(getSupportFragmentManager()));

        binding.tabLayout.withPager(binding.viewPager);

        if (PermissionHelper.canDrawOverlays(this) && PermissionHelper.checkUsagePermission(this)) {
            ServiceHelper.startService(this, LockAppService.class);
        }

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    CheckEvent.checkHome(context, "Tab App Lock");
                } else {
                    CheckEvent.checkHome(context, "Tab Theme");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.btnSetting.setOnClickListener(this);
        binding.btnMoreApp.setOnClickListener(this);
        binding.btnPreminum.setOnClickListener(this);
        binding.btnSearch.setOnClickListener(this);

        new NativeAds(this, getString(R.string.id_ads_native_home), R.layout.ads_native_home_medium, binding.frameLayoutAds, new NativeAds.Callback() {
            @Override
            public void onAdFailedToLoad(LoadAdError i) {
                super.onAdFailedToLoad(i);
                binding.motionLayoutAds.transitionToEnd();
            }
        });

        new MyRemoteConfig(this);
        showRateDialog();


        registerNetworkChange();


    }

    private void registerNetworkChange() {
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://bhpstudiotech.com")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                ThemeService service = retrofit.create(ThemeService.class);

                service.getAllTheme().enqueue(new Callback<ThemeInstallResult>() {
                    @Override
                    public void onResponse(Call<ThemeInstallResult> call, Response<ThemeInstallResult> response) {
                        if (response.isSuccessful() && response.body() != null && response.body().getImages().size() != 0) {
                            Common.THEME_BASE = response.body().getBase();
                            viewModel.setThemeResult(response.body());
                        } else {
                            viewModel.setThemeResult(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<ThemeInstallResult> call, Throwable t) {
                        viewModel.setThemeResult(null);
                    }
                });

            }
        }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    private void showRateDialog() {
        if (FirstLoginSetting.getInstance(context).getValue()) {
            FirstLoginSetting.getInstance(context).setValue(false);
        } else {
            if (!RateSetting.getInstance(context).getValue()) {
                new RateDialog(context).show();
            }
        }
    }

    private void fetchDataViewModel() {
        if (viewModel.getListApp().getValue() == null) {
            taskLoadApp();
        }

        if (viewModel.getListAppLock().getValue() == null) {
            List<String> listAppLock = ListAppLockSetting.getInstance(this).getValue();
            viewModel.getListAppLock().postValue(listAppLock);
        }

        if (viewModel.getListThemeInstall().getValue() == null) {
            List<Theme> listThemeInstall = getThemeInternalStorage();
            viewModel.setListThemeInstall(listThemeInstall);
        }

        if (viewModel.getThemeChoose().getValue() == null) {
            Theme themeChoose = ThemeSetting.getInstance(context).getValue();
            viewModel.setThemeChoose(themeChoose);
        }


    }

    private List<Theme> getThemeInternalStorage() {
        List<Theme> result = new ArrayList<>();
        File fileRootBackground = AppFileUtils.getThemeBackgroundRoot(this);
        File fileRootPreview = AppFileUtils.getThemePreviewRoot(this);

        File[] listFile = fileRootBackground.listFiles();
        List<File> fileList = new ArrayList<>(Arrays.asList(listFile));
        Collections.sort(fileList, (file1, file2) -> (int) (file2.lastModified() - file1.lastModified()));

        for (int i = 0; i < fileList.size(); i++) {
            String name = fileList.get(i).getName();
            File previewFile = new File(fileRootPreview, name);
            File backgroundFile = new File(fileRootBackground, name);
            result.add(new Theme(backgroundFile, previewFile));
        }
        return result;
    }


    private void taskLoadApp() {
        new Thread() {
            @Override
            public void run() {
                super.run();

                //create list app recommend lock
                List<String> listAppRecommend = new ArrayList<>();
                listAppRecommend.add("com.facebook.katana");
                listAppRecommend.add("org.telegram.messenger");

                //list app in device
                List<List<String>> listsApp = new ArrayList<>();

                //add list recommend
                listsApp.add(new ArrayList<>());

                //add list application
                listsApp.add(new ArrayList<>());

                List<String> allApp = MyUtils.getAllAppLauncher(context);
                for (String s : allApp) {
                    listsApp.get(listAppRecommend.contains(s) ? 0 : 1).add(s);
                }

                viewModel.getListApp().postValue(listsApp);
            }
        }.start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PermissionHelper.REQUEST_CODE_AUTO_START:
            case PermissionHelper.CODE_REQUEST_PERMISSION_USAGE:
                updateDialogPermission(requestCode);
                break;
            case PermissionHelper.CODE_REQUEST_PERMISSION_OVERLAY:
                if (Build.VERSION.RELEASE.equals("8.1.0") || Build.VERSION.RELEASE.equals("8.1")) {
                    KProgressHUD dialogLoading = KProgressHUD.create(context)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setLabel(getString(R.string.please_wait))
                            .setDetailsLabel("On android 8.1, it takes about 10-15s to process this task")
                            .setCancellable(false)
                            .setAnimationSpeed(2)
                            .setDimAmount(0.5f).show();

                    new CountDownTimer(15000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            if (PermissionHelper.canDrawOverlays(context)) {
                                this.cancel();
                                if (dialogLoading != null && dialogLoading.isShowing()) {
                                    dialogLoading.dismiss();
                                }

                                updateDialogPermission(requestCode);
                            }
                        }

                        @Override
                        public void onFinish() {
                            if (HomeActivity.this.isDestroyed()) {
                                return;
                            }
                            if (dialogLoading != null && dialogLoading.isShowing()) {
                                dialogLoading.dismiss();
                            }
                            updateDialogPermission(requestCode);
                        }
                    }.start();

                } else {
                    updateDialogPermission(requestCode);
                }
                break;
        }
    }


    private void updateDialogPermission(int requestCode) {
        switch (requestCode) {
            case PermissionHelper.CODE_REQUEST_PERMISSION_USAGE:
                if (PermissionHelper.checkUsagePermission(context)) {
                    ReceiverLocalHelper.sendBroadcast(context, ReceiverLocalHelper.ReceiverType.ACTION_CLOSE_USAGE_PERMISSION_DIALOG);

                    CheckEvent.checkHome(context, "Permission App Usage Success");
                }
                break;
            case PermissionHelper.CODE_REQUEST_PERMISSION_OVERLAY:
                if (PermissionHelper.canDrawOverlays(context)) {
                    ReceiverLocalHelper.sendBroadcast(context, ReceiverLocalHelper.ReceiverType.ACTION_CLOSE_OVERLAY_PERMISSION_DIALOG);
                    CheckEvent.checkHome(context, "Permission Overlay Success");
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        try {
            int fragmentCount = getSupportFragmentManager().getBackStackEntryCount();
            if (fragmentCount > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                if (RateSetting.getInstance(context).getValue()) {
                    super.onBackPressed();
                } else {
                    RateDialog rateDialog = new RateDialog(context);
                    rateDialog.setOnDismissListener(dialog -> {
                        super.onBackPressed();
                    });
                    rateDialog.show();
                }
            }
        } catch (Exception e) {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnSetting:
                getSupportFragmentManager().beginTransaction().replace(R.id.containerFragmentHome, new SettingFragment()).addToBackStack(null).commit();

                CheckEvent.checkHome(context, "icon setting");
                break;
            case R.id.btnMoreApp:
                Toasty.info(this, "Coming soon!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnPreminum:
                if (MyApplication.isShowAds) {
                    if (ProxPurchase.getInstance().isAvailable()) {
                        startActivity(new Intent(this, RemoveAdsActivity.class));
                    } else {
                        Toast.makeText(context, "Please check the connection!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showDialogAlertSubscribe();
                }
                break;
            case R.id.btnSearch:
                getSupportFragmentManager().beginTransaction().replace(R.id.containerFragmentHome, new SearchAppFragment()).addToBackStack(null).commit();
                break;
        }
    }

    private void showDialogAlertSubscribe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You are using the upgrade, if you want to check the information, please visit the store's application to check. Thank you very much!");

        builder.setPositiveButton("To the Store", (dialog, which) -> {
            dialog.dismiss();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/account/subscriptions")));
            } catch (android.content.ActivityNotFoundException anfe) {
                Toast.makeText(this, "An Error!", Toast.LENGTH_SHORT).show();
            }

        });
        builder.setNegativeButton("Ok", (dialog, which) -> {
            dialog.dismiss();
        });

        builder.create().show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CheckEvent.checkAppLock(context, viewModel.getListAppLock().getValue().size());
    }
}