package com.app.lock.fingerprint.privacy.guard.view_custom.pin_lock;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.app.lock.databinding.ViewIndicatorPin4Binding;

import org.jetbrains.annotations.NotNull;

public class IndicatorPin4View extends ConstraintLayout {
    private ViewIndicatorPin4Binding binding;
    private Context context;


    public IndicatorPin4View (@NonNull @NotNull Context context) {
        super(context);
        initView();
    }

    public IndicatorPin4View (@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public IndicatorPin4View (@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public IndicatorPin4View (@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView () {
        context = getContext();
        binding = ViewIndicatorPin4Binding.inflate(LayoutInflater.from(context), this, true);

        setPinChange("");

    }

    public void setPinView (PinInputView pinView) {
        pinView.setCallbackIndicator(password -> setPinChange(password));
    }

    private void setPinChange (String password) {
        switch (password.length()) {
            case 4:
                binding.tvPass4.setVisibility(VISIBLE);

                binding.tvPass3.setVisibility(VISIBLE);
                binding.tvPass2.setVisibility(VISIBLE);

                binding.tvPass1.setVisibility(VISIBLE);

                binding.viewCur4.setVisibility(INVISIBLE);
                binding.viewCur3.setVisibility(INVISIBLE);
                binding.viewCur2.setVisibility(INVISIBLE);
                binding.viewCur1.setVisibility(INVISIBLE);
                break;
            case 3:
                binding.tvPass4.setVisibility(INVISIBLE);

                binding.tvPass3.setVisibility(VISIBLE);
                binding.tvPass2.setVisibility(VISIBLE);

                binding.tvPass1.setVisibility(VISIBLE);

                binding.viewCur4.setVisibility(VISIBLE);
                binding.viewCur3.setVisibility(INVISIBLE);
                binding.viewCur2.setVisibility(INVISIBLE);
                binding.viewCur1.setVisibility(INVISIBLE);
                break;
            case 2:
                binding.tvPass4.setVisibility(INVISIBLE);

                binding.tvPass3.setVisibility(INVISIBLE);
                binding.tvPass2.setVisibility(VISIBLE);

                binding.tvPass1.setVisibility(VISIBLE);

                binding.viewCur4.setVisibility(INVISIBLE);
                binding.viewCur3.setVisibility(VISIBLE);
                binding.viewCur2.setVisibility(INVISIBLE);
                binding.viewCur1.setVisibility(INVISIBLE);


                break;
            case 1:
                binding.tvPass4.setVisibility(INVISIBLE);

                binding.tvPass3.setVisibility(INVISIBLE);
                binding.tvPass2.setVisibility(INVISIBLE);

                binding.tvPass1.setVisibility(VISIBLE);

                binding.viewCur4.setVisibility(INVISIBLE);
                binding.viewCur3.setVisibility(INVISIBLE);
                binding.viewCur2.setVisibility(VISIBLE);
                binding.viewCur1.setVisibility(INVISIBLE);
                break;
            case 0:
                binding.tvPass4.setVisibility(INVISIBLE);

                binding.tvPass3.setVisibility(INVISIBLE);
                binding.tvPass2.setVisibility(INVISIBLE);

                binding.tvPass1.setVisibility(INVISIBLE);

                binding.viewCur4.setVisibility(INVISIBLE);
                binding.viewCur3.setVisibility(INVISIBLE);
                binding.viewCur2.setVisibility(INVISIBLE);
                binding.viewCur1.setVisibility(VISIBLE);
                break;
        }
    }
}
