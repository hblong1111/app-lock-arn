package com.app.lock.fingerprint.privacy.guard.adapter.intruder;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemIntruderBinding;

import java.io.File;
import java.util.List;

public class IntruderAdapter extends RecyclerView.Adapter<IntruderAdapter.ViewHolder> {
    public static final String PAYLOAD_UPDATE_PADDING = "PAYLOAD_UPDATE_PADDING";
    private List<File> list;

    private Callback callback;

    public IntruderAdapter (List<File> list, Callback callback) {
        this.list = list;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder (@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemIntruderBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder (@NonNull ViewHolder holder, int position) {
        holder.bind(list.get(position));
        setPaddingItem(holder, position);

        holder.itemView.setOnClickListener(view -> callback.onItemClick(position));
        holder.binding.btnDelete.setOnClickListener(view -> callback.onDeleteItem(list.get(position)));
    }

    @Override
    public void onBindViewHolder (@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {

            String payload;
            try {
                payload = (String) payloads.get(0);
            } catch (Exception e) {
                e.printStackTrace();
                payload = null;
            }

            if (payload.equals(PAYLOAD_UPDATE_PADDING)) {
                setPaddingItem(holder, position);
            }

            holder.itemView.setOnClickListener(view -> callback.onItemClick(position));
            holder.binding.btnDelete.setOnClickListener(view -> callback.onDeleteItem(list.get(position)));
        }
    }

    private void setPaddingItem (ViewHolder holder, int position) {
        int l = 0;
        int r = 0;
        int t = 0;
        int b = 0;

        if (position < 2) {
            t = 50;
        } else {
            t = 10;
        }
        if (position > getItemCount() - 3) {
            b = 50;
        } else {
            b = 10;
        }
        if (position % 2 == 0) {
            l = 30;
            r = 10;
        } else {
            r = 30;
            l = 10;
        }

        holder.itemView.setPadding(l, t, r, b);
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemIntruderBinding binding;

        public ViewHolder (@NonNull ItemIntruderBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind (File file) {
            binding.setFile(file);
            binding.executePendingBindings();
        }
    }

    public interface Callback {
        void onItemClick (int pos);

        void onDeleteItem (File pos);
    }
}
