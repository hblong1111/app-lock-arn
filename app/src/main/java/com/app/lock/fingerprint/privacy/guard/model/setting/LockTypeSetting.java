package com.app.lock.fingerprint.privacy.guard.model.setting;

import android.content.Context;

import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.utils.SharedUtils;

public class LockTypeSetting extends AppSetting {
    private static LockTypeSetting INSTANCE;
    private Context context;

    public static LockTypeSetting getInstance (Context context) {
        if (INSTANCE == null) {
            INSTANCE = new LockTypeSetting(context) {
            };
        }
        return INSTANCE;
    }

    private LockTypeSetting (Context context) {
        this.context = context;
    }

    @Override
    protected String getKey () {
        return super.getKey();
    }

    @Override
    public void setValue (Object value) {
        try {
            LockType type = (LockType) value;
            SharedUtils.getInstance(context).putString(getKey(), type.name());
            ReceiverLocalHelper.sendBroadcast(context,ReceiverLocalHelper.ReceiverType.ACTION_CHANGE_LOCK_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public LockType getValue () {
        try {
            LockType lockType = LockType.valueOf(SharedUtils.getInstance(context).getString(getKey()));
            return lockType;
        } catch (Exception e) {
            e.printStackTrace();
            return LockType.TYPE_PIN_6;
        }
    }

    public enum LockType {
        TYPE_PIN_4,
        TYPE_PIN_6,
        TYPE_PATTERN
    }
}
