package com.app.lock.fingerprint.privacy.guard.fragment.app_lock_pager;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.lock.databinding.DialogRequestAutoStartPermissionBinding;
import com.app.lock.databinding.DialogRequestOverlayPermissionBinding;
import com.app.lock.databinding.DialogRequestUsagePermissionBinding;
import com.app.lock.databinding.FragmentAllAppBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.home_app.ListAppAdapter;
import com.app.lock.fingerprint.privacy.guard.helper.DialogHelper;
import com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ServiceHelper;
import com.app.lock.fingerprint.privacy.guard.model.setting.ListAppLockSetting;
import com.app.lock.fingerprint.privacy.guard.service.LockAppService;
import com.app.lock.fingerprint.privacy.guard.view_model.HomeViewModel;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class AllAppFragment extends Fragment {
    private FragmentAllAppBinding binding;
    private ListAppAdapter adapter;
    private List<List<String>> listsApp;
    private Context context;
    private ListAppAdapter.Callback callback;

    private HomeViewModel viewModel;
    private List<String> listAppLock;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentAllAppBinding.inflate(inflater, container, false);
        context = getActivity();

        viewModel = new ViewModelProvider(getActivity()).get(HomeViewModel.class);

        setupRCV();
        return binding.getRoot();
    }


    private void setupRCV() {
        createCallback();
        listsApp = new ArrayList<>();
        listAppLock = new ArrayList<>();
        adapter = new ListAppAdapter(listsApp, callback, listAppLock);

        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new LinearLayoutManager(context));


        viewModel.getListApp().observe(getActivity(), lists -> {
            listsApp.addAll(lists);
            adapter.notifyDataSetChanged();
        });

        viewModel.getListAppLock().observe(getActivity(), lists -> {
            if (listAppLock.equals(lists)) {
                return;
            }
            listAppLock.clear();
            listAppLock.addAll(lists);
            adapter.notifyItemRangeChanged(0, adapter.getItemCount(), adapter.UPDATE_STATE_LOCK);
        });
    }

    private void createCallback() {
        callback = new ListAppAdapter.Callback() {
            @Override
            public void onItemAppClick(int position, String packageName) {
                if (!PermissionHelper.checkUsagePermission(context)) {
                    showDialogRequestUsage(packageName, position);
                } else if (!PermissionHelper.canDrawOverlays(context)) {
                    showDialogRequestOverlay(packageName, position);
                } else {
                    addAppLock(position, packageName);
                }
            }

            private void showDialogRequestUsage(String packageName, int position) {
                DialogRequestUsagePermissionBinding binding = DialogRequestUsagePermissionBinding.inflate(getLayoutInflater());
                DialogHelper.showDialog(context, binding.getRoot(), new DialogHelper.ConfigDialog() {
                    @Override
                    public boolean setCancelable() {
                        return true;
                    }

                    @Override
                    public void configDialog(Dialog dialog) {
                        super.configDialog(dialog);

                        ReceiverLocalHelper.registerReceiver(context, new BroadcastReceiver() {
                            @Override
                            public void onReceive(Context context, Intent intent) {
                                if (isAdded() && dialog != null && dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (!PermissionHelper.canDrawOverlays(context)) {
                                    showDialogRequestOverlay(packageName, position);
                                }
                            }
                        }, ReceiverLocalHelper.ReceiverType.ACTION_CLOSE_USAGE_PERMISSION_DIALOG);
                        binding.btnClose.setOnClickListener(v -> {
                            dialog.dismiss();
                        });

                        binding.btnAgree.setOnClickListener(v -> {
                            PermissionHelper.requestUsagePermission(getActivity());
                        });
                    }
                });


            }

            private void showDialogRequestOverlay(String packageName, int position) {

                if (!isAdded()) {
                    return;
                }
                DialogRequestOverlayPermissionBinding binding = DialogRequestOverlayPermissionBinding.inflate(getLayoutInflater());
                DialogHelper.showDialog(context, binding.getRoot(), new DialogHelper.ConfigDialog() {
                    @Override
                    public boolean setCancelable() {
                        return true;
                    }

                    @Override
                    public void configDialog(Dialog dialog) {
                        super.configDialog(dialog);


                        ReceiverLocalHelper.registerReceiver(context, new BroadcastReceiver() {
                            @Override
                            public void onReceive(Context context, Intent intent) {
                                if (isAdded()) {
                                    if (dialog != null && dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                }
                                if (PermissionHelper.checkDeviceAutoStart(getActivity())) {
                                    showDialogRequestAutoStart(packageName, position);
                                } else {
                                    onGrandPermission(packageName, position);
                                }
                            }
                        }, ReceiverLocalHelper.ReceiverType.ACTION_CLOSE_OVERLAY_PERMISSION_DIALOG);
                        binding.btnClose.setOnClickListener(v -> {
                            if (isAdded()) {
                                if (dialog != null && dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        });

                        binding.btnAgree.setOnClickListener(v -> {
                            PermissionHelper.requestOverlayPermission(getActivity());
                        });
                    }
                });
            }

            private void showDialogRequestAutoStart(String packageName, int position) {
                DialogRequestAutoStartPermissionBinding binding = DialogRequestAutoStartPermissionBinding.inflate(getLayoutInflater());
                DialogHelper.showDialog(context, binding.getRoot(), new DialogHelper.ConfigDialog() {
                    @Override
                    public boolean setCancelable() {
                        return false;
                    }

                    @Override
                    public void configDialog(Dialog dialog) {
                        super.configDialog(dialog);
                        binding.btnAgree.setOnClickListener(v -> {
                            PermissionHelper.requestAutoStart(getActivity());
                            dialog.dismiss();
                        });
                        binding.btnAgree.setOnClickListener(v -> {
                            dialog.dismiss();
                            PermissionHelper.requestAutoStart(getActivity());
                        });

                        dialog.setOnDismissListener(dialog1 -> {
                            onGrandPermission(packageName, position);
                        });
                    }
                });
            }
        };

    }

    private void addAppLock(int position, String packageName) {
        if (listAppLock.contains(packageName)) {
            listAppLock.remove(packageName);
            try {
                String label = (String) context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(packageName, 0));
                Toasty.error(requireActivity(), "UnLocked: " + label, Toast.LENGTH_SHORT, true).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            listAppLock.add(packageName);
            try {
                String label = (String) context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(packageName, 0));
                Toasty.success(requireActivity(), "Locked: " + label, Toast.LENGTH_SHORT, true).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ListAppLockSetting.getInstance(context).setValue(listAppLock);

        adapter.notifyItemChanged(position, adapter.UPDATE_STATE_LOCK);

        viewModel.setListAppLock(listAppLock);

    }

    private void onGrandPermission(String packageName, int position) {
        ReceiverLocalHelper.sendBroadcast(context, ReceiverLocalHelper.ReceiverType.ACTION_GRAND_ALL_PERMISSION);
        ServiceHelper.startService(context, LockAppService.class);
        addAppLock(position, packageName);
    }
}
