package com.app.lock.fingerprint.privacy.guard.adapter;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemPasswordSafeBinding;
import com.app.lock.fingerprint.privacy.guard.model.PasswordSafe;
import com.blankj.utilcode.util.ScreenUtils;

import java.util.List;

public class PasswordSafeAdapter extends RecyclerView.Adapter<PasswordSafeAdapter.ViewHolder> {
    private List<PasswordSafe> list;
    private Callback callback;

    public PasswordSafeAdapter(List<PasswordSafe> list, Callback callback) {
        this.list = list;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemPasswordSafeBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        setPadding(holder, position);
        holder.bind(list.get(position), !holder.binding.tvPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance()));
        holder.binding.tvPassword.setOnClickListener(v -> {
            boolean isShow = !holder.binding.tvPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance());
            holder.binding.tvPassword.setTransformationMethod(isShow ? PasswordTransformationMethod.getInstance() : HideReturnsTransformationMethod.getInstance());
            holder.binding.setIsShow(!holder.binding.getIsShow());
            holder.binding.executePendingBindings();
        });

        holder.itemView.setOnClickListener(v -> callback.onClickItemAdapter(position));
        holder.binding.btnClose.setOnClickListener(v -> callback.onRemoveItem(position));
    }

    private void setPadding(ViewHolder holder, int position) {
        int l = 20;
        int t;
        int r = 20;
        int b;
        if (position == 0) {
            t = 50;
        } else {
            t = 10;
        }
        if (position > getItemCount() - 2) {
            b = ScreenUtils.getScreenHeight() / 4;
        } else {
            b = 10;
        }


        holder.itemView.setPadding(l, t, r, b);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemPasswordSafeBinding binding;

        public ViewHolder(@NonNull ItemPasswordSafeBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(PasswordSafe item, Boolean isShowPass) {
            binding.setIsShow(isShowPass);
            binding.setPassword(item);
            binding.executePendingBindings();
        }
    }

    public interface Callback {
        void onClickItemAdapter(int pos);

        void onRemoveItem(int pos);
    }
}
