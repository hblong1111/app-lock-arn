package com.app.lock.fingerprint.privacy.guard.view_custom;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.app.lock.R;
import com.app.lock.databinding.ViewLockBinding;
import com.app.lock.fingerprint.privacy.guard.ads.NativeAds;
import com.app.lock.fingerprint.privacy.guard.application.MyApplication;
import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ServiceHelper;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.model.setting.IntruderSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.PasswordSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ShowIconSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ThemeSetting;
import com.app.lock.fingerprint.privacy.guard.service.LockAppService;
import com.app.lock.fingerprint.privacy.guard.utils.CheckEvent;
import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.app.lock.fingerprint.privacy.guard.view_custom.pattern.PatternLockView;
import com.app.lock.fingerprint.privacy.guard.view_custom.pin_lock.PinInputView;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.LoadAdError;

public class LockView extends ConstraintLayout {
    private Context context;
    private ViewLockBinding binding;


    private boolean isShowIconApp;

    private Vibrator vibrator;


    //create callback pin
    private PinInputView.Callback callbackPin = new PinInputView.Callback() {
        @Override
        public void onChange(String password) {

        }

        @Override
        public void onComplete(String password) {
            String passwordSetting = PasswordSetting.getInstance(context).getValue();
            if (passwordSetting.equals(password)) {
                onUnLockSuccess();
            } else {
                onUnLockError();
            }
        }
    };
    //create callback pattern
    private PatternLockView.PatternCallback callbackPattern = new PatternLockView.PatternCallback() {
        @Override
        public void onDraw(String password) {

        }

        @Override
        public void onComplete(String password) {
            String passwordSetting = PasswordSetting.getInstance(context).getValue();
            if (passwordSetting.equals(password)) {
                onUnLockSuccess();
            } else {
                onUnLockError();
            }
        }
    };
//    private ValueAnimator valueAnimator;


    public LockView(@NonNull Context context) {
        super(context);
        init();
    }

    public LockView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LockView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public LockView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setVisibility(INVISIBLE);
        context = getContext();

        binding = ViewLockBinding.inflate(LayoutInflater.from(context), this, true);

        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);


//        setup view
        setLockType();
        setShowIconApp();
        setTheme();

        //set callback view lock
        binding.pinInput.setCallback(callbackPin);
        binding.patternLock.setCallbackPattern(callbackPattern);


        //register receiver local
        ReceiverLocalHelper.registerReceiver(context, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setTheme();
            }
        }, ReceiverLocalHelper.ReceiverType.ACTION_CHANGE_THEME);

        ReceiverLocalHelper.registerReceiver(context, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setShowIconApp();
            }
        }, ReceiverLocalHelper.ReceiverType.ACTION_CHANGE_SHOW_ICON);

        ReceiverLocalHelper.registerReceiver(context, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setLockType();
            }
        }, ReceiverLocalHelper.ReceiverType.ACTION_CHANGE_LOCK_TYPE);


        //fixme: disable load ads delay
//        initAnimator();

        loadAds();

    }

    private void initAnimator() {
//        binding.viewClick.setOnClickListener(v -> {
//        });
//        valueAnimator = ValueAnimator.ofFloat(0, 1);
//        valueAnimator.setInterpolator(new LinearInterpolator());
//        valueAnimator.addUpdateListener(animation -> {
//            float value = (float) animation.getAnimatedValue();
//            binding.patternLock.setScaleX(value);
//            binding.patternLock.setScaleY(value);
//            binding.pinInput.setScaleX(value);
//            binding.pinInput.setScaleY(value);
//        });
//        valueAnimator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                super.onAnimationEnd(animation);
//                binding.viewClick.setVisibility(View.INVISIBLE);
//            }
//        });
    }

    private void loadAds() {
        new NativeAds(getContext(), context.getString(R.string.id_ads_native_unlock), R.layout.ads_native_unlock, binding.frameLayoutAds, new NativeAds.Callback() {
            @Override
            public void onAdFailedToLoad(LoadAdError i) {
                super.onAdFailedToLoad(i);
                Log.d("hblong", "onAdFailedToLoad: " + "load false");
                binding.motionLayoutAds.transitionToEnd();
            }
        });
    }

    private void vibrate(long time) {
        // Vibrate for time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(time, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            vibrator.vibrate(time);
        }
    }

    public void setLockType() {
        LockTypeSetting.LockType lockType = LockTypeSetting.getInstance(context).getValue();
        if (lockType == LockTypeSetting.LockType.TYPE_PIN_4) {
            binding.indicatorPin4.setPinView(binding.pinInput);
        } else {
            binding.indicatorPin6.setPinView(binding.pinInput);
        }

        binding.setType(lockType);
        binding.executePendingBindings();

    }

    public void setShowIconApp() {
        isShowIconApp = ShowIconSetting.getInstance(context).getValue();
        binding.imgIconApp.setVisibility(isShowIconApp ? VISIBLE : INVISIBLE);
    }

    public void hideLock(long delay) {
        postDelayed(() -> {
            setVisibility(INVISIBLE);
            binding.viewClick.setVisibility(VISIBLE);
            binding.pinInput.resetPin();
            binding.patternLock.resetPattern();
//            if (valueAnimator.isRunning()) {
//                valueAnimator.pause();
//            }

            loadAds();
        }, delay);
    }

    public void showLock(String packageName) {
        post(() -> {
            setVisibility(VISIBLE);
//            valueAnimator.setDuration(MyApplication.isShowAds ? Common.TIME_DELAY_LOAD_NATIVE_ADS : 1);
//            valueAnimator.start();
            if (isShowIconApp) {
                Drawable drawable = null;
                try {
                    drawable = context.getPackageManager().getApplicationIcon(packageName);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                if (drawable != null) {
                    Glide.with(context).load(drawable).into(binding.imgIconApp);
                }
            }
        });
    }


    private void onUnLockError() {
        if (IntruderSetting.getInstance(context).getValue()) {
            ServiceHelper.startService(context, LockAppService.class, Common.ACTION_TAKE_INTRUDER);
        }
        vibrate(200);
        binding.pinInput.resetPin();
        binding.patternLock.onErrorPassword();
    }

    private void onUnLockSuccess() {
        CheckEvent.checkUnlock(context);
        hideLock(0);
    }


    public void setTheme() {
        Theme theme = ThemeSetting.getInstance(context).getValue();
        Glide.with(context).load(theme.getBackgroundFile()).into(binding.imgBackground);
    }
}
