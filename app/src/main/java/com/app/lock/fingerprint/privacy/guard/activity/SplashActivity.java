package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.ads.control.Admod;
import com.ads.control.funtion.AdCallback;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.app.lock.R;
import com.app.lock.databinding.ActivitySplashBinding;
import com.app.lock.fingerprint.privacy.guard.ads.InterstitialAds;
import com.app.lock.fingerprint.privacy.guard.application.MyApplication;
import com.app.lock.fingerprint.privacy.guard.model.setting.PasswordSetting;
import com.app.lock.fingerprint.privacy.guard.purchase.ProxPurchase;
import com.app.lock.fingerprint.privacy.guard.receiver.ResetAppReceiver;
import com.app.lock.fingerprint.privacy.guard.utils.AppFileUtils;
import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.google.android.gms.ads.LoadAdError;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SplashActivity extends AppCompatActivity {
    private ActivitySplashBinding binding;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (am != null) {
            List<ActivityManager.AppTask> tasks = am.getAppTasks();
            if (tasks != null && tasks.size() > 0) {
                tasks.get(0).setExcludeFromRecents(true);
            }
        }
        registerReceiver(new ResetAppReceiver(), new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));

        installDefaultTheme();

        if (MyApplication.isShowAds) {
            InterstitialAds.getInstance(this, new InterstitialAds.CustomCallback() {
            });
        }


        MyApplication.isShowAds = !ProxPurchase.getInstance().isPurchased();

        new Handler().postDelayed(() -> {
            loadAds();
        }, 3000);

    }

    private void loadAds() {
        if (MyApplication.isShowAds) {
            Admod.getInstance().loadSplashInterstitalAds(SplashActivity.this,
                    getString(R.string.id_interstitial_splash),
                    20000,
                    new AdCallback() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            startActivity();
                        }

                        @Override
                        public void onAdFailedToLoad(LoadAdError i) {
                            super.onAdFailedToLoad(i);
                            startActivity();
                        }
                    });
        } else {
            startActivity();
        }
    }

    private void installDefaultTheme() {
        //create folder Internal storage
        File rootThemeBackground = AppFileUtils.getThemeBackgroundRoot(this);
        File rootThemePreview = AppFileUtils.getThemePreviewRoot(this);

        rootThemeBackground.mkdirs();
        rootThemePreview.mkdirs();

        //check if downloaded or not
        if (rootThemePreview.listFiles() == null || rootThemePreview.listFiles().length == 0) {
            new Thread(() -> {
                fetchData(rootThemeBackground, rootThemePreview);
            }).start();
        }
    }

    private void fetchData(File rootThemeBackground, File rootThemePreview) {
        String[] listBackground;
        String[] listPreview;
        try {
            listBackground = getAssets().list("theme_background");
            listPreview = getAssets().list("theme_preview");

            for (int i = listBackground.length - 1; i >= 0; i--) {
                //save bitmap background to Internal storage
                Bitmap bitmapBackground = getBitmapFromAsset(this, String.format("theme_background/%d.png", i));
                File fileSaveBackground = new File(rootThemeBackground, String.format("%d.png", i));
                saveBitmapToFile(fileSaveBackground, bitmapBackground);

                //save bitmap preview to Internal storage
                Bitmap bitmapPreview = getBitmapFromAsset(this, String.format("theme_preview/%d.png", i));
                File fileSavePreview = new File(rootThemePreview, String.format("%d.png", i));
                saveBitmapToFile(fileSavePreview, bitmapPreview);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startActivity() {

        new Handler(getMainLooper()).postDelayed(() -> {
            Intent intent;
            if (PasswordSetting.getInstance(context).getValue().length() == 0) {
                intent = new Intent(context, ChooseTypeLockActivity.class);
            } else {
                intent = new Intent(context, LockActivity.class);
            }
            context.startActivity(intent);
            finish();
        }, 0);
    }

    private void saveBitmapToFile(File fileSaveImage, Bitmap bitmap) {
        try (FileOutputStream out = new FileOutputStream(fileSaveImage)) {
            if (bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            }
            // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap getBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();

        InputStream inputStream;
        Bitmap bitmap = null;
        try {
            inputStream = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(inputStream);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }

}