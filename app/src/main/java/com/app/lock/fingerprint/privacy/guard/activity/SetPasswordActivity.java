package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.app.lock.R;
import com.app.lock.databinding.ActivitySetPasswordBinding;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.PasswordSetting;
import com.app.lock.fingerprint.privacy.guard.view_custom.pattern.PatternLockView;
import com.app.lock.fingerprint.privacy.guard.view_custom.pin_lock.PinInputView;

public class SetPasswordActivity extends AppCompatActivity {
    private ActivitySetPasswordBinding binding;

    private String newPassword;

    private String title;

    private boolean isError;

    private PinInputView.Callback callbackPin;
    private PatternLockView.PatternCallback callbackPattern;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySetPasswordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        binding.pinInput.setColor(R.color.black);
        binding.patternLockView.setColorDots(R.color.black);
        binding.patternLockView.setColorLine(R.color.black);

        LockTypeSetting.LockType lockType = LockTypeSetting.getInstance(this).getValue();
        binding.containerPattern.setVisibility(lockType == LockTypeSetting.LockType.TYPE_PATTERN ? View.VISIBLE : View.INVISIBLE);
        binding.containerPin.setVisibility(lockType == LockTypeSetting.LockType.TYPE_PATTERN ? View.INVISIBLE : View.VISIBLE);

        if (lockType != LockTypeSetting.LockType.TYPE_PATTERN) {
            setTitle("Pin Code Setup");
            callbackPin = new PinInputView.Callback() {
                @Override
                public void onChange (String password) {
                }

                @Override
                public void onComplete (String password) {
                    if (newPassword == null) {
                        newPassword = password;
                        setTitle("Pin Code Confirm");
                    } else {
                        if (newPassword.equals(password)) {
                            onSetPasswordSuccess();
                        } else {
                            setTitle("Pin Code Incorrect");
                            setError(true);
                        }
                    }

                    binding.pinInput.resetPin();
                }
            };
            if (lockType == LockTypeSetting.LockType.TYPE_PIN_4) {
                binding.indicatorView4.setPinView(binding.pinInput);
                binding.indicatorView4.setVisibility(View.VISIBLE);
            } else {
                binding.indicatorView6.setPinView(binding.pinInput);
                binding.indicatorView6.setVisibility(View.VISIBLE);
            }
            binding.pinInput.setCallback(callbackPin);
        } else {
            setTitle("Pattern Setup");

            callbackPattern = new PatternLockView.PatternCallback() {
                @Override
                public void onDraw (String password) {
                }

                @Override
                public void onComplete (String password) {
                    Log.d("hblong", "onComplete: " + password);
                    if (password.length() < 4) {
                        setTitle("Invalid pattern");
                        setError(true);
                        return;
                    }
                    if (newPassword == null) {
                        newPassword = password;
                        setTitle("Pattern Confirm");
                        setError(false);
                    } else {
                        if (newPassword.equals(password)) {
                            onSetPasswordSuccess();
                        } else {
                            setTitle("Pattern Incorrect");
                            setError(true);
                        }
                    }
                }
            };

            binding.patternLockView.setCallbackPattern(callbackPattern);
        }

    }

    private void onSetPasswordSuccess () {
        setTitle("Success");
        setError(false);
        PasswordSetting.getInstance(this).setValue(newPassword);
        startActivity(new Intent(this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    public void setTitle (String title) {
        this.title = title;
        binding.setTitle(title);
        binding.executePendingBindings();
    }

    public void setError (boolean error) {
        isError = error;
        if (error) {
            binding.patternLockView.onErrorPassword();
        } else {
            binding.patternLockView.resetPattern();
        }
        binding.setIsError(isError);
        binding.executePendingBindings();
    }
}