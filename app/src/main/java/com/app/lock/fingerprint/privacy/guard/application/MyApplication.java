package com.app.lock.fingerprint.privacy.guard.application;

import android.app.Application;
import android.os.Environment;

import com.ads.control.Admod;
import com.app.lock.R;
import com.app.lock.fingerprint.privacy.guard.purchase.ProxPurchase;
import com.app.lock.fingerprint.privacy.guard.utils.AndroidUtils;
import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyApplication extends Application {
    public static boolean isShowAds = true;

    @Override
    public void onCreate() {
        super.onCreate();
        initAds();
        String pathRoot = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/" + getString(R.string.app_name);
        Common.PATH_IMAGES = pathRoot + "/.images";
        Common.PATH_DOCUMENTS = pathRoot + "/.documents";
        Common.PATH_VIDEOS = pathRoot + "/.videos";
        Common.PATH_MUSIC = pathRoot + "/.musics";
        Common.PATH_FILES_UN_LOCK = pathRoot;


        List<String> listIdSub = new ArrayList<>();

        listIdSub.add(getString(R.string.id_in_app_purchase_month));
        listIdSub.add(getString(R.string.id_in_app_purchase_year));
        ProxPurchase.getInstance().initBilling(this, null, listIdSub);
        AndroidUtils.fix();
    }


    private void initAds() {


        List<String> testDeviceIds = Arrays.asList("87AC249488102514CAFAB216B6CD2CB9",
                "525AEAEC801136F4BA2A8AA2A68EF441"
                , "35996F5D57149145A5F9F0CCB753F292"
                , "57B8520C005B1BCE13CA37A9169A1AA8",
                "59DF72E77192E05DB117DBDFE213F625");

        RequestConfiguration configuration = new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();


        Admod.getInstance().init(this, testDeviceIds);
        MobileAds.setRequestConfiguration(configuration);
        MobileAds.initialize(this, initializationStatus -> {
        });
//                appOpenManager = new AppOpenManager(MyApplication.this);


    }
}
