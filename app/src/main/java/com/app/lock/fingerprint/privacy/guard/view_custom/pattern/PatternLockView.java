package com.app.lock.fingerprint.privacy.guard.view_custom.pattern;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;


import com.app.lock.R;

import java.util.ArrayList;
import java.util.List;

public class PatternLockView extends View {
    private Paint paintDots;
    private Paint paintLine;
    private Paint paintDotsBackground;
    private Context context;

    private float wDraw, hDraw;

    private float dotSize = 30;

    private float padding = 50;
    private List<Dots> dotsList;
    private List<Dots> dotsListActive;
    private float xTouch;
    private float yTouch;


    private PatternCallback callback;
    private Runnable runnableError = () -> {
        resetPattern();
        invalidate();
    };
    private int colorDots;
    private int colorLine;
    private int colorDotsSelected;
    private int colorError;


    public PatternLockView (Context context) {
        super(context);
        initView();
    }

    public PatternLockView (Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public PatternLockView (Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public PatternLockView (Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView () {
        context = getContext();

        createPaint();

        dotsList = new ArrayList<>();
        dotsListActive = new ArrayList<>();


    }

    private void createPaint () {
        paintDots = new Paint();
        paintDots.setColor(ContextCompat.getColor(context, getColorDots()));
        paintDots.setStyle(Paint.Style.FILL);
        paintDots.setAntiAlias(true);

        paintDotsBackground = new Paint();
        paintDotsBackground.setColor(ContextCompat.getColor(context, getColorDotsSelected()));
        paintDotsBackground.setStyle(Paint.Style.FILL);
        paintDotsBackground.setAntiAlias(true);

        paintLine = new Paint();
        paintLine.setColor(ContextCompat.getColor(context, getColorLine()));
        paintLine.setStyle(Paint.Style.STROKE);
        paintLine.setAntiAlias(true);
        paintLine.setStrokeWidth(dotSize / 2);
        paintLine.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    protected void onLayout (boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        wDraw = getMeasuredWidth();
        hDraw = getMeasuredHeight();

        //distance between dots
        float distance = (Math.min(wDraw, hDraw) - padding * 2 - dotSize * 3) / 2;

        for (int i = 0; i < 9; i++) {
            int col = (i % 3) + 1;
            int row = i / 3 + 1;


            float x = 0;
            float y = 0;

            switch (col) {
                case 1:
                    x = (wDraw / 2) - distance - dotSize;
                    break;
                case 2:
                    x = wDraw / 2;
                    break;
                case 3:
                    x = (wDraw / 2) + distance + dotSize;
                    break;
            }
            switch (row) {
                case 1:
                    y = (hDraw / 2) - distance - dotSize;
                    break;
                case 2:
                    y = (hDraw / 2);
                    break;
                case 3:
                    y = (hDraw / 2) + distance + dotSize;
                    break;
            }
            Dots dots = new Dots(x, y, i + 1);
            dotsList.add(dots);

        }
    }

    @Override
    protected void onDraw (Canvas canvas) {
        for (int i = 0; i < dotsListActive.size(); i++) {
            if (i <= dotsListActive.size() - 2) {
                Dots dots = dotsListActive.get(i);
                Dots dotsNext = dotsListActive.get(i + 1);
                canvas.drawLine(dots.getX(), dots.getY(), dotsNext.getX(), dotsNext.getY(), paintLine);
            } else {
                Dots dots = dotsListActive.get(i);
                canvas.drawLine(dots.getX(), dots.getY(), xTouch, yTouch, paintLine);
            }
        }

        for (Dots dots :
                dotsList) {
            dots.draw(canvas);
        }
        super.onDraw(canvas);
    }


    @Override
    public boolean onTouchEvent (MotionEvent event) {
        xTouch = event.getX();
        yTouch = event.getY();


        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (dotsListActive.size() != 0) {
                    xTouch = dotsListActive.get(dotsListActive.size() - 1).x;
                    yTouch = dotsListActive.get(dotsListActive.size() - 1).y;
                    if (callback != null) {
                        callback.onComplete(getPassWord());
                    }
                }
                break;
            case MotionEvent.ACTION_DOWN:
                paintLine.setColor(ContextCompat.getColor(context, getColorLine()));

                removeCallbacks(runnableError);
                resetPattern();
                checkActiveDot(xTouch, yTouch);
                break;
            case MotionEvent.ACTION_MOVE:
                if (dotsListActive.size() != 0) {
                    checkActiveDot(xTouch, yTouch);
                }
                break;
        }

        invalidate();
        return true;
    }

    private void checkActiveDot (float xTouch, float yTouch) {
        for (Dots dots :
                dotsList) {
            if (dots.isActive) {
                continue;
            }
            dots.checkActive(xTouch, yTouch);
        }
    }

    public void setCallbackPattern (PatternCallback callback) {
        this.callback = callback;
    }


    private String getPassWord () {
        String result = "";
        for (Dots dots :
                dotsListActive) {
            result += dots.getValue();
        }
        return result;
    }

    public void resetPattern () {
        for (Dots dots :
                dotsList) {
            dots.setActive(false);
        }
        dotsListActive.clear();
    }

    public void onErrorPassword () {
        paintLine.setColor(ContextCompat.getColor(context, getColorError()));
        postDelayed(runnableError, 1000);
    }

    public int getColorDots () {
        if (colorDots == 0) {
            colorDots = R.color.color_dots_df;
        }
        return colorDots;
    }

    public void setColorDots (int colorDots) {
        this.colorDots = colorDots;
        paintDots.setColor(colorDots);
        invalidate();
    }

    public int getColorLine () {
        if (colorLine == 0) {
            colorLine = R.color.color_line_df;
        }
        return colorLine;
    }

    public void setColorLine (int colorLine) {
        this.colorLine = colorLine;
        paintLine.setColor(colorDots);
        invalidate();
    }

    public int getColorDotsSelected () {
        if (colorDotsSelected == 0) {
            colorDotsSelected = R.color.color_dots_select_df;
        }
        return colorDotsSelected;
    }

    public void setColorDotsSelected (int colorDotsSelected) {
        this.colorDotsSelected = colorDotsSelected;
    }

    public int getColorError () {
        if (colorError == 0) {
            colorError = R.color.color_error_pattern_df;
        }
        return colorError;
    }

    public void setColorError (int colorError) {
        this.colorError = colorError;
    }

    class Dots {
        private float x, y;
        private float radius;
        private boolean isActive;
        private float radiusActive;

        private int value;


        public Dots (float x, float y, int value) {
            this.x = x;
            this.y = y;
            this.value = value;
            this.radius = dotSize / 2;
            radiusActive = radius * 3;
        }


        public Dots () {
        }

        public void draw (Canvas canvas) {
            if (isActive) {
                canvas.drawCircle(x, y, radiusActive, paintDotsBackground);
            }
            canvas.drawCircle(x, y, radius, paintDots);
        }

        public float getX () {
            return x;
        }

        public void setX (float x) {
            this.x = x;
        }

        public float getY () {
            return y;
        }

        public void setY (float y) {
            this.y = y;
        }

        public float getRadius () {
            return radius;
        }

        public void setRadius (float radius) {
            this.radius = radius;
        }

        public boolean isActive () {
            return isActive;
        }

        public void setActive (boolean active) {
            isActive = active;
        }

        public int getValue () {
            return value;
        }

        public void checkActive (float xTouch, float yTouch) {
            isActive = Math.abs(xTouch - x) <= (radiusActive) && Math.abs(yTouch - y) <= (radiusActive);
            if (isActive) {
                try {
                    int index = - 1;
                    int v = dotsListActive.get(dotsListActive.size() - 1).getValue();
                    switch (v) {
                        case 2:
                            if (value == 8) {
                                index = 5;
                            }
                            break;
                        case 8:
                            if (value == 2) {
                                index = 5;
                            }
                            break;
                        case 6:
                            if (value == 4) {
                                index = 5;
                            }
                            break;
                        case 4:
                            if (value == 6) {
                                index = 5;
                            }
                            break;
                        case 1:
                            switch (value) {
                                case 3:
                                    index = 2;
                                    break;
                                case 7:
                                    index = 4;
                                    break;
                                case 9:
                                    index = 5;
                                    break;
                            }
                            break;
                        case 3:
                            switch (value) {
                                case 1:
                                    index = 2;
                                    break;
                                case 7:
                                    index = 5;
                                    break;
                                case 9:
                                    index = 6;
                                    break;
                            }
                            break;
                        case 7:
                            switch (value) {
                                case 3:
                                    index = 5;
                                    break;
                                case 1:
                                    index = 4;
                                    break;
                                case 9:
                                    index = 8;
                                    break;
                            }
                            break;
                        case 9:
                            switch (value) {
                                case 1:
                                    index = 5;
                                    break;
                                case 7:
                                    index = 8;
                                    break;
                                case 3:
                                    index = 6;
                                    break;
                            }
                            break;
                    }

                    if (index > 0) {
                        Dots dots = dotsList.get(index - 1);
                        if (! dots.isActive()) {
                            dots.setActive(true);
                            dotsListActive.add(dots);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                boolean needAdd = true;
                for (Dots dotActive : dotsListActive){
                    if (dotActive.getValue() == this.getValue()) {
                        needAdd = false;
                        break;
                    }
                }
                if (needAdd){
                    dotsListActive.add(this);
                }

                if (callback != null) {
                    callback.onDraw(getPassWord());
                }
            }
        }
    }

    public interface PatternCallback {

        void onDraw (String password);

        void onComplete (String password);
    }
}
