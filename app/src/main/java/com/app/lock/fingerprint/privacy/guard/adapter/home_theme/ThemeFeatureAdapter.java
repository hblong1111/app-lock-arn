package com.app.lock.fingerprint.privacy.guard.adapter.home_theme;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemThemeFeaturedBinding;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.model.ThemeInstallResult;
import com.blankj.utilcode.util.ScreenUtils;

import java.util.List;

public class ThemeFeatureAdapter extends RecyclerView.Adapter<ThemeFeatureAdapter.ViewHolder> {
    public static final String PAYLOAD_UPDATE_THEME_CHOOSE = "PAYLOAD_UPDATE_THEME_CHOOSE";
    private ThemeInstallResult result;
    private Theme themeChoose;
    private Callback callback;

    public ThemeFeatureAdapter(ThemeInstallResult result, Theme themeChoose, Callback callback) {
        this.result = result;
        this.themeChoose = themeChoose;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemThemeFeaturedBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ThemeInstallResult.Image themeFeatured = result.getImages().get(position);
        Boolean selected = themeChoose.getPreviewFile().getName().equals(themeFeatured.getName());
        holder.bind(themeFeatured.getPreviewLink(), selected);
        holder.itemView.setOnClickListener(v -> callback.onItemClick(position));
        setPadding(holder, position);
    }

    private void setPadding(ViewHolder holder, int position) {
        int l = 10;
        int t;
        int r = 10;
        int b;
        if (position < 3) {
            t = 50;
        } else {
            t = 10;
        }
        if (position >= getItemCount() - getItemCount() % 3) {
            b = ScreenUtils.getScreenHeight() / 4;
        } else {
            b = 10;
        }
        holder.itemView.setPadding(l, t, r, b);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {

            ThemeInstallResult.Image themeFeatured = result.getImages().get(position);

            try {
                String payload = (String) payloads.get(0);

                //update item view choose
                if (payload.equals(PAYLOAD_UPDATE_THEME_CHOOSE)) {
                    Boolean selected = themeChoose.getPreviewFile().getName().equals(themeFeatured.getName());
                    holder.binding.setSelected(selected);
                    holder.binding.executePendingBindings();
                }
            } catch (Exception e) {

            }
            holder.itemView.setOnClickListener(v -> callback.onItemClick(position));
        }
    }

    @Override
    public int getItemCount() {
        return result == null || result.getImages() == null ? 0 : result.getImages().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemThemeFeaturedBinding binding;

        public ViewHolder(@NonNull ItemThemeFeaturedBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(String previewLink, Boolean selected) {
            binding.setPreviewLink(previewLink);
            binding.setSelected(selected);
            binding.executePendingBindings();
        }
    }

    public interface Callback {
        void onItemClick(int pos);
    }

}
