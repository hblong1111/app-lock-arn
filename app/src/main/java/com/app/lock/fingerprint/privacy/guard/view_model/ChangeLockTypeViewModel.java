package com.app.lock.fingerprint.privacy.guard.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;

public class ChangeLockTypeViewModel extends ViewModel {
    private MutableLiveData<Boolean> isUnLock = new MutableLiveData<>();
    private MutableLiveData<LockTypeSetting.LockType> typeChoose = new MutableLiveData<>();

    public void setIsUnLock (MutableLiveData<Boolean> isUnLock) {
        this.isUnLock = isUnLock;
    }


    public MutableLiveData<LockTypeSetting.LockType> getTypeChoose () {
        return typeChoose;
    }

    public void setTypeChoose (LockTypeSetting.LockType typeChoose) {
        this.typeChoose.postValue(typeChoose);
    }

    public MutableLiveData<Boolean> getIsUnLock () {
        return isUnLock;
    }

    public void setIsUnLock (Boolean isUnLock) {
        this.isUnLock.postValue(isUnLock);
    }
}
