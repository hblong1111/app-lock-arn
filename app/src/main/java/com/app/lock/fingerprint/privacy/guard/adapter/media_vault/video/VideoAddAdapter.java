package com.app.lock.fingerprint.privacy.guard.adapter.media_vault.video;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.app.lock.databinding.ItemVideoAddBinding;
import com.blankj.utilcode.util.ScreenUtils;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;

public class VideoAddAdapter extends RecyclerView.Adapter<VideoAddAdapter.ViewHolder> {
    public static final String PAYLOAD_CHOOSE_CHANGE = "PAYLOAD_CHOOSE_CHANGE";
    private List<String> list;
    private List<String> listChoose;
    private Callback callback;

    public VideoAddAdapter (List<String> list, List<String> listChoose, Callback callback) {
        this.list = list;
        this.listChoose = listChoose;
        this.callback = callback;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder (@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemVideoAddBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder (@NonNull @NotNull ViewHolder holder, int position) {
        String path = list.get(position);
        File videoFile = new File(path);
        Boolean isChoose = listChoose.contains(path);
        holder.bind(videoFile, isChoose);

        setPadding(holder, position);
        holder.itemView.setOnClickListener(view -> callback.onItemClick(position));
    }

    @Override
    public void onBindViewHolder (@NonNull @NotNull ViewHolder holder, int position, @NonNull @NotNull List<Object> payloads) {

        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            String payload = (String) payloads.get(0);
            if (payload.equals(PAYLOAD_CHOOSE_CHANGE)) {
                String path = list.get(position);
                Boolean isChoose = listChoose.contains(path);
                holder.binding.setIsChoose(isChoose);
                holder.itemView.setOnClickListener(view -> callback.onItemClick(position));
            }
        }
    }

    private void setPadding (ViewHolder holder, int position) {
        int l = 20;
        int t = 0;
        int r = 20;
        int b = 0;

        if (position < 1) {
            t = 50;
        } else {
            t = 5;
        }

        if (position >= getItemCount() - 1) {
            b = ScreenUtils.getScreenHeight() / 4;
        } else {
            b = 5;
        }


        holder.itemView.setPadding(l, t, r, b);
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemVideoAddBinding binding;

        public ViewHolder (@NonNull @NotNull ItemVideoAddBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind (File videoFile, Boolean isChoose) {
            binding.setVideoFile(videoFile);
            binding.setIsChoose(isChoose);
            binding.executePendingBindings();
        }
    }

    public interface Callback {
        void onItemClick (int pos);
    }
}
