package com.app.lock.fingerprint.privacy.guard.fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.lock.databinding.DialogRequestAutoStartPermissionBinding;
import com.app.lock.databinding.DialogRequestOverlayPermissionBinding;
import com.app.lock.databinding.DialogRequestUsagePermissionBinding;
import com.app.lock.databinding.FragmentSearchAppBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.SearchAppAdapter;
import com.app.lock.fingerprint.privacy.guard.helper.DialogHelper;
import com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ServiceHelper;
import com.app.lock.fingerprint.privacy.guard.model.setting.ListAppLockSetting;
import com.app.lock.fingerprint.privacy.guard.service.LockAppService;
import com.app.lock.fingerprint.privacy.guard.view_model.HomeViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class SearchAppFragment extends Fragment {
    private FragmentSearchAppBinding binding;
    private HomeViewModel viewModel;
    private SearchAppAdapter adapter;
    private List<String> listAppClone;
    private List<String> listApp;
    private List<String> listAppLock;
    private Context context;
    private SearchAppAdapter.Callback callback = (pos, packageName) -> {

        if (!PermissionHelper.checkUsagePermission(context)) {
            showDialogRequestUsage(packageName, pos);
        } else if (!PermissionHelper.canDrawOverlays(context)) {
            showDialogRequestOverlay(packageName, pos);
        } else {
            addAppLock(pos, packageName);
        }

    };

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override

    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        context = requireContext();
        binding = FragmentSearchAppBinding.inflate(inflater);
        viewModel = new ViewModelProvider(requireActivity()).get(HomeViewModel.class);

        setupRCV();

        viewModel.getListAppLock().observe(requireActivity(), appList -> {
            if (appList.equals(listAppLock)) {
                return;
            }
            listAppLock.addAll(appList);
            adapter.notifyDataSetChanged();
        });
        viewModel.getListApp().observe(requireActivity(), lists -> {
            for (List<String> list :
                    lists) {
                for (String app :
                        list) {
                    listApp.add(app);
                    adapter.notifyItemInserted(adapter.getItemCount());
                }
            }
            listAppClone = new ArrayList<>();
            listAppClone.addAll(listApp);
        });
        binding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listApp.clear();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (listApp == null || listAppClone == null) {
                    return;
                }
                String txt = binding.edtSearch.getText().toString().trim();
                if (txt.isEmpty() && listAppClone != null) {
                    listApp.clear();
                    listApp.addAll(listAppClone);
                    adapter.notifyDataSetChanged();
                } else {
                    for (int i = 0; i < listAppClone.size(); i++) {
                        if (listAppClone.get(i).contains(txt)) {
                            listApp.add(listAppClone.get(i));
                            adapter.notifyItemInserted(adapter.getItemCount());
                        }
                    }
                }


            }
        });
        binding.btnClose.setOnClickListener(view -> requireActivity().onBackPressed());
        return binding.getRoot();
    }


    private void showDialogRequestUsage(String packageName, int position) {
        DialogRequestUsagePermissionBinding binding = DialogRequestUsagePermissionBinding.inflate(getLayoutInflater());
        DialogHelper.showDialog(context, binding.getRoot(), new DialogHelper.ConfigDialog() {
            @Override
            public boolean setCancelable() {
                return true;
            }

            @Override
            public void configDialog(Dialog dialog) {
                super.configDialog(dialog);

                ReceiverLocalHelper.registerReceiver(context, new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        if (!PermissionHelper.canDrawOverlays(context)) {
                            showDialogRequestOverlay(packageName, position);
                        }
                    }
                }, ReceiverLocalHelper.ReceiverType.ACTION_CLOSE_USAGE_PERMISSION_DIALOG);
                binding.btnClose.setOnClickListener(v -> {
                    dialog.dismiss();
                });

                binding.btnAgree.setOnClickListener(v -> {
                    PermissionHelper.requestUsagePermission(getActivity());
                });
            }
        });


    }


    private void showDialogRequestOverlay(String packageName, int position) {

        if (!isAdded()) {
            return;
        }
        DialogRequestOverlayPermissionBinding binding = DialogRequestOverlayPermissionBinding.inflate(getLayoutInflater());
        DialogHelper.showDialog(context, binding.getRoot(), new DialogHelper.ConfigDialog() {
            @Override
            public boolean setCancelable() {
                return true;
            }

            @Override
            public void configDialog(Dialog dialog) {
                super.configDialog(dialog);


                ReceiverLocalHelper.registerReceiver(context, new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (isAdded()) {
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                        if (PermissionHelper.checkDeviceAutoStart(getActivity())) {
                            showDialogRequestAutoStart(packageName, position);
                        } else {
                            onGrandPermission(packageName, position);
                        }
                    }
                }, ReceiverLocalHelper.ReceiverType.ACTION_CLOSE_OVERLAY_PERMISSION_DIALOG);
                binding.btnClose.setOnClickListener(v -> {
                    if (isAdded()) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                });

                binding.btnAgree.setOnClickListener(v -> {
                    PermissionHelper.requestOverlayPermission(getActivity());
                });
            }
        });
    }

    private void showDialogRequestAutoStart(String packageName, int position) {
        DialogRequestAutoStartPermissionBinding binding = DialogRequestAutoStartPermissionBinding.inflate(getLayoutInflater());
        DialogHelper.showDialog(context, binding.getRoot(), new DialogHelper.ConfigDialog() {
            @Override
            public boolean setCancelable() {
                return false;
            }

            @Override
            public void configDialog(Dialog dialog) {
                super.configDialog(dialog);
                binding.btnAgree.setOnClickListener(v -> {
                    PermissionHelper.requestAutoStart(getActivity());
                    dialog.dismiss();
                });
                binding.btnAgree.setOnClickListener(v -> {
                    dialog.dismiss();
                    PermissionHelper.requestAutoStart(getActivity());
                });

                dialog.setOnDismissListener(dialog1 -> {
                    onGrandPermission(packageName, position);
                });
            }
        });
    }

    private void onGrandPermission(String packageName, int position) {
        ReceiverLocalHelper.sendBroadcast(context, ReceiverLocalHelper.ReceiverType.ACTION_GRAND_ALL_PERMISSION);
        ServiceHelper.startService(context, LockAppService.class);
        addAppLock(position, packageName);
    }

    private void addAppLock(int position, String packageName) {
        if (listAppLock.contains(packageName)) {
            listAppLock.remove(packageName);
            try {
                String label = (String) context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(packageName, 0));
                Toasty.error(requireActivity(), "UnLocked: " + label, Toast.LENGTH_SHORT, true).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            listAppLock.add(packageName);
            try {
                String label = (String) context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(packageName, 0));
                Toasty.success(requireActivity(), "Locked: " + label, Toast.LENGTH_SHORT, true).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ListAppLockSetting.getInstance(context).setValue(listAppLock);

        adapter.notifyItemChanged(position);

        viewModel.setListAppLock(listAppLock);

    }

    private void setupRCV() {
        listApp = new ArrayList<>();
        listAppLock = new ArrayList<>();
        adapter = new SearchAppAdapter(listApp, listAppLock, callback);
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new LinearLayoutManager(requireContext()));
    }
}
