package com.app.lock.fingerprint.privacy.guard.activity;

import static com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper.CODE_REQUEST_PERMISSION_OVERLAY;
import static com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper.CODE_REQUEST_PERMISSION_USAGE;
import static com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper.startActivityTutorial;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.app.lock.R;
import com.app.lock.databinding.ActivityPermissionListBinding;
import com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ServiceHelper;
import com.app.lock.fingerprint.privacy.guard.service.LockAppService;

public class PermissionListActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityPermissionListBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPermissionListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.view3.setOnClickListener(this);
        binding.view4.setOnClickListener(this);
        binding.btnBack.setOnClickListener(this);
        binding.bntOverlay.setOnClickListener(this);
        binding.btnUsage.setOnClickListener(this);

//        binding.view3.setOnTouchListener((v, event) -> true);
//        binding.view4.setOnTouchListener((v, event) -> true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.swichOverlay.setChecked(PermissionHelper.canDrawOverlays(this));
        binding.switchUsage.setChecked(PermissionHelper.checkUsagePermission(this));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.view3:
            case R.id.btnUsage:
                binding.switchUsage.setChecked(PermissionHelper.canDrawOverlays(this));
                if (PermissionHelper.checkUsagePermission(this)) {
                    showDialogAlertPermission(view.getId());
                } else {
                    startUsageSetting();
                }
                break;
            case R.id.view4:
            case R.id.bntOverlay:
                if (PermissionHelper.canDrawOverlays(this)) {
                    showDialogAlertPermission(view.getId());
                } else {
                    startOverlaySetting();
                }
                binding.swichOverlay.setChecked(PermissionHelper.canDrawOverlays(this));
                break;
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }

    private void showDialogAlertPermission(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setMessage("Without your permission, the protection of apps will not work");
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        builder.setPositiveButton("Skip", (dialogInterface, i) -> {
            dialogInterface.dismiss();
            if (id == R.id.btnUsage) {
                startUsageSetting();
            } else if (!PermissionHelper.canDrawOverlays(this)) {
                startOverlaySetting();
            }
        });

        builder.create().show();


    }


    private void startOverlaySetting() {
        try {
            Intent intent1 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent1, CODE_REQUEST_PERMISSION_OVERLAY);
        } catch (Exception e) {
            Intent intent1 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            startActivityForResult(intent1, CODE_REQUEST_PERMISSION_OVERLAY);
        }
        startActivityTutorial(this, 1000);
    }

    private void startUsageSetting() {
        try {
            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, CODE_REQUEST_PERMISSION_USAGE);
        } catch (Exception e) {
            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS, null);
            startActivityForResult(intent, CODE_REQUEST_PERMISSION_USAGE);
        }

        startActivityTutorial(this, 1000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        boolean isServiceRunning = ServiceHelper.isMyServiceRunning(this, LockAppService.class);

        if (isServiceRunning) {
            if (!PermissionHelper.canDrawOverlays(this) || !PermissionHelper.checkUsagePermission(this)) {
                ServiceHelper.stopService(LockAppService.class);
            }
        } else if (PermissionHelper.canDrawOverlays(this) && PermissionHelper.checkUsagePermission(this)) {
            Toast.makeText(this, "Lock Apps function is running!", Toast.LENGTH_SHORT).show();
            ServiceHelper.startService(this, LockAppService.class);
        }
    }

}