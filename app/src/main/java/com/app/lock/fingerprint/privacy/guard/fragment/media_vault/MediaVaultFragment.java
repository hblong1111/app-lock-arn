package com.app.lock.fingerprint.privacy.guard.fragment.media_vault;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.lock.R;
import com.app.lock.databinding.FragmentMediaVaultBinding;
import com.app.lock.fingerprint.privacy.guard.ads.InterstitialAds;
import com.app.lock.fingerprint.privacy.guard.ads.NativeAds;
import com.app.lock.fingerprint.privacy.guard.fragment.media_vault.audios.AudiosFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.media_vault.document.DocumentsFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.media_vault.photos.PhotosFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.media_vault.videos.VideosFragment;
import com.app.lock.fingerprint.privacy.guard.view_model.MediaVaultViewModel;
import com.google.android.gms.ads.LoadAdError;

public class MediaVaultFragment extends Fragment implements View.OnClickListener {
    private FragmentMediaVaultBinding binding;
    private Context context;
    private MediaVaultViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentMediaVaultBinding.inflate(inflater);


        viewModel = new ViewModelProvider(requireActivity()).get(MediaVaultViewModel.class);

        context = requireActivity();

        binding.btnBack.setOnClickListener(this);
        binding.btnVideos.setOnClickListener(this);
        binding.btnPhotos.setOnClickListener(this);
        binding.btnDocuments.setOnClickListener(this);

        binding.btnAudios.setOnClickListener(this);


        viewModel.getListAudios().observe(requireActivity(), strings -> {
            binding.tvNumAudios.setText(String.valueOf(strings.size()));
        });


        viewModel.getListDocument().observe(requireActivity(), strings -> {
            binding.tvNumDocuments.setText(String.valueOf(strings.size()));
        });


        viewModel.getListImages().observe(requireActivity(), strings -> {
            binding.tvNumPhotos.setText(String.valueOf(strings.size()));
        });


        viewModel.getListVideos().observe(requireActivity(), strings -> {
            binding.tvNumVideos.setText(String.valueOf(strings.size()));
        });


        new NativeAds(requireActivity(), getString(R.string.id_ads_native_function), R.layout.ads_native_home_medium, binding.frameLayoutAds, new NativeAds.Callback() {
            @Override
            public void onAdFailedToLoad(LoadAdError i) {
                super.onAdFailedToLoad(i);
                binding.motionLayoutAds.transitionToEnd();
            }
        });


        return binding.getRoot();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnBack) {
            requireActivity().onBackPressed();
            return;
        }
        InterstitialAds.getInstance(requireActivity(), new InterstitialAds.CustomCallback() {
            @Override
            public void onAdFailedToLoad(LoadAdError i) {
                super.onAdFailedToLoad(i);
                closeAds();
            }

            @Override
            public void onPostShow() {
                super.onPostShow();
                closeAds();
            }

            private void closeAds() {
                Fragment fragment = null;
                switch (view.getId()) {
                    case R.id.btnVideos:
                        fragment = new VideosFragment();
                        break;
                    case R.id.btnPhotos:
                        fragment = new PhotosFragment();
                        break;
                    case R.id.btnDocuments:
                        fragment = new DocumentsFragment();
                        break;
                    case R.id.btnAudios:
                        fragment = new AudiosFragment();
                        break;
                }

                if (fragment != null) {
                    requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerFragment, fragment).addToBackStack(null).commit();
                }
            }
        }).showAds();
    }
}
