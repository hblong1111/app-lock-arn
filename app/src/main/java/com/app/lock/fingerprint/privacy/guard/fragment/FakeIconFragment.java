package com.app.lock.fingerprint.privacy.guard.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.lock.R;
import com.app.lock.databinding.DialogFakeIconBinding;
import com.app.lock.databinding.FragmentFakeIconBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.FakeIconAdapter;
import com.app.lock.fingerprint.privacy.guard.model.setting.FakeIconSetting;
import com.app.lock.fingerprint.privacy.guard.utils.CheckEvent;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

public class FakeIconFragment extends Fragment implements View.OnClickListener {
    private FragmentFakeIconBinding binding;
    private FragmentActivity activity;
    @SuppressLint("SetTextI18n")
    private FakeIconAdapter.Callback callback = (iconApp, pkgList) -> {
        FakeIconSetting.getInstance(activity).setValue(iconApp.getPkgName());
        Dialog alertDialog = new Dialog(activity);

        int width = getResources().getDisplayMetrics().widthPixels;

        DialogFakeIconBinding dialogBinding = DialogFakeIconBinding.inflate(getLayoutInflater(), null, false);

        dialogBinding.tvName.setText(iconApp.getName() + " ");

        dialogBinding.btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        dialogBinding.btnSubmit.setOnClickListener(v -> {
            alertDialog.dismiss();
            showDialogConfirmChangeIcon(pkgList, iconApp);
        });


        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(dialogBinding.getRoot());
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = (int) (0.9f * width);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        alertDialog.setCancelable(false);
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    };

    private void showDialogConfirmChangeIcon(List<String> pkgList, FakeIconAdapter.IconApp iconApp) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Warning");
        builder.setMessage("This action will result in the exit of the application, do you want to continue?");
        builder.setPositiveButton("Ok", (dialog, which) -> {
            dialog.dismiss();
            changeIcon(pkgList, iconApp);
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        builder.create().show();
    }

    private List<FakeIconAdapter.IconApp> listIcon;
    private List<String> listPackage;
    private Context context;
    private FakeIconAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentFakeIconBinding.inflate(inflater);

        context = requireContext();
        activity = requireActivity();

        setupRCV();

        binding.btnBack.setOnClickListener(this);
        binding.getRoot().setOnClickListener(this);

        return binding.getRoot();
    }


    @SuppressLint({"UseCompatLoadingForDrawables", "NotifyDataSetChanged"})
    private void setupRCV() {
        listIcon = new ArrayList<>();
        listPackage = new ArrayList<>();
        adapter = new FakeIconAdapter(listIcon, listPackage, callback);
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new GridLayoutManager(activity, 2));


        new Thread(() -> {
            Drawable drawableDefault;
            Drawable drawableCalculator;
            Drawable drawableCompass;
            Drawable drawableCalendar;
            Drawable drawableNews;
            try {
                drawableDefault = context.getResources().getDrawable(R.mipmap.ic_launcher_df);
                drawableCalculator = context.getResources().getDrawable(R.mipmap.ic_launcher_calculator);
                drawableCompass = context.getResources().getDrawable(R.mipmap.ic_launcher_compas);
                drawableCalendar = context.getResources().getDrawable(R.mipmap.ic_launcher_calendar);
                drawableNews = context.getResources().getDrawable(R.mipmap.ic_launcher_news);
            } catch (Exception e) {
                drawableDefault = ContextCompat.getDrawable(context, R.mipmap.ic_launcher_df);
                drawableCalculator = ContextCompat.getDrawable(context, R.mipmap.ic_launcher_calculator);
                drawableCompass = ContextCompat.getDrawable(context, R.mipmap.ic_launcher_compas);
                drawableCalendar = ContextCompat.getDrawable(context, R.mipmap.ic_launcher_calendar);
                drawableNews = ContextCompat.getDrawable(context, R.mipmap.ic_launcher_news);
            }
            listIcon.add(new FakeIconAdapter.IconApp("Default", context.getPackageName() + ".activity.SplashActivity", drawableDefault));
            if (FakeIconSetting.getInstance(context).getValue() == null || FakeIconSetting.getInstance(context).getValue().isEmpty()) {
                FakeIconSetting.getInstance(context).setValue(listIcon.get(0).getPkgName());
            }
            listIcon.add(new FakeIconAdapter.IconApp("Calculator", context.getPackageName() + ".Calculator", drawableCalculator));
            listIcon.add(new FakeIconAdapter.IconApp("Compass", context.getPackageName() + ".Compass", drawableCompass));
            listIcon.add(new FakeIconAdapter.IconApp("Calendar", context.getPackageName() + ".Calendar", drawableCalendar));
            listIcon.add(new FakeIconAdapter.IconApp("News", context.getPackageName() + ".News", drawableNews));

            for (FakeIconAdapter.IconApp iconApp :
                    listIcon) {
                listPackage.add(iconApp.getPkgName());
            }


            activity.runOnUiThread(() -> {
                adapter.notifyDataSetChanged();
            });
        }).start();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnBack) {
            requireActivity().onBackPressed();
        }
    }


    private void changeIcon(List<String> pkgList, FakeIconAdapter.IconApp iconApp) {
        KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f).show();
        for (String s :
                pkgList) {
            activity.getPackageManager().setComponentEnabledSetting(
                    new ComponentName(activity, s),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP
            );
        }

        activity.getPackageManager().setComponentEnabledSetting(
                new ComponentName(activity, iconApp.getPkgName()),
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP
        );
        CheckEvent.checkFakeIcon(context);
    }
}
