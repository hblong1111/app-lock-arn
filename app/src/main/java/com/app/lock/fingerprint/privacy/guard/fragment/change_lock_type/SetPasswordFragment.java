package com.app.lock.fingerprint.privacy.guard.fragment.change_lock_type;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.lock.R;
import com.app.lock.databinding.FragmentSetPasswordBinding;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.PasswordSetting;
import com.app.lock.fingerprint.privacy.guard.utils.CheckEvent;
import com.app.lock.fingerprint.privacy.guard.view_custom.pattern.PatternLockView;
import com.app.lock.fingerprint.privacy.guard.view_custom.pin_lock.PinInputView;
import com.app.lock.fingerprint.privacy.guard.view_model.ChangeLockTypeViewModel;

public class SetPasswordFragment extends Fragment {
    private FragmentSetPasswordBinding binding;

    private String newPassword;


    private boolean isError;

    private PinInputView.Callback callbackPin;
    private PatternLockView.PatternCallback callbackPattern;
    private Context context;
    private ChangeLockTypeViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSetPasswordBinding.inflate(inflater);

        context = requireActivity();


        viewModel = new ViewModelProvider(requireActivity()).get(ChangeLockTypeViewModel.class);

        binding.pinInput.setColor(R.color.black);
        binding.patternLockView.setColorDots(R.color.black);
        binding.patternLockView.setColorLine(R.color.black);

        LockTypeSetting.LockType lockType = viewModel.getTypeChoose().getValue();

        binding.containerPattern.setVisibility(lockType == LockTypeSetting.LockType.TYPE_PATTERN ? View.VISIBLE : View.INVISIBLE);
        binding.containerPin.setVisibility(lockType == LockTypeSetting.LockType.TYPE_PATTERN ? View.INVISIBLE : View.VISIBLE);

        if (lockType != LockTypeSetting.LockType.TYPE_PATTERN) {
            setupPinView(lockType);
        } else {
            setTitle("Pattern Setup");

            callbackPattern = new PatternLockView.PatternCallback() {
                @Override
                public void onDraw(String password) {
                }

                @Override
                public void onComplete(String password) {
                    if (password.length() < 4) {
                        setTitle("Invalid pattern");
                        setError(true);
                        return;
                    }
                    if (newPassword == null) {
                        newPassword = password;
                        setTitle("Pattern Confirm");
                        setError(false);
                    } else {
                        if (newPassword.equals(password)) {
                            onSetPasswordSuccess();
                        } else {
                            setTitle("Pattern Incorrect");
                            setError(true);
                        }
                    }
                }
            };

            binding.patternLockView.setCallbackPattern(callbackPattern);
        }

        return binding.getRoot();
    }

    private void setupPinView(LockTypeSetting.LockType lockType) {
        binding.pinInput.setPassLength(lockType == LockTypeSetting.LockType.TYPE_PIN_4 ? 4 : 6);

        setTitle("Pin Code Setup");
        callbackPin = new PinInputView.Callback() {
            @Override
            public void onChange(String password) {
            }

            @Override
            public void onComplete(String password) {
                if (newPassword == null) {
                    newPassword = password;
                    setTitle("Pin Code Confirm");
                } else {
                    if (newPassword.equals(password)) {
                        onSetPasswordSuccess();
                    } else {
                        setTitle("Pin Code Incorrect");
                        setError(true);
                    }
                }

                binding.pinInput.resetPin();
            }
        };
        if (lockType == LockTypeSetting.LockType.TYPE_PIN_4) {
            binding.indicatorView4.setPinView(binding.pinInput);
            binding.indicatorView4.setVisibility(View.VISIBLE);
        } else {
            binding.indicatorView6.setPinView(binding.pinInput);
            binding.indicatorView6.setVisibility(View.VISIBLE);
        }
        binding.pinInput.setCallback(callbackPin);
    }

    private void onSetPasswordSuccess() {
        setTitle("Success");
        setError(false);
        LockTypeSetting.getInstance(context).setValue(viewModel.getTypeChoose().getValue());
        PasswordSetting.getInstance(context).setValue(newPassword);

        CheckEvent.checkChangeLockType(requireContext());
        requireActivity().finish();
    }

    public void setTitle(String title) {
        binding.setTitle(title);
        binding.executePendingBindings();
    }

    public void setError(boolean error) {
        isError = error;
        if (error) {
            binding.patternLockView.onErrorPassword();
        } else {
            binding.patternLockView.resetPattern();
        }
        binding.setIsError(isError);
        binding.executePendingBindings();
    }
}
