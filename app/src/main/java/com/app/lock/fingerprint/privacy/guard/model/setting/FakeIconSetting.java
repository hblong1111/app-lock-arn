package com.app.lock.fingerprint.privacy.guard.model.setting;

import android.content.Context;

import com.app.lock.fingerprint.privacy.guard.utils.SharedUtils;

public class FakeIconSetting extends AppSetting {
    private static FakeIconSetting INSTANCE;
    private Context context;

    public static FakeIconSetting getInstance (Context context) {
        if (INSTANCE == null) {
            INSTANCE = new FakeIconSetting(context);
        }
        return INSTANCE;
    }

    private FakeIconSetting(Context context) {
        this.context = context;
    }

    @Override
    protected String getKey () {
        return super.getKey();
    }

    @Override
    public void setValue (Object password) {
        try {
            SharedUtils.getInstance(context).putString(getKey(), (String) password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getValue () {
        return SharedUtils.getInstance(context).getString(getKey());
    }
}
