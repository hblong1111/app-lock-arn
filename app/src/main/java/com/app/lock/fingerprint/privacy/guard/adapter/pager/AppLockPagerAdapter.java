package com.app.lock.fingerprint.privacy.guard.adapter.pager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.lock.fingerprint.privacy.guard.fragment.app_lock_pager.AllAppFragment;
import com.app.lock.fingerprint.privacy.guard.fragment.app_lock_pager.LockedAppFragment;

public class AppLockPagerAdapter extends FragmentStatePagerAdapter {
    public AppLockPagerAdapter (@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem (int position) {

        if (position == 0) {
            return new AllAppFragment();
        }
        return new LockedAppFragment();
    }

    @Override
    public int getCount () {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle (int position) {
        switch (position) {
            case 0:
                return "All Apps";
            case 1:
                return "Locked Apps";
        }
        return super.getPageTitle(position);
    }
}
