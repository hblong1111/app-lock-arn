package com.app.lock.fingerprint.privacy.guard.model.setting;

import android.content.Context;

import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.utils.SharedUtils;

public class ShowIconSetting extends AppSetting {
    private static ShowIconSetting INSTANCE;
    private Context context;

    public static ShowIconSetting getInstance (Context context) {
        if (INSTANCE == null) {
            INSTANCE = new ShowIconSetting(context);
        }
        return INSTANCE;
    }

    private ShowIconSetting (Context context) {
        this.context = context;
    }

    @Override
    protected String getKey () {
        return super.getKey();
    }

    @Override
    public void setValue (Object password) {
        try {
            SharedUtils.getInstance(context).putBoolean(getKey(), (Boolean) password);
            ReceiverLocalHelper.sendBroadcast(context, ReceiverLocalHelper.ReceiverType.ACTION_CHANGE_SHOW_ICON);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean getValue () {
        return SharedUtils.getInstance(context).getBoolean(getKey(), false);
    }
}
