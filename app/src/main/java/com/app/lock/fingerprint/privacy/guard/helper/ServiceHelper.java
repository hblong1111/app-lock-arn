package com.app.lock.fingerprint.privacy.guard.helper;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.blankj.utilcode.util.ServiceUtils;

public class ServiceHelper {

    public static void stopService(Class<?> serviceClass) {
        ServiceUtils.stopService(serviceClass);
    }

    public static void startService(Context context, Class<?> serviceClass, String action) {
        Intent intent = new Intent(context, serviceClass).setAction(action);

        context.startService(intent);
    }

    public static void startService(Context context, Class<?> serviceClass) {
        if (!isMyServiceRunning(context, serviceClass)) {
            Intent intent = new Intent(context, serviceClass);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            } else {
                context.startService(intent);
            }
        }
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
