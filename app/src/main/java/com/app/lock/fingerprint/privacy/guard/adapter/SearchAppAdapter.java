package com.app.lock.fingerprint.privacy.guard.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemAppSearchBinding;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SearchAppAdapter extends RecyclerView.Adapter<SearchAppAdapter.ViewHolder> {
    private List<String> list;
    private List<String> listChoose;
    private Callback callback;

    public SearchAppAdapter(List<String> list, List<String> listChoose, Callback callback) {
        this.list = list;
        this.listChoose = listChoose;
        this.callback = callback;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemAppSearchBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        holder.bind(list.get(position), listChoose.contains(list.get(position)));
        setPadding(holder, position);
        holder.itemView.setOnClickListener(view -> callback.onClick(position, list.get(position)));
    }

    private void setPadding(ViewHolder holder, int position) {
        int l = 20;
        int t = position < 1 ? 50 : 10;
        int b = position >= getItemCount() - 1 ? 50 : 10;
        int r = 20;

        holder.itemView.setPadding(l, t, r, b);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemAppSearchBinding binding;

        public ViewHolder(@NonNull @NotNull ItemAppSearchBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(String packageName, Boolean selected) {
            binding.setPackageName(packageName);
            binding.setSelected(selected);
            binding.executePendingBindings();
        }
    }

    public interface Callback {
        void onClick(int pos, String packageName);
    }
}
