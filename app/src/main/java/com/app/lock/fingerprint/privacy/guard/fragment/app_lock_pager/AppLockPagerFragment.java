package com.app.lock.fingerprint.privacy.guard.fragment.app_lock_pager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.lock.databinding.FragmentPagerAppLockBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.pager.AppLockPagerAdapter;

public class AppLockPagerFragment extends Fragment {
    private FragmentPagerAppLockBinding binding;

    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPagerAppLockBinding.inflate(inflater,container,false);

        binding.viewPager.setAdapter(new AppLockPagerAdapter(getChildFragmentManager()));

        binding.tabLayout.setupWithViewPager(binding.viewPager);
        return binding.getRoot();
    }
}
