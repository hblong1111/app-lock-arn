package com.app.lock.fingerprint.privacy.guard.adapter.home_theme;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemThemeInstallBinding;
import com.app.lock.fingerprint.privacy.guard.model.Theme;

import java.util.List;

public class ThemeInstallAdapter extends RecyclerView.Adapter<ThemeInstallAdapter.ViewHolder> {
    private static final int COLUMN_COUNT = 3;
    private List<Theme> list;
    private Theme themeChoose;
    private Callback callback;

    public static String PAYLOAD_UPDATE_THEME_CHOOSE = "PAYLOAD_UPDATE_THEME_CHOOSE";

    public ThemeInstallAdapter(List<Theme> list, Theme themeChoose, Callback callback) {
        this.list = list;
        this.themeChoose = themeChoose;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(ItemThemeInstallBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(list.get(position));
        setPaddingItem(holder, position);

        holder.itemView.setOnClickListener(view -> callback.onItemClick(position));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            try {
                String payload = (String) payloads.get(0);
                if (payload.equals(PAYLOAD_UPDATE_THEME_CHOOSE)) {
                    holder.binding.setIsSelected(themeChoose.equals(list.get(position)));
                    holder.binding.executePendingBindings();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            setPaddingItem(holder, position);

            holder.itemView.setOnClickListener(view -> callback.onItemClick(position));
        }
    }

    private void setPaddingItem(ViewHolder holder, int position) {
        int left = 10;
        int top = 0;
        int right = 10;
        int bottom;

        if (position < COLUMN_COUNT) {
            top = 50;
        } else {
            top = 20;
        }
        if (position >= getItemCount() - getItemCount() % COLUMN_COUNT) {
            bottom = 50;
        } else {
            bottom = 0;
        }


        holder.itemView.setPadding(left, top, right, bottom);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemThemeInstallBinding binding;

        public ViewHolder(@NonNull ItemThemeInstallBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(Theme theme) {
            binding.setIsSelected(theme.equals(themeChoose));
            binding.setPreviewThemeFile(theme.getPreviewFile());
            binding.executePendingBindings();
        }
    }

    public interface Callback {
        void onItemClick(int position);
    }
}
