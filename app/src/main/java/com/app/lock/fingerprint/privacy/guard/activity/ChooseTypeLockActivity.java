package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.app.lock.R;
import com.app.lock.databinding.ActivityChooseTypeLockBinding;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;

public class ChooseTypeLockActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityChooseTypeLockBinding binding;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChooseTypeLockBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnPattern.setOnClickListener(this);
        binding.btnPin6.setOnClickListener(this);
        binding.btnPin4.setOnClickListener(this);

    }

    @Override
    public void onClick (View v) {
        LockTypeSetting.LockType lockType;

        switch (v.getId()) {
            case R.id.btnPattern:
                lockType = LockTypeSetting.LockType.TYPE_PATTERN;
                break;
            case R.id.btnPin4:
                lockType = LockTypeSetting.LockType.TYPE_PIN_4;
                break;
            case R.id.btnPin6:
                lockType = LockTypeSetting.LockType.TYPE_PIN_6;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + v.getId());
        }

        LockTypeSetting.getInstance(this).setValue(lockType);
        startActivity(new Intent(this, SetPasswordActivity.class));
    }
}