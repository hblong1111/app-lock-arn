package com.app.lock.fingerprint.privacy.guard.adapter.home_app;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.R;
import com.app.lock.databinding.ItemAppBinding;
import com.app.lock.databinding.ItemTitleListAppBinding;
import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ListAppAdapter extends RecyclerView.Adapter<ListAppAdapter.ViewHolder> {

    private static final int TYPE_RECOMMEND_APP_TITLE = 1;
    private static final int TYPE_APP_TITLE = 2;
    private static final int TYPE_APP = 3;

    public static final String UPDATE_STATE_LOCK = "UPDATE_STATE_LOCK";

    private List<List<String>> list;
    private Context context;
    private PackageManager packageManager;
    private Callback callback;

    private List<String> listAppLock;

    public ListAppAdapter (List<List<String>> list, Callback callback, List<String> listAppLock) {
        this.list = list;
        this.callback = callback;
        this.listAppLock = listAppLock;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder (@NonNull @NotNull ViewGroup parent, int viewType) {
        if (context == null) {
            context = parent.getContext();
            packageManager = context.getPackageManager();
        }

        if (viewType == TYPE_APP) {
            return new ViewHolder(ItemAppBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        } else {
            return new ViewHolder(ItemTitleListAppBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }
    }

    @Override
    public int getItemViewType (int position) {
        if (list.isEmpty()) {
            return 0;
        }
        int positionTitleApp = list.get(0).size() + 1;
        if (position == 0) {
            return TYPE_RECOMMEND_APP_TITLE;
        } else if (position == positionTitleApp) {
            return TYPE_APP_TITLE;
        } else {
            return TYPE_APP;
        }
    }


    @Override
    public void onBindViewHolder (@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
            return;
        }
        try {
            String payload = (String) payloads.get(0);
            if (payload.equals(UPDATE_STATE_LOCK)) {
                String packageName = getListApp().get(position);
                Drawable iconButtonLock = ContextCompat.getDrawable(context, listAppLock.contains(packageName) ? R.drawable.ic_lock_app_adapter : R.drawable.ic_un_lock_adapter_app);
                holder.itemAppBinding.btnLock.setImageDrawable(iconButtonLock);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBindViewHolder (@NonNull @NotNull ViewHolder holder, int position) {
        setPaddingItem(holder, position);
        if (getItemViewType(position) == TYPE_APP) {
            setupViewApp(holder, position);
        } else {
            holder.itemTitleListAppBinding.tvTitle.setText(position == 0 ? "recommend" : "Application");
        }
    }


    private void setupViewApp (@NotNull ViewHolder holder, int position) {

        Drawable iconButtonLock = ContextCompat.getDrawable(context, listAppLock.contains(getListApp().get(position)) ? R.drawable.ic_lock_app_adapter : R.drawable.ic_un_lock_adapter_app);
        holder.itemAppBinding.btnLock.setImageDrawable(iconButtonLock);
        Drawable icon = null;
        String labelApp = null;
        try {
            ApplicationInfo infoApp = packageManager.getApplicationInfo(getListApp().get(position), 0);
            icon = infoApp.loadIcon(packageManager);
            labelApp = (String) infoApp.loadLabel(packageManager);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (icon != null && labelApp != null) {
            Glide.with(context).load(icon).into(holder.itemAppBinding.img);
            holder.itemAppBinding.tvName.setText(labelApp);
        } else {
            holder.itemView.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(v -> {
            callback.onItemAppClick(position, getListApp().get(position));
        });
    }

    private void setPaddingItem (@NotNull ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_APP) {
            if (getItemViewType(position - 1) == TYPE_RECOMMEND_APP_TITLE || getItemViewType(position - 1) == TYPE_APP_TITLE) {
                holder.itemView.setPadding(0, 50, 0, 0);
            } else if (position == getItemCount() - 1) {
                holder.itemView.setPadding(0, 15, 0, 50);
            } else {
                holder.itemView.setPadding(0, 15, 0, 0);
            }
        } else {
            holder.itemView.setPadding(0, 25, 0, 0);
        }
    }

    private List<String> getListApp () {
        List<String> result = new ArrayList<>();
        for (List<String> stringList :
                list) {
            result.add("");
            for (String s :
                    stringList) {
                result.add(s);
            }
        }
        return result;
    }

    @Override
    public int getItemCount () {
        if (list.isEmpty()) {
            return 0;
        }
        int itemCount = 2;
        for (List<String> stringList :
                list) {
            itemCount += stringList.size();
        }
        return itemCount;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemAppBinding itemAppBinding;
        private ItemTitleListAppBinding itemTitleListAppBinding;

        public ViewHolder (@NonNull @NotNull ItemAppBinding itemView) {
            super(itemView.getRoot());
            itemAppBinding = itemView;
        }

        public ViewHolder (ItemTitleListAppBinding inflate) {
            super(inflate.getRoot());
            itemTitleListAppBinding = inflate;
        }
    }

    public interface Callback {
        void onItemAppClick (int position, String packageName);
    }
}
