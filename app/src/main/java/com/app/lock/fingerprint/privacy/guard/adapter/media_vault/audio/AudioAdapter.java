package com.app.lock.fingerprint.privacy.guard.adapter.media_vault.audio;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.lock.databinding.ItemAudioBinding;
import com.blankj.utilcode.util.ScreenUtils;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;

public class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.ViewHolder> {
    public static final String PAYLOAD_CHANGE_SELECTED = "PAYLOAD_CHANGE_SELECTED";
    private List<String> list;
    private List<String> listSelected;
    private Callback callback;
    private boolean isEdit;

    public AudioAdapter (List<String> list, List<String> listSelected, Callback callback) {
        this.list = list;
        this.listSelected = listSelected;
        this.callback = callback;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder (@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemAudioBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder (@NonNull @NotNull ViewHolder holder, int position) {

        holder.bind(new File(list.get(position)), isEdit, listSelected.contains(list.get(position)));
        setPadding(holder, position);
        holder.itemView.setOnClickListener(view -> {
            callback.onClick(position);
        });

        holder.itemView.setOnLongClickListener(view -> {
            callback.onLongClick(position);
            return false;
        });
    }

    @Override
    public void onBindViewHolder (@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
        } else {
            try {
                String payload = (String) payloads.get(0);
                if (payload.equals(PAYLOAD_CHANGE_SELECTED)) {
                    holder.binding.setIsEdit(isEdit);
                    holder.binding.setIsChoose(listSelected.contains(list.get(position)));
                    holder.binding.executePendingBindings();

                    setPadding(holder, position);
                    holder.itemView.setOnClickListener(view -> {
                        callback.onClick(position);
                    });

                    holder.itemView.setOnLongClickListener(view -> {
                        callback.onLongClick(position);
                        return false;
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setPadding (ViewHolder holder, int position) {
        int l = 20;
        int t = 0;
        int r = 20;
        int b = 0;

        if (position < 1) {
            t = 50;
        } else {
            t = 5;
        }

        if (position >= getItemCount() - 1) {
            b = ScreenUtils.getScreenHeight() / 4;
        } else {
            b = 5;
        }


        holder.itemView.setPadding(l, t, r, b);
    }

    @Override
    public int getItemCount () {
        return list.size();
    }

    public void setEdit (boolean edit) {
        isEdit = edit;
        notifyItemRangeChanged(0, getItemCount(), PAYLOAD_CHANGE_SELECTED);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemAudioBinding binding;

        public ViewHolder (@NotNull @NonNull ItemAudioBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind (File audioFile, boolean isEdit, boolean isChoose) {
            binding.setAudioFile(audioFile);
            binding.setIsEdit(isEdit);
            if (isEdit) {
                binding.setIsChoose(isChoose);
            }
            binding.executePendingBindings();
        }
    }

    public interface Callback {
        void onClick (int pos);

        void onLongClick (int position);
    }
}
