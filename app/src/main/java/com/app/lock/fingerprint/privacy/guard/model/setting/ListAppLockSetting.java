package com.app.lock.fingerprint.privacy.guard.model.setting;

import android.content.Context;

import com.app.lock.fingerprint.privacy.guard.utils.SharedUtils;

import java.util.ArrayList;
import java.util.List;

public class ListAppLockSetting extends AppSetting {
    private static ListAppLockSetting INSTANCE;
    private Context context;

    public static ListAppLockSetting getInstance (Context context) {
        if (INSTANCE == null) {
            INSTANCE = new ListAppLockSetting(context);
        }
        return INSTANCE;
    }

    private ListAppLockSetting (Context context) {
        this.context = context;
    }

    @Override
    protected String getKey () {
        return super.getKey();
    }

    @Override
    public void setValue (Object value) {
        try {
            SharedUtils.getInstance(context).putListString(getKey(), (ArrayList<String>) value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> getValue () {
        return SharedUtils.getInstance(context).getListString(getKey());
    }
}
