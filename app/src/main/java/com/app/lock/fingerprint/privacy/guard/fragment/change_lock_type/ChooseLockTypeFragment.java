package com.app.lock.fingerprint.privacy.guard.fragment.change_lock_type;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.lock.R;
import com.app.lock.databinding.FragmentChooseLockTypeBinding;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;
import com.app.lock.fingerprint.privacy.guard.view_model.ChangeLockTypeViewModel;

public class ChooseLockTypeFragment extends Fragment implements View.OnClickListener {

    private FragmentChooseLockTypeBinding binding;

    private ChangeLockTypeViewModel viewModel;


    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentChooseLockTypeBinding.inflate(inflater);

        viewModel = new ViewModelProvider(requireActivity()).get(ChangeLockTypeViewModel.class);

        binding.btnPattern.setOnClickListener(this);
        binding.btnPin4.setOnClickListener(this);
        binding.btnPin6.setOnClickListener(this);

        return binding.getRoot();
    }

    @Override
    public void onClick (View view) {
        LockTypeSetting.LockType lockType;
        switch (view.getId()) {
            case R.id.btnPin6:
                lockType = LockTypeSetting.LockType.TYPE_PIN_6;
                break;
            case R.id.btnPattern:
                lockType = LockTypeSetting.LockType.TYPE_PATTERN;
                break;
            default:
                lockType = LockTypeSetting.LockType.TYPE_PIN_4;
                break;
        }

        viewModel.setTypeChoose(lockType);

    }
}
