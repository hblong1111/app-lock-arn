package com.app.lock.fingerprint.privacy.guard.fragment.themes_pager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.lock.databinding.FragmentPagerThemesBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.pager.AppLockPagerAdapter;
import com.app.lock.fingerprint.privacy.guard.adapter.pager.ThemePagerAdapter;

public class ThemePagerFragment extends Fragment {
    private FragmentPagerThemesBinding binding;

    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPagerThemesBinding.inflate(inflater, container, false);

        binding.viewPager.setAdapter(new ThemePagerAdapter(getChildFragmentManager()));


        binding.tabLayout.setupWithViewPager(binding.viewPager);
        return binding.getRoot();
    }
}
