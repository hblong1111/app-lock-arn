package com.app.lock.fingerprint.privacy.guard.fragment.media_vault.videos;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.lock.BuildConfig;
import com.app.lock.R;
import com.app.lock.databinding.FragmentVideosBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.media_vault.video.VideoAdapter;
import com.app.lock.fingerprint.privacy.guard.helper.MediaVaultHelper;
import com.app.lock.fingerprint.privacy.guard.view_model.MediaVaultViewModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class VideosFragment extends Fragment implements View.OnClickListener {
    private FragmentVideosBinding binding;
    private MediaVaultViewModel viewModel;
    private VideoAdapter adapter;
    private List<String> listVideo;
    private List<String> listSelected;
    private boolean isEditList;
    private VideoAdapter.Callback callback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentVideosBinding.inflate(inflater);
        viewModel = new ViewModelProvider(requireActivity()).get(MediaVaultViewModel.class);

        viewModel.setNumberChoose(0);

        setupRCV();

        binding.btnBack.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);
        binding.btnUnLock.setOnClickListener(this);
        binding.btnAdd.setOnClickListener(this);

        viewModel.getListVideos().observe(requireActivity(), paths -> {
            if (paths.isEmpty()) {
                binding.rcv.setVisibility(View.INVISIBLE);
                binding.groupNotFound.setVisibility(View.VISIBLE);
            } else {

                binding.rcv.setVisibility(View.VISIBLE);
                binding.groupNotFound.setVisibility(View.INVISIBLE);
            }
            if (!paths.equals(listVideo)) {
                for (String path :
                        paths) {
                    if (!listVideo.contains(path)) {
                        listVideo.add(0, path);
                        adapter.notifyItemInserted(0);
                    }
                }
            }
        });

        viewModel.getNumberChoose().observe(requireActivity(), integer -> {
            binding.btnUnLock.setText(String.format("Un Lock %d File", integer));
        });
        return binding.getRoot();
    }

    private void setupRCV() {
        listVideo = new ArrayList<>();
        listSelected = new ArrayList<>();
        callback = createCallbackAdapter();
        adapter = new VideoAdapter(listVideo, listSelected, callback);

        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new LinearLayoutManager(requireActivity()));
    }

    private VideoAdapter.Callback createCallbackAdapter() {
        return new VideoAdapter.Callback() {
            @Override
            public void onClick(int pos) {
                if (listVideo.isEmpty()) {
                    return;
                }
                String path = listVideo.get(pos);
                if (!isEditList) {
                    File file = new File(path);
                    Uri uri = FileProvider.getUriForFile(requireActivity(), BuildConfig.APPLICATION_ID, file);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    String type = "video/*";
                    intent.setDataAndType(uri, type);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    requireActivity().startActivity(intent);
                    viewModel.getNumberClickFile().setValue(viewModel.getNumberClickFile().getValue() + 1);
                } else {
                    if (listSelected.contains(path)) {
                        listSelected.remove(path);
                    } else {
                        listSelected.add(path);
                    }
                    viewModel.setNumberChoose(listSelected.size());
                    adapter.notifyItemChanged(pos, adapter.PAYLOAD_CHANGE_SELECTED);
                }
            }

            @Override
            public void onLongClick(int position) {
                if (!isEditList) {
                    setEditList(true);
                } else {
                    String path = listVideo.get(position);
                    if (listSelected.contains(path)) {
                        listSelected.remove(path);
                    } else {
                        listSelected.add(path);
                    }
                    viewModel.setNumberChoose(listSelected.size());
                    adapter.notifyItemChanged(position, adapter.PAYLOAD_CHANGE_SELECTED);
                }
            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                requireActivity().onBackPressed();
                break;
            case R.id.btnAdd:
                requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerFragment, new VideosAddFragment()).addToBackStack(null).commit();
                break;
            case R.id.btnUnLock:
                for (String path :
                        listSelected) {
                    MediaVaultHelper.unLock(requireActivity(), path);
                    int index = listVideo.indexOf(path);
                    listVideo.remove(index);
                    adapter.notifyItemRemoved(index);
                }
                viewModel.setListVideos(listVideo);
            case R.id.btnCancel:
                setEditList(false);
                break;
        }
    }

    public void setEditList(boolean editList) {
        if (!editList) {
            listSelected.clear();
        }
        if (isEditList == editList) {
            return;
        }
        isEditList = editList;
        adapter.setEdit(editList);
        binding.setIsEdit(isEditList);
        binding.executePendingBindings();
    }
}
