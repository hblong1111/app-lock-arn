package com.app.lock.fingerprint.privacy.guard.fragment.change_password;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.lock.R;
import com.app.lock.databinding.FragmentUnLockBinding;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.PasswordSetting;
import com.app.lock.fingerprint.privacy.guard.view_custom.pattern.PatternLockView;
import com.app.lock.fingerprint.privacy.guard.view_custom.pin_lock.PinInputView;
import com.app.lock.fingerprint.privacy.guard.view_model.ChangeLockTypeViewModel;
import com.app.lock.fingerprint.privacy.guard.view_model.ChangePasswordViewModel;

public class UnLockChangePasswordFragment extends Fragment {
    private FragmentUnLockBinding binding;
    private PinInputView.Callback callbackPin;
    private Context context;
    private PatternLockView.PatternCallback callbackPattern;
    private LockTypeSetting.LockType lockType;

    private ChangePasswordViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentUnLockBinding.inflate(inflater);

        viewModel = new ViewModelProvider(requireActivity()).get(ChangePasswordViewModel.class);

        context = requireActivity();

        binding.setTitleFragment("To change password, you need confirm old password");
        setTextTitle("Your Old Password");

        lockType = LockTypeSetting.getInstance(context).getValue();
        binding.pinInput.setColor(R.color.black);
        binding.patternLock.setColorLine(R.color.black);
        binding.patternLock.setColorDots(R.color.black);

        binding.setType(lockType);


        if (lockType != LockTypeSetting.LockType.TYPE_PATTERN) {
            if (lockType == LockTypeSetting.LockType.TYPE_PIN_4) {
                binding.indicatorPin4.setPinView(binding.pinInput);
            } else {
                binding.indicatorPin6.setPinView(binding.pinInput);
            }
        }


        callbackPin = new PinInputView.Callback() {
            @Override
            public void onChange (String password) {

            }

            @Override
            public void onComplete (String password) {
                checkPassword(password);
            }
        };

        callbackPattern = new PatternLockView.PatternCallback() {
            @Override
            public void onDraw (String password) {
                setTitleError(false);
                setTextTitle("Drawing pattern");
            }

            @Override
            public void onComplete (String password) {
                if (password.length() < 4) {
                    setTitleError(true);
                    setTextTitle("Invalid pattern");
                    return;
                }
                checkPassword(password);
            }
        };

        binding.pinInput.setCallback(callbackPin);

        binding.patternLock.setCallbackPattern(callbackPattern);
        return binding.getRoot();
    }

    private void checkPassword (String password) {
        boolean isUnLock = password.equals(PasswordSetting.getInstance(context).getValue());
        setTitleError(! isUnLock);
        if (password.equals(PasswordSetting.getInstance(context).getValue())) {
            setTextTitle("Confirm Success!!!");
            onUnLockSuccess();
        } else {
            binding.patternLock.resetPattern();
            binding.pinInput.resetPin();
            setTextTitle("Wrong password, please try again");
        }
    }

    private void onUnLockSuccess () {
        viewModel.setIsUnLock(true);
    }

    private void setTitleError (boolean isUnLock) {
        binding.setIsError(isUnLock);
        binding.executePendingBindings();
    }

    private void setTextTitle (String txt) {
        binding.setTitle(txt);
        binding.executePendingBindings();
    }
}
