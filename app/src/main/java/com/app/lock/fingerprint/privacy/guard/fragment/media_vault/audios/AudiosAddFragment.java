package com.app.lock.fingerprint.privacy.guard.fragment.media_vault.audios;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.lock.R;
import com.app.lock.databinding.FragmentAudiosAddBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.media_vault.audio.AudioAddAdapter;
import com.app.lock.fingerprint.privacy.guard.helper.MediaVaultHelper;
import com.app.lock.fingerprint.privacy.guard.view_model.MediaVaultViewModel;

import java.util.ArrayList;
import java.util.List;

public class AudiosAddFragment extends Fragment implements View.OnClickListener {
    private FragmentAudiosAddBinding binding;
    private MediaVaultViewModel viewModel;
    private AudioAddAdapter adapter;
    private List<String> listAudio;
    private List<String> listAudioChoose;
    private Context context;
    private MediaVaultHelper.Type type = MediaVaultHelper.Type.TYPE_AUDIOS;
    private AudioAddAdapter.Callback callback = new AudioAddAdapter.Callback() {
        @Override
        public void onItemClick (int pos) {
            String path = listAudio.get(pos);
            if (listAudioChoose.contains(path)) {
                listAudioChoose.remove(path);
            } else {
                listAudioChoose.add(path);
            }
            adapter.notifyItemChanged(pos, adapter.PAYLOAD_CHOOSE_CHANGE);

            viewModel.setNumberChoose(listAudioChoose.size());
        }
    };


    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = requireActivity();
        binding = FragmentAudiosAddBinding.inflate(inflater);

        viewModel = new ViewModelProvider(requireActivity()).get(MediaVaultViewModel.class);

        viewModel.setNumberChoose(0);


        setupRCV();

        fetchData();

        viewModel.getNumberChoose().observe(requireActivity(), integer -> {
            binding.bntAdd.setText(String.format("Add %d Files", integer));
        });

        binding.btnBack.setOnClickListener(this);
        binding.bntAdd.setOnClickListener(this);
        return binding.getRoot();
    }

    private void fetchData () {
        new Thread(() -> {
            ArrayList<String> listImageDevice = MediaVaultHelper.getAudiosPath(context);

            requireActivity().runOnUiThread(() -> {
                if (listImageDevice.isEmpty()) {
                    binding.rcv.setVisibility(View.INVISIBLE);
                    binding.groupNotFound.setVisibility(View.VISIBLE);
                } else {
                    binding.rcv.setVisibility(View.VISIBLE);
                    binding.groupNotFound.setVisibility(View.INVISIBLE);
                }
            });
            for (String path :
                    listImageDevice) {
                listAudio.add(path);
                requireActivity().runOnUiThread(() -> {
                    adapter.notifyItemInserted(adapter.getItemCount());
                });
            }
        }).start();
    }

    private void setupRCV () {
        listAudio = new ArrayList<>();
        listAudioChoose = new ArrayList<>();
        adapter = new AudioAddAdapter(listAudio, listAudioChoose, callback);
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new LinearLayoutManager(context));
    }

    @Override
    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.bntAdd:
                if (! listAudioChoose.isEmpty()) {
                    List<String> listLock = viewModel.getListAudios().getValue();
                    for (String path :
                            listAudioChoose) {
                        listLock.add(MediaVaultHelper.lockFile(context, path, type));
                    }
                    viewModel.setListAudios(listLock);
                }
                break;
        }

        requireActivity().onBackPressed();
    }
}
