package com.app.lock.fingerprint.privacy.guard.view_custom;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.lock.R;
import com.app.lock.databinding.ViewHomeTabLayoutBinding;
import com.app.lock.fingerprint.privacy.guard.activity.HomeActivity;
import com.app.lock.fingerprint.privacy.guard.ads.InterstitialAds;

public class HomeTabLayoutView extends ConstraintLayout {
    private ViewHomeTabLayoutBinding binding;
    private Context context;

    public HomeTabLayoutView(@NonNull Context context) {
        super(context);
        init();
    }

    public HomeTabLayoutView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HomeTabLayoutView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public HomeTabLayoutView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        context = getContext();
        binding = ViewHomeTabLayoutBinding.inflate(LayoutInflater.from(context), this, true);
    }

    public void withPager(ViewPager pager) {
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                binding.setPosition(position);
                binding.executePendingBindings();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.btnAppLock.setOnClickListener(v -> {
            pager.setCurrentItem(0);
        });
        binding.btnTheme.setOnClickListener(v -> {
            if (pager.getCurrentItem() == 0) {
                pager.setCurrentItem(1);
                //show ads intertitial
                InterstitialAds.getInstance((Activity) context, new InterstitialAds.CustomCallback() {
                }).showAds();
            }
        });
    }

    @BindingAdapter("setTintImage")
    public static void setTintImage(ImageView imageView, boolean isSelected) {
        imageView.setColorFilter(ContextCompat.getColor(imageView.getContext(), isSelected ? R.color.white : R.color.color_txt_tab_un_seletec), android.graphics.PorterDuff.Mode.SRC_IN);

    }


}
