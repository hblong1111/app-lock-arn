package com.app.lock.fingerprint.privacy.guard.service;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.LifecycleService;


import com.app.lock.R;
import com.app.lock.fingerprint.privacy.guard.activity.SplashActivity;
import com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ServiceHelper;
import com.app.lock.fingerprint.privacy.guard.model.setting.IntruderSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ListAppLockSetting;
import com.app.lock.fingerprint.privacy.guard.utils.AndroidUtils;
import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.app.lock.fingerprint.privacy.guard.view_custom.LockView;

import java.util.List;

public class LockAppService extends LifecycleService {

    private Context context = this;

    private CameraIntruder cameraIntruder;


    @Override
    public void onCreate() {
        super.onCreate();

        //create channel and show notification
        new NotificationService().createChannel().show();

        startReceiver();

        //create WindowManager and add view
        WindowManagerHelper.getInstance(this).addView();

        //start task check package app recent
        TaskCheckPackage.getInstance(this).start();

        if (IntruderSetting.getInstance(this).getValue() && PermissionHelper.checkCamera(context)) {
            cameraIntruder = new CameraIntruder(this, this);
        }


        ReceiverLocalHelper.registerReceiver(context, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                WindowManagerHelper.getInstance(LockAppService.this).addView();
            }
        }, ReceiverLocalHelper.ReceiverType.ACTION_GRAND_ALL_PERMISSION);


        ReceiverLocalHelper.registerReceiver(context, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (cameraIntruder != null) {
                    cameraIntruder.destroy();
                    cameraIntruder = null;
                }
            }
        }, ReceiverLocalHelper.ReceiverType.ACTION_CHANGE_INTRUDER_SETTING);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();
            if (action.equals(Common.ACTION_TAKE_INTRUDER)) {
                if (cameraIntruder != null) {
                    cameraIntruder.takePhoto();
                }
            } else if (action.equals(Common.ACTION_CREATE_CAMERA)) {
                if (cameraIntruder == null) {
                    cameraIntruder = new CameraIntruder(this, this);
                }
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        WindowManagerHelper.getInstance(this).removeView();
        TaskCheckPackage.getInstance(this).onDestroy();

        sendBroadcast(new Intent("GET_BUZZER"));

        //create an intent that you want to start again.
        Intent intent = new Intent(getApplicationContext(), LockAppService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 1, intent, android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S ? PendingIntent.FLAG_MUTABLE : 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime() + 2000, pendingIntent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {

        sendBroadcast(new Intent("GET_BUZZER"));

        //create an intent that you want to start again.
        Intent intent = new Intent(getApplicationContext(), LockAppService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 1, intent, android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S ? PendingIntent.FLAG_MUTABLE : 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime() + 2000, pendingIntent);
        super.onTaskRemoved(rootIntent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        super.onBind(intent);
        return null;
    }


    private void startReceiver() {
        RestartReceiver receiver = new RestartReceiver();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("GET_BUZZER");
        registerReceiver(receiver, localIntentFilter);

    }

    private static class TaskCheckPackage extends Thread {
        private static TaskCheckPackage INSTANCE;
        private Context context;

        public static TaskCheckPackage getInstance(Context context) {
            if (INSTANCE == null) {
                INSTANCE = new TaskCheckPackage(context);
            }
            return INSTANCE;
        }

        private TaskCheckPackage(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            super.run();
            String oldPackageName = null;
            while (true) {
                String s = getRecentApps(context);
                if (s == null || s.length() == 0 || s.equals(oldPackageName)) {
                    continue;
                }

                oldPackageName = s;

                Log.d("hblong", "run: " + s);
                if (ListAppLockSetting.getInstance(context).getValue().contains(s)) {
                    WindowManagerHelper.getInstance(context).showLock(s);
                } else {
                    WindowManagerHelper.getInstance(context).hideLock(150);
                }
            }
        }

        @Override
        public synchronized void start() {
            if (!isInterrupted()) {
                super.start();
            }
        }

        private String getRecentApps(Context context) {
            String topPackageName = "";

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                UsageStatsManager mUsageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);

                long time = System.currentTimeMillis();

                UsageEvents usageEvents = mUsageStatsManager.queryEvents(time - 1000 * 30, System.currentTimeMillis() + (10 * 1000));
                UsageEvents.Event event = new UsageEvents.Event();
                while (usageEvents.hasNextEvent()) {
                    usageEvents.getNextEvent(event);
                }

                if (event != null && !TextUtils.isEmpty(event.getPackageName()) && event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND) {
                    if (AndroidUtils.isRecentActivity(event.getClassName())) {
                        return event.getClassName();
                    }
                    return event.getPackageName();
                } else {
                    topPackageName = "";
                }
            } else {
                ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;

                // If the current running activity it will return not the package name it will return the activity refernce.
                if (AndroidUtils.isRecentActivity(componentInfo.getClassName())) {
                    return componentInfo.getClassName();
                }

                topPackageName = componentInfo.getPackageName();
            }

            return topPackageName;
        }

        public void onDestroy() {
            interrupt();
        }
    }

    private static class WindowManagerHelper {
        private static WindowManagerHelper INSTANCE;
        private WindowManager mWindowManager;
        private LockView lockView;
        private WindowManager.LayoutParams paramsLockView;
        private Context context;

        public static WindowManagerHelper getInstance(Context context) {
            if (INSTANCE == null) {
                INSTANCE = new WindowManagerHelper(context);
            }
            return INSTANCE;
        }

        private WindowManagerHelper(Context context) {
            this.context = context;
            mWindowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
            lockView = new LockView(context);

            paramsLockView = new WindowManager.LayoutParams();


            paramsLockView.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
            paramsLockView.format = PixelFormat.RGBA_8888;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                paramsLockView.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            } else {
                paramsLockView.type = WindowManager.LayoutParams.TYPE_PHONE;
            }
            paramsLockView.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

            paramsLockView.width = WindowManager.LayoutParams.MATCH_PARENT;
            paramsLockView.height = WindowManager.LayoutParams.MATCH_PARENT;

        }

        public void addView() {
            if (PermissionHelper.canDrawOverlays(context) && PermissionHelper.checkUsagePermission(context)) {
                try {
                    mWindowManager.addView(lockView, paramsLockView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        private void hideLock(long delay) {
            lockView.hideLock(delay);
        }

        public void removeView() {
            try {
                mWindowManager.removeView(lockView);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void showLock(String packageName) {
            lockView.showLock(packageName);
        }

    }

    private class NotificationService {
        private final String CHANNEL_ID = "Calculator Lock Channel";

        public NotificationService createChannel() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel serviceChannel = new NotificationChannel(
                        CHANNEL_ID,
                        "Foreground Service Channel",
                        NotificationManager.IMPORTANCE_DEFAULT
                );
                NotificationManager manager = getSystemService(NotificationManager.class);
                manager.createNotificationChannel(serviceChannel);
            }

            return this;
        }

        public void show() {
            Intent notificationIntent = new Intent(context, SplashActivity.class);

            PendingIntent pendingIntent = PendingIntent.getActivity(context,
                    0, notificationIntent, android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S ? PendingIntent.FLAG_MUTABLE : 0);

            Notification notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.content_text_notification_lock_service))
                    .setShowWhen(false)
                    .setSmallIcon(R.drawable.drawable_small_icon_notification)
                    .build();

            try {
                startForeground(1, notification);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class RestartReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ServiceHelper.startService(context, LockAppService.class);
        }
    }

}
