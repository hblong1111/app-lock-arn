package com.app.lock.fingerprint.privacy.guard.fragment.intruder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.lock.R;
import com.app.lock.databinding.FragmentIntruderBinding;
import com.app.lock.databinding.FragmentIntruderIntroBinding;
import com.app.lock.fingerprint.privacy.guard.helper.PermissionHelper;
import com.app.lock.fingerprint.privacy.guard.model.setting.IntruderSetting;
import com.app.lock.fingerprint.privacy.guard.utils.CheckEvent;
import com.app.lock.fingerprint.privacy.guard.view_model.IntruderViewModel;

import org.jetbrains.annotations.NotNull;

public class IntruderIntroFragment extends Fragment implements View.OnClickListener {
    private FragmentIntruderIntroBinding binding;
    private IntruderViewModel viewModel;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = FragmentIntruderIntroBinding.inflate(inflater);

        viewModel = new ViewModelProvider(requireActivity()).get(IntruderViewModel.class);

        binding.btnActive.setOnClickListener(this);
        binding.btnBack.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnActive:
                if (PermissionHelper.checkCamera(requireActivity())) {
                    IntruderSetting.getInstance(requireActivity()).setValue(true);
                    viewModel.setIsActive(true);
                } else {
                    PermissionHelper.requestCamera(requireActivity());
                }

                CheckEvent.checkIntruder(requireContext());
                break;
            case R.id.btnBack:
                requireActivity().onBackPressed();
                break;
        }
    }
}
