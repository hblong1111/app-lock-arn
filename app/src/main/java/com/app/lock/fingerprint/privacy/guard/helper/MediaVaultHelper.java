package com.app.lock.fingerprint.privacy.guard.helper;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.blankj.utilcode.util.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MediaVaultHelper {

    private static final String FILE_NAME_VAULT_EXTEND = ".LOL";

    public static List<String> getDocumentPath(Context context) {
        List<String> listResult = new ArrayList<>();
        Cursor cursor;
        Uri uri = MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL);


        String[] projection = {MediaStore.Files.FileColumns.DATA
                , MediaStore.Files.FileColumns.DATE_MODIFIED
                , MediaStore.Files.FileColumns.MEDIA_TYPE
                , MediaStore.Files.FileColumns.DISPLAY_NAME
                , MediaStore.Files.FileColumns.MIME_TYPE
        };

        String selection;
        selection = MediaStore.Files.FileColumns.MEDIA_TYPE + " = " + (Build.VERSION.SDK_INT >= 30 ? MediaStore.Files.FileColumns.MEDIA_TYPE_DOCUMENT : 10003);

        cursor = context.getContentResolver().query(uri, projection, selection,
                null, MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC");

        int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);
        int column_index_data1 = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MIME_TYPE);

        while (cursor.moveToNext()) {
            String path = cursor.getString(column_index_data);
            String type = cursor.getString(column_index_data1);
            listResult.add(path);
        }
        return listResult;
    }

    public static ArrayList<String> getAudiosPath(Context context) {
        ArrayList<String> listResult = new ArrayList<>();
        Cursor cursor;
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.DATE_MODIFIED, MediaStore.Audio.AudioColumns.MIME_TYPE};


        cursor = context.getContentResolver().query(uri, projection, null,
                null, MediaStore.Audio.AudioColumns.DATE_MODIFIED + " DESC");

        int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        int column_index_data1 = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.MIME_TYPE);
        while (cursor.moveToNext()) {
            String path = cursor.getString(column_index_data);
            String type = cursor.getString(column_index_data1);
            listResult.add(path);
        }
        return listResult;
    }

    public static ArrayList<String> getImagesPath(Context context) {
        Uri uri;
        ArrayList<String> listOfAllImages = new ArrayList<>();
        Cursor cursor;
        int column_index_data;
        String PathOfImage = null;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA};

        String sort = MediaStore.Images.ImageColumns.DATE_MODIFIED + " DESC";
        cursor = context.getContentResolver().query(uri, projection, null,
                null, sort);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        while (cursor.moveToNext()) {
            PathOfImage = cursor.getString(column_index_data);
            listOfAllImages.add(PathOfImage);
        }
        return listOfAllImages;
    }

    public static ArrayList<String> getVideosPath(Context context) {
        Uri uri;
        ArrayList<String> listOfAllImages = new ArrayList<>();
        Cursor cursor;
        int column_index_data;
        String PathOfImage = null;
        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA};

        String sort = MediaStore.Video.VideoColumns.DATE_MODIFIED + " DESC";
        cursor = context.getContentResolver().query(uri, projection, null,
                null, sort);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        while (cursor.moveToNext()) {
            PathOfImage = cursor.getString(column_index_data);
            listOfAllImages.add(PathOfImage);
        }
        return listOfAllImages;
    }

    public static String convertFileSize(long total) {
        if (total < 1024) {
            return total + " B";
        } else if (total < 1024 * 1024) {
            float size = (float) Math.round(total * 100 / 1024) / 100;
            return size + " KB";
        } else if (total < 1024 * 1024 * 1024) {
            float size = (float) Math.round(total * 100 / (1024 * 1024)) / 100;
            return size + " MB";
        } else if (total < 1024 * 1024 * 1024 * 1024) {
            float size = (float) Math.round(total * 100 / (1024 * 1024 * 1024)) / 100;
            return size + " GB";
        }
        return "";

    }

    public static ArrayList<String> getAllFileVault(Type type) {
        String pathRoot;
        switch (type) {
            case TYPE_AUDIOS:
                pathRoot = Common.PATH_MUSIC;
                break;
            case TYPE_DOCUMENTS:
                pathRoot = Common.PATH_DOCUMENTS;
                break;
            case TYPE_IMAGES:
                pathRoot = Common.PATH_IMAGES;
                break;
            case TYPE_VIDEOS:
                pathRoot = Common.PATH_VIDEOS;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }

        File listFile[] = new File(pathRoot).listFiles();
        if (listFile != null && listFile.length > 0) {
            ArrayList<String> fileList = new ArrayList<>();
            for (File file :
                    listFile) {
                fileList.add(file.getPath());
            }
            return fileList;
        }

        return new ArrayList<>();
    }

    public static void unLock(Context context, String path) {
        File file = new File(path);
        File file1 = new File(Common.PATH_FILES_UN_LOCK + "/" + getFileName(file.getName()));
        moveFile(file, file1, null);
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file1)));
    }

    public static String lockFile(Context context, String path, Type type) {
        String pathRoot;
        switch (type) {
            case TYPE_AUDIOS:
                pathRoot = Common.PATH_MUSIC;
                break;
            case TYPE_DOCUMENTS:
                pathRoot = Common.PATH_DOCUMENTS;
                break;
            case TYPE_IMAGES:
                pathRoot = Common.PATH_IMAGES;
                break;
            case TYPE_VIDEOS:
                pathRoot = Common.PATH_VIDEOS;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }

        File file = new File(path);
        File fileTarget = new File(pathRoot + "/ " + getFileName(file.getName()));
        moveFile(file, fileTarget, null);
        delete(context, file);

        return fileTarget.getAbsolutePath();
    }

    public static String getFileName(String name) {
        if (name.contains(FILE_NAME_VAULT_EXTEND)) {
            return name.substring(2, name.lastIndexOf(FILE_NAME_VAULT_EXTEND));
        } else {
            return "." + name + FILE_NAME_VAULT_EXTEND;
        }
    }

    public static void moveFile(File file, File targetFile, FileUtils.OnReplaceListener onReplaceListener) {
        FileUtils.moveFile(file, targetFile, onReplaceListener);
    }

    public static boolean delete(final Context context, final File file) {
        if (!file.delete()) {
            final String where = MediaStore.MediaColumns.DATA + "=?";
            final String[] selectionArgs = new String[]{
                    file.getAbsolutePath()
            };
            final ContentResolver contentResolver = context.getContentResolver();
            final Uri filesUri = MediaStore.Files.getContentUri("external");

            contentResolver.delete(filesUri, where, selectionArgs);

            if (file.exists()) {
                contentResolver.delete(filesUri, where, selectionArgs);
            }

        }

        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
        return !file.exists();
    }

    public enum Type {
        TYPE_AUDIOS,
        TYPE_IMAGES,
        TYPE_VIDEOS,
        TYPE_DOCUMENTS
    }
}
