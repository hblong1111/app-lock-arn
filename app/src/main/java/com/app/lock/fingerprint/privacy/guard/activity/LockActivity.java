package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import com.app.lock.R;
import com.app.lock.databinding.ActivityLockBinding;
import com.app.lock.databinding.ViewLockBinding;
import com.app.lock.fingerprint.privacy.guard.ads.NativeAds;
import com.app.lock.fingerprint.privacy.guard.application.MyApplication;
import com.app.lock.fingerprint.privacy.guard.helper.ReceiverLocalHelper;
import com.app.lock.fingerprint.privacy.guard.helper.ServiceHelper;
import com.app.lock.fingerprint.privacy.guard.model.Theme;
import com.app.lock.fingerprint.privacy.guard.model.setting.IntruderSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.LockTypeSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.PasswordSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ShowIconSetting;
import com.app.lock.fingerprint.privacy.guard.model.setting.ThemeSetting;
import com.app.lock.fingerprint.privacy.guard.service.LockAppService;
import com.app.lock.fingerprint.privacy.guard.utils.CheckEvent;
import com.app.lock.fingerprint.privacy.guard.utils.Common;
import com.app.lock.fingerprint.privacy.guard.view_custom.pattern.PatternLockView;
import com.app.lock.fingerprint.privacy.guard.view_custom.pin_lock.PinInputView;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.LoadAdError;

public class LockActivity extends AppCompatActivity {
    private ActivityLockBinding binding;
    private Context context;
    private Vibrator vibrator;


    //create callback pin
    private PinInputView.Callback callbackPin = new PinInputView.Callback() {
        @Override
        public void onChange(String password) {

        }

        @Override
        public void onComplete(String password) {
            String passwordSetting = PasswordSetting.getInstance(context).getValue();
            if (passwordSetting.equals(password)) {
                onUnLockSuccess();
            } else {
                onUnLockError();
            }
        }
    };
    //create callback pattern
    private PatternLockView.PatternCallback callbackPattern = new PatternLockView.PatternCallback() {
        @Override
        public void onDraw(String password) {

        }

        @Override
        public void onComplete(String password) {
            String passwordSetting = PasswordSetting.getInstance(context).getValue();
            Log.d("hblong", "onComplete: " + password + "|" + passwordSetting);
            if (passwordSetting.equals(password)) {
                onUnLockSuccess();
            } else {
                onUnLockError();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLockBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        context = this;

        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);


//        setup view
        setLockType(LockTypeSetting.getInstance(context).getValue());
        setTheme();


        //set callback view lock
        binding.pinInput.setCallback(callbackPin);
        binding.patternLock.setCallbackPattern(callbackPattern);
//        binding.viewClick.setOnClickListener(v -> {
//        });
//
//        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
//        valueAnimator.setDuration(MyApplication.isShowAds ? Common.TIME_DELAY_LOAD_NATIVE_ADS : 1);
//        valueAnimator.setInterpolator(new LinearInterpolator());
//        valueAnimator.addUpdateListener(animation -> {
//            float value = (float) animation.getAnimatedValue();
//            binding.patternLock.setScaleX(value);
//            binding.patternLock.setScaleY(value);
//            binding.pinInput.setScaleX(value);
//            binding.pinInput.setScaleY(value);
//        });
//        valueAnimator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                super.onAnimationEnd(animation);
//                binding.viewClick.setVisibility(View.INVISIBLE);
//            }
//        });
//        valueAnimator.start();

        new NativeAds(this, context.getString(R.string.id_ads_native_function), R.layout.ads_native_unlock, binding.frameLayoutAds, new NativeAds.Callback() {
            @Override
            public void onAdFailedToLoad(LoadAdError i) {
                super.onAdFailedToLoad(i);
                binding.motionLayoutAds.transitionToEnd();
                binding.frameLayoutAds.setVisibility(View.INVISIBLE);
            }
        });
    }


    private void vibrate(long time) {
        // Vibrate for time
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(time, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            vibrator.vibrate(time);
        }
    }

    public void setLockType(LockTypeSetting.LockType lockType) {
        if (lockType == LockTypeSetting.LockType.TYPE_PIN_4) {
            binding.indicatorPin4.setPinView(binding.pinInput);
        } else {
            binding.indicatorPin6.setPinView(binding.pinInput);
        }

        binding.setType(lockType);
        binding.executePendingBindings();

    }

    private void onUnLockError() {
        if (IntruderSetting.getInstance(context).getValue()) {
            ServiceHelper.startService(context, LockAppService.class, Common.ACTION_TAKE_INTRUDER);
        }
        vibrate(200);
        binding.pinInput.resetPin();
        binding.patternLock.onErrorPassword();
    }

    private void onUnLockSuccess() {
        CheckEvent.checkUnlock(context);
        startActivity(new Intent(context, HomeActivity.class));
        finish();
    }


    public void setTheme() {
        Theme theme = ThemeSetting.getInstance(context).getValue();
        Glide.with(context).load(theme.getBackgroundFile()).into(binding.imgBackground);
    }
}