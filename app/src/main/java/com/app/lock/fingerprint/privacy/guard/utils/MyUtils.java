package com.app.lock.fingerprint.privacy.guard.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.LauncherApps;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MyUtils {
    public static List<String> getAllAppLauncher(Context context) {
        List<String> listApp = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LauncherApps launcherApps = (LauncherApps) context.getSystemService(Context.LAUNCHER_APPS_SERVICE);
            List<LauncherActivityInfo> activityList = launcherApps.getActivityList(null, launcherApps.getProfiles().get(0));
            for (LauncherActivityInfo info :
                    activityList) {
                String packageName = info.getApplicationInfo().packageName;
                if (!packageName.equals(context.getPackageName())) {
                    listApp.add(packageName);
                }
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN, null);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> activitiesInfo = context.getPackageManager().queryIntentActivities(intent, 0);
            for (ResolveInfo info :
                    activitiesInfo) {
                String packageName = info.activityInfo.packageName;
                if (!packageName.equals(context.getPackageName())) {
                    listApp.add(packageName);
                }
            }
        }
        return listApp;
    }
}
