package com.app.lock.fingerprint.privacy.guard.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.app.lock.R;
import com.app.lock.databinding.ActivityRemoveAdsBinding;
import com.app.lock.fingerprint.privacy.guard.application.MyApplication;
import com.app.lock.fingerprint.privacy.guard.purchase.ProxPurchase;
import com.app.lock.fingerprint.privacy.guard.utils.AndroidUtils;
import com.blankj.utilcode.util.ActivityUtils;

import java.nio.channels.AlreadyBoundException;
import java.util.ArrayList;
import java.util.List;

public class RemoveAdsActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int TYPE_MONTH = 11;
    public static final int TYPE_YEAR = 22;
    private ActivityRemoveAdsBinding binding;

    private int selectType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRemoveAdsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        binding.btnContinue.setOnClickListener(this);
        binding.btnYear.setOnClickListener(this);
        binding.btnMonth.setOnClickListener(this);

        setupView();
    }

    private void setupView() {

        //setup view Month
        String priceM = ProxPurchase.getInstance().getPriceSub(getString(R.string.id_in_app_purchase_month));
        String textTitleM = String.format("1 Month: %s", priceM);
        String textDesM = String.format("Experience the upgrade with ad-free features, unlimited functions for only %s a Monthly", priceM);
        binding.tvTitleMonth.setText(textTitleM);
        binding.tvDesciptionMonth.setText(textDesM);

        //setup view Year
        String priceY = ProxPurchase.getInstance().getPriceSub(getString(R.string.id_in_app_purchase_year));
        String textTitleY = String.format("1 Year: %s", priceY);
        String textDesY = String.format("Experience the upgrade with ad-free features, unlimited functions for only %s a Yearly", priceY);
        binding.tvTitleYear.setText(textTitleY);
        binding.tvDescriptionYear.setText(textDesY);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMonth:
                setSelectType(TYPE_MONTH);
                break;
            case R.id.btnYear:
                setSelectType(TYPE_YEAR);
                break;
            case R.id.btnContinue:
                if (selectType != 0) {
                    ProxPurchase.getInstance().subscribe(this, getString(selectType == TYPE_MONTH ? R.string.id_in_app_purchase_month : R.string.id_in_app_purchase_year));
                } else {
                    Toast.makeText(this, "Please select an option!", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    public void setSelectType(int selectType) {
        this.selectType = selectType;
        binding.setSelectType(selectType);
        binding.executePendingBindings();
    }

    @Override
    protected void onDestroy() {
        MyApplication.isShowAds = !ProxPurchase.getInstance().isPurchased();
        super.onDestroy();
    }
}