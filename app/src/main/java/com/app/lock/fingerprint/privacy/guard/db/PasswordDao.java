package com.app.lock.fingerprint.privacy.guard.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.app.lock.fingerprint.privacy.guard.model.PasswordSafe;

import java.util.List;

@Dao
public interface PasswordDao {
    @Query("SELECT * FROM passwordsafe")
    List<PasswordSafe> getAll();

    @Insert
    long insert(PasswordSafe passwordSafes);

    @Update
    void update(PasswordSafe passwordSafes);

    @Delete
    void delete(PasswordSafe passwordSafes);
}
