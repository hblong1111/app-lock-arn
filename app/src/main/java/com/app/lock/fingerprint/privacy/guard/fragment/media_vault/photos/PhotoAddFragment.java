package com.app.lock.fingerprint.privacy.guard.fragment.media_vault.photos;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.lock.R;
import com.app.lock.databinding.FragmentPhotoAddBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.media_vault.photo.ImageAddAdapter;
import com.app.lock.fingerprint.privacy.guard.helper.MediaVaultHelper;
import com.app.lock.fingerprint.privacy.guard.view_model.MediaVaultViewModel;

import java.util.ArrayList;
import java.util.List;

public class PhotoAddFragment extends Fragment implements View.OnClickListener {
    private FragmentPhotoAddBinding binding;
    private MediaVaultViewModel viewModel;
    private ImageAddAdapter adapter;
    private List<String> listImage;
    private List<String> listImageChoose;
    private Context context;
    private MediaVaultHelper.Type type = MediaVaultHelper.Type.TYPE_IMAGES;
    private ImageAddAdapter.Callback callback = pos -> {
        String path = listImage.get(pos);
        if (listImageChoose.contains(path)) {
            listImageChoose.remove(path);
        } else {
            listImageChoose.add(path);
        }
        adapter.notifyItemChanged(pos, adapter.PAYLOAD_CHOOSE_CHANGE);

        viewModel.setNumberChoose(listImageChoose.size());
    };


    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = requireActivity();
        binding = FragmentPhotoAddBinding.inflate(inflater);

        viewModel = new ViewModelProvider(requireActivity()).get(MediaVaultViewModel.class);

        viewModel.setNumberChoose(0);


        setupRCV();

        fetchData();

        viewModel.getNumberChoose().observe(requireActivity(), integer -> {
            binding.bntAdd.setText(String.format("Add %d Files", integer));
        });

        binding.btnBack.setOnClickListener(this);
        binding.bntAdd.setOnClickListener(this);
        return binding.getRoot();
    }

    private void fetchData () {
        new Thread(() -> {
            ArrayList<String> listImageDevice = MediaVaultHelper.getImagesPath(context);

            requireActivity().runOnUiThread(() -> {
                if (listImageDevice.isEmpty()) {
                    binding.rcv.setVisibility(View.INVISIBLE);
                    binding.groupNotFound.setVisibility(View.VISIBLE);
                } else {
                    binding.rcv.setVisibility(View.VISIBLE);
                    binding.groupNotFound.setVisibility(View.INVISIBLE);
                }
            });
            for (String path :
                    listImageDevice) {
                listImage.add(path);
                requireActivity().runOnUiThread(() -> {
                    adapter.notifyItemInserted(adapter.getItemCount());
                });
            }
        }).start();
    }

    private void setupRCV () {
        listImage = new ArrayList<>();
        listImageChoose = new ArrayList<>();
        adapter = new ImageAddAdapter(listImage, listImageChoose, callback);
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new GridLayoutManager(context, 4));
    }

    @Override
    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.bntAdd:
                if (! listImageChoose.isEmpty()) {
                    List<String> listLock = viewModel.getListImages().getValue();
                    for (String path :
                            listImageChoose) {
                        listLock.add(MediaVaultHelper.lockFile(context, path, type));
                    }
                    viewModel.setListImages(listLock);
                }
                break;
        }

        requireActivity().onBackPressed();
    }
}
