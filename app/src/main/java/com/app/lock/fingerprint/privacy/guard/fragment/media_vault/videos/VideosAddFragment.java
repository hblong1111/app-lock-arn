package com.app.lock.fingerprint.privacy.guard.fragment.media_vault.videos;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.lock.R;
import com.app.lock.databinding.FragmentVideosAddBinding;
import com.app.lock.fingerprint.privacy.guard.adapter.media_vault.video.VideoAddAdapter;
import com.app.lock.fingerprint.privacy.guard.helper.MediaVaultHelper;
import com.app.lock.fingerprint.privacy.guard.view_model.MediaVaultViewModel;

import java.util.ArrayList;
import java.util.List;

public class VideosAddFragment extends Fragment implements View.OnClickListener {
    private FragmentVideosAddBinding binding;
    private MediaVaultViewModel viewModel;
    private VideoAddAdapter adapter;
    private List<String> listVideo;
    private List<String> listVideoChoose;
    private Context context;
    private MediaVaultHelper.Type type = MediaVaultHelper.Type.TYPE_VIDEOS;
    private VideoAddAdapter.Callback callback = new VideoAddAdapter.Callback() {
        @Override
        public void onItemClick (int pos) {
            String path = listVideo.get(pos);
            if (listVideoChoose.contains(path)) {
                listVideoChoose.remove(path);
            } else {
                listVideoChoose.add(path);
            }
            adapter.notifyItemChanged(pos, adapter.PAYLOAD_CHOOSE_CHANGE);

            viewModel.setNumberChoose(listVideoChoose.size());
        }
    };


    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = requireActivity();
        binding = FragmentVideosAddBinding.inflate(inflater);

        viewModel = new ViewModelProvider(requireActivity()).get(MediaVaultViewModel.class);

        viewModel.setNumberChoose(0);


        setupRCV();

        fetchData();

        viewModel.getNumberChoose().observe(requireActivity(), integer -> {
            binding.bntAdd.setText(String.format("Add %d Files", integer));
        });

        binding.btnBack.setOnClickListener(this);
        binding.bntAdd.setOnClickListener(this);
        return binding.getRoot();
    }

    private void fetchData () {
        new Thread(() -> {
            ArrayList<String> listImageDevice = MediaVaultHelper.getVideosPath(context);

            requireActivity().runOnUiThread(() -> {
                if (listImageDevice.isEmpty()) {
                    binding.rcv.setVisibility(View.INVISIBLE);
                    binding.groupNotFound.setVisibility(View.VISIBLE);
                } else {
                    binding.rcv.setVisibility(View.VISIBLE);
                    binding.groupNotFound.setVisibility(View.INVISIBLE);
                }
            });
            for (String path :
                    listImageDevice) {
                listVideo.add(path);
                requireActivity().runOnUiThread(() -> {
                    adapter.notifyItemInserted(adapter.getItemCount());
                });
            }
        }).start();
    }

    private void setupRCV () {
        listVideo = new ArrayList<>();
        listVideoChoose = new ArrayList<>();
        adapter = new VideoAddAdapter(listVideo, listVideoChoose, callback);
        binding.rcv.setAdapter(adapter);
        binding.rcv.setLayoutManager(new LinearLayoutManager(context));
    }

    @Override
    public void onClick (View view) {
        switch (view.getId()) {
            case R.id.bntAdd:
                if (! listVideoChoose.isEmpty()) {
                    List<String> listLock = viewModel.getListVideos().getValue();
                    for (String path :
                            listVideoChoose) {
                        listLock.add(MediaVaultHelper.lockFile(context, path, type));
                    }
                    viewModel.setListVideos(listLock);
                }
                break;
        }

        requireActivity().onBackPressed();
    }
}
